ExpEYES web site
################

ExpEYES is from the PHOENIX project of Inter-University Accelerator Centre,
New Delhi. It is a hardware & software framework for developing science
experiments, demonstrations and projects without getting in to the details
of electronics or computer programming. It converts your PC into a science
laboratory.

The source of the web site is mainly written in ReStruturedText and converted
in static HTML files tanks to `Pelican <https://blog.pelican.com>`__.

The style was derived from the theme dev-random2 for Pelican,
Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>, which is distributed
under *DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE*.

In order to generate the web pages, one can run :code:`make html`.

### For developers

+ sudo apt-get install pelican libjs-mathjax fonts-mathjax fonts-mathjax-extra
+  ./develop_server.sh start port=8000
+ The website is now available at localhost:8000

as an alternative to :code:`./develop_server.sh start`,
you can run :code:`make serve`; you can even consider to run
:code:`make regenerate` while you are serving the pages locally, which
will rebuild the html tree whenever changes in the source are detected.

The packages providing mathjax features are necessary to render the
LaTeX formulae used in some pages.

How to translate the website
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

List of languages for localization (l10n)
-----------------------------------------

The file Makefile contains on line, near its end, defining a list
of languages (initially only fr). If you want to manage more languages,
add other two-character language codes, separated by spaces.

Generating the TODO list for translations
-----------------------------------------

Launch the command :code:`make todo-l10n`. This command outputs lists of not yet
translated files, and lists of files which may be revised (older than the
english source).

Preparing a source English file for translation (or not)
--------------------------------------------------------

Before doing any translation of file foo.rst, you must check that this file
declares its language explicitly; one of the header lines must be:

:code:`:lang: en`

If such a line is missing, the build of translations may fail.

When a file must **not be translated**, you must check that its heading
lines define a tag "no-translation", for example there may be a line:

:code:`:tags: no-translation`

in the header; so this file will no longer be listed as a non-translated file.

Translating an English file like foo.rst
-----------------------------------------

In this example, the target language will be Spanish, code :code:`es`.

- Copy the file to :code:`foo.es.rst`
- Change the header :code:`:lang: en` to :code:`:lang: es` in the new file
- translate the text, in order to make it readable by the target users.
    - for ordinary text, make an "ordinary" translation
    - for links, it may be interesting to add other references,
      to documents in Spanish language, when they exist
    - when the page contains source code in Python for example, it
      is often worth to translate comments, and strings which will be displaied
      to users.

       
Some translations are bound to the templates
--------------------------------------------

Pelican uses a templating system (based on Jinja2) to render
pages. Some translations are not in the directory :code:`content`
but in the directory :code:`themes/expeyesdev-random2/`.

In order to translate that part (which is rather lightweight compared
to the other one), one must go to the directory
:code:`themes/expeyesdev-random2/`, then launch the command
:code:`lang=xy pybabel init --input-file messages.pot --output-dir lang/ --locale ${lang} --domain expeyes` where :code:`lang=xy` defines the new language
(for example :code:`lang=es` will be used for Spanish language). This
will bootstrap the creation of files for the new language. A new file
will be created, under the directory
:code:`lang/xy/LC_MESSAGES/expeyes.po` (:code:`xy` being the language code)

To translate the English strings to language :code:`xy`, one must
open the file :code:`lang/xy/LC_MESSAGES/expeyes.po` with a suitable
translation tool (for example Emacs, with its po-mode!) and input
the translations. When translations are updated and saved, one must
launch the command :code:`make` in the templates directory to
compile .po files to .mo files, which are later used to create the
localized pages.

