#! /usr/bin/python3

"""
Ensure one title in every RST file
"""

def ensureTitle(filename: str, default_title="ExpEyes") -> bool:
    """
    Ensure the file filename contains one title
    :param filename: path to a file
    :type  filename: str
    :param default_title: a default title when it is missing
    :type  default_title: str
    :return: whether a title was inserted
    :rtype: bool
    """
    lines=open(filename).readlines()
    title_lines = [l for l in lines if l.startswith(":title: ")]
    if title_lines:
        # there is at least one line beginning with ":title:"
        return False
    if lines[1].startswith("##"):
        # pelican will find a title, let us return False
        return False
    ############### begin the work ########################
    first_non_directive_line=0
    while not lines[first_non_directive_line].startswith(":"):
        first_non_directive_line+=1
    while lines[first_non_directive_line].startswith(":"):
        first_non_directive_line+=1
    ## now first_non_directive_line is the right place to insert a title
    ## let us find one
    title = f":title: {default_title}\n"
    the_title =     f"{default_title}\n"
    the_underline = "#"*len(default_title)+"\n"
    first_title_line = 0
    while first_title_line < len(lines)-1 and \
          not lines[first_title_line+1].startswith("##"):
          first_title_line +=1
    if first_title_line < len(lines)-1:
        title = ":title: " + lines [first_title_line]
        the_title = ""+lines [first_title_line]
        the_underline = ""+lines [first_title_line+1]
        del lines[first_title_line+1]
        del lines[first_title_line]
    ## the title is found, let us insert the line with ":title:"
    lines.insert(first_non_directive_line, title)
    ## and let us write the title as a first line
    lines.insert(0, the_title)
    lines.insert(1, the_underline)
    ## then let us move the 
    ## and overwrite the file
    with open(filename,"w") as outfile:
        for l in lines:
            outfile.write(l)
    return True

if __name__ == "__main__":
    import os
    for root, dirs, files in os.walk("content"):
        for f in files:
            if not f.endswith(".rst"):
                continue
            path=os.path.join(root, f)
            if ensureTitle(path):
                print("added a title in:", path)
        
