window.onload = function(e){
    var path=window.location.pathname;
    var prefix="";
    var barepath=path;
    var pattern= new RegExp("^(/..)(/.*)$");
    var m=path.match(pattern);
    if (m) {
	prefix=m[1];
	barepath=m[2];
    }
    var languages=document.querySelector("#languages").value.split(",");
    var flags=document.querySelector("#flags");
    var imgcode="";
    for(var i=0; i<languages.length; i++){
	var l=languages[i];
	var otherprefix="";
	if (l!="en") otherprefix="/"+l;
	var url="#";
	if (prefix!=otherprefix) url=otherprefix+barepath;
	imgcode+="<a class='flag' href='"+url+"'><img class='flag' src='"+prefix+"/theme/images/"+l+".gif' alt='"+l+"'/></a>";
    }
    flags.innerHTML=imgcode;
}
