��          �               �  O   �  1   �  )     '   I  -   q  -   �  #   �  '   �  '     +   A  7   m  '   �  $   �     �     �          '  
   B     M     h     ~     �  �   �  �  r  [     5   s  ,   �  .   �  1     0   7  +   h  ,   �  *   �  .   �  5   	  0   Q	  '   �	     �	  !   �	     �	  !   �	     
     
     2
     G
  
   J
    U
   <a href="/Expt17/examplePrograms.html">Example Programs to access ExpEYES17</a> <a href="/category/expts_17.html">Experiments</a> <a href="/pages/android.html">Android</a> <a href="/pages/design.html">Design</a> <a href="/pages/hardware.html">How to Buy</a> <a href="/pages/microhope.html">MicroHOPE</a> <a href="/pages/news.html">News</a> <a href="/pages/people.html">People</a> <a href="/pages/python.html">Python</a> <a href="/pages/software.html">Software</a> <a href="schoollevel.html">School Level Experiments</a> Articles in the category "%(category)s" Electrical Circuits and LCR elements Electronics Experiments using ExpEYES-17 I2C Modules Mechanics and Thermal etc. Powered by Sound (velocity and beats) Website maintained at and contact:     The pages below are organized according to the pull down menus of the ExpEYES17 GUI program. The help files mainly contains wiring diagrams, photograph of experimental setup, screen shots of results and a brief description. Project-Id-Version: Expeyes Website 1.0
Report-Msgid-Bugs-To: georgesk@debian.org
POT-Creation-Date: 2018-09-30 19:52+0200
PO-Revision-Date: 2018-09-30 19:55+0200
Last-Translator: Georges Khaznadar <georgesk@debian.org>
Language: fr
Language-Team: fr <LL@li.org>
Plural-Forms: nplurals=2; plural=(n > 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.6.0
 <a href="/fr/Expt17/examplePrograms.html">Exemple de programme pour commander ExpEYES17</a> <a href="/fr/category/expts_17.html">Expériences</a> <a href="/fr/pages/android.html">Android</a> <a href="/fr/pages/design.html">Conception</a> <a href="/fr/pages/hardware.html">Où acheter</a> <a href="/fr/pages/microhope.html">MicroHOPE</a> <a href="/fr/pages/news.html">Nouvelles</a> <a href="/fr/pages/people.html">Les gens</a> <a href="/fr/pages/python.html">Python</a> <a href="/fr/pages/software.html">Logiciel</a> <a href="schoollevel.html">Expériences scolaires</a> Articles dans la catégorie « %(category)s » Circuits électriques et éléments RLC Électronique Expériences utilisant ExpEYES-17 Modules I2C Mécanique et thermophysique etc. Propulsé par Son (vitesse et battements) Site web maintenu à et contact :     Les pages ci-dessous sont organisées selon les menus déroulants de l'interface graphique du programme ExpEYES17. Les fichiers d'aide contiennent les schémas de câblage, des photos de montage, et des copies d'écrans de résultats, avec une brève description. 