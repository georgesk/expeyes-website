Expériences pour jeunes ingénieurs et scientifiques
###################################################

:slug: welcome
:date: 2018-09-07
:title: Expériences pour jeunes ingénieurs et scientifiques
:lang: fr


.. raw:: html

	 <div class="noborder" style="float: left; margin: 10px 50px 15px 15px;">
.. image:: /images/eyes17-nb2.png
   :width: 406px
   :height: 368px
   :alt: Expeyes17 and a laptop
.. raw:: html

	 </div>


-    Un outil pour étudier les sciences par l'exploration et les expériences.
-    Couvre les `Expériences </fr/category/expts_17.html>`__ aux niveaux
     secondaire et universitaire
-    Oscilloscope 4 canaux, 1Méch/s, calibres jusqu'à +/-16V
-    Générateur BF sinus/triangle, 5 Hz à 5 kHz
-    Sources de tension programmables, +/-5V et +/-3.3V
-    Fréquencemètre et mesures de temps.
-    Support des capteurs au standard I2C
-    Résolution analogique 12bits.
-    Matériel et logiciel libres.
-    Logiciel en langage de programmation Python.
-    démarrez avec le `Manuel Utilisateur </fr/Documents/eyes17-a4-fr.pdf>`__
     et les `Vidéos <https://www.youtube.com/channel/UCIHUjpPn9wf1aHElqLn1RJQ>`__
-    Testé sur Raspberry Pi (`paquet debian </fr/Documents/eyes17-rp-4.0.0.deb>`__)

`Peut être commandé en ligne <https://www.fabtolab.com/expeyes>`__

    ExpEYES est fait par `PHOENIX <http://www.iuac.res.in/%7Eelab/phoenix/>`__,
un projet de
l'`Inter-University Accelerator Centre <http://www.iuac.res.in/>`__,
New Delhi. C'est un combiné de logiciel et de matériel pour développer des
expériences scientifiques, des démonstrations et des projets sans
nécessairement rentrer dans les détails de l'électronique et de la
programmation. Il transforme votre PC en laboratoire de sciences. Le projet
PHOENIX (Physics with Home-made Equipment and Innovative Experiments) a démarré
en 2005 en tant que programme de vulgarisation de l'IUAC, avec les objectifs
de développer un équipement de laboratoire abordable et de former des
professeurs. La conception d'ExpEYES combine la propriété de mesure en temps
réel des micro-contrôleurs avec la facilité et la flexibilité du
`langage de programmation Python </pages/python.html>`__  pour l'analyse
des données et leur visualisation. Il fonctionne aussi comme appareillage de
test pour les passionnés d'électronique et les étudiants ingénieurs.
Le logiciel pour tous les produits de PHOENIX sont distribués sous la
`Licence Publique Générale GNU <https://www.gnu.org/licenses/gpl-3.0.en.html>`__
et le matériel est sous la licence libre
`CERN OHL </fr/Documents/cern_ohl_v_1_2.pdf>`__.

La dernière version d'ExpEYES a été créée en février 2017.  Le manuel
utilisateur est `ICI </fr/Documents/eyes17-a4-fr.pdf>`__.
 Il y a quelques vidéos au sujet d'ExpEYES17
`sur Youtube <https://www.youtube.com/channel/UCIHUjpPn9wf1aHElqLn1RJQ>`__.

L'IUAC organise des programmes de formation pour des enseignants
du secondaire tous les six mois et les candidats sélectionnés sont
suivis complètement. Rejoignez la
`Liste de Diffusion Phoenix <http://www.freelists.org/list/phoenix-project>`__
pour être informé des
`Programmes de Formation <http://www.iuac.res.in/%7Eelab/phoenix/workshops/index.html>`__
et des nouveaux développements.


Nous organisons aussi des « Ateliers d'un jour » dans les
établissements d'enseignement secondaire en fournissant des
personnes ressources, pour plus de détails, cliquer
`ICI  </fr/pages/oneDayWorkshops.html>`__.


Pour l'information au sujet d'ExpEYES Junior, créé en 2012, cliquer
`ICI  </fr/pages/ejun.html>`__.


On trouvera `ICI </fr/pages/compare.html>`__ une comparaison entre
ExpEYES17 et ExpEYES Junior.

