Experiments for Young Engineers and Scientists
##############################################

:slug: welcome
:date: 2018-09-07
:title: Experiments for Young Engineers and Scientists
:lang: en


.. raw:: html

	 <div class="noborder" style="float: left; margin: 10px 50px 15px 15px;">
.. image:: /images/eyes17-nb2.png
   :width: 406px
   :height: 368px
   :alt: Expeyes17 and a laptop
.. raw:: html

	 </div>

-    A tool for learning science by exploring and experimenting.
-    Covers Experiments at Under Graduate and Graduate level
-    4 channel Oscilloscope, 1Msps, +/-16V input range
-    Sine/Triangular Wave Generator, 5Hz to 5kHz
-    Programmable voltage sources, +/5V and +/-3.3V
-    Frequency Counter and time measurements.
-    Supports I2C standard sensors
-    12bit analog resolution.
-    Open Hardware and Free Software.
-    Software in Python programming language.
-    get started with the User Manual and Videos	
-    Tested on Raspberry Pi (`deb file </Documents/eyes17-rp-4.0.0.deb>`__)

`Available Online <https://www.fabtolab.com/expeyes>`__

    ExpEYES is from the `PHOENIX <http://www.iuac.res.in/%7Eelab/phoenix/>`__
project of
`Inter-University Accelerator Centre <http://www.iuac.res.in/>`__, New Delhi.
It is a hardware & software framework for developing science experiments,
demonstrations and projects without getting in to the details of electronics
or computer programming.It converts your PC into a science laboratory.
PHOENIX (Physics with Home-made Equipment and Innovative Experiments)
project was started, in 2005 as a part of IUAC's outreach program,
with the objectives of developing affordable laboratory equipment and
training teachers. Design of ExpEYES combines the real-time measurement
capability of micro-controllers with the ease and flexibility of
`Python programming language </pages/python.html>`__
for data analysis and visualisation.It also functions as a test equipment
for electronics hobbyists and engineering students. Software for all products
from PHOENIX are distributed under
`GNU General Public License <https://www.gnu.org/licenses/gpl-3.0.en.html>`__
and the hardware designs are under
`CERN OHL <http://expeyes.in/Documents/cern_ohl_v_1_2.pdf>`__.


The latest version of ExpEYES has been released in Feb-2017. A User Manual is
`HERE </Documents/eyes17-a4.pdf>`__. Some videos on
ExpEYES17 are
`on Youtube <https://www.youtube.com/channel/UCIHUjpPn9wf1aHElqLn1RJQ>`__.


IUAC conducts training programs for College teachers every six months
and selected candidates are fully supported. Join the
`Phoenix Mailing List <http://www.freelists.org/list/phoenix-project>`__
to be informed about the
`Training Programs <http://www.iuac.res.in/%7Eelab/phoenix/workshops/index.html>`__
and new developments.


We also support "One Day Workshops" organized by colleges by providing
resource persons, for more details on it click
`HERE </pages/oneDayWorkshops.html>`__.


For information about ExpEYES Junior, released in 2012, click
`HERE </pages/ejun.html>`__.


A comparison between ExpEYES17 and ExpEYES Junior is
`HERE </pages/compare.html>`__.
