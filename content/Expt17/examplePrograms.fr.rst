Programmes d'exemple
####################

:slug: Expt17/examplePrograms
:date: 2018-09-07
:title: Programmes d'exemple
:lang: fr
       

Voici de courts morceaux de code qu'on peut lancer depuis l'interface
graphique d'ExpEYES17. Il faut retirer les commentaires des deux
premières lignes si on veut les lancer en ligne de commande.

-  `Lecture de tension <code/readInputs.py.html>`__
-  `Réglage de tensions continues <code/setVoltages.py.html>`__
-  `Capture d'une entrée unique <code/capture1.py.html>`__
-  `Capture de deux entrées <code/capture2.py.html>`__
-  `Capture de quatre entrées <code/capture4.py.html>`__
-  `Signal triangulaire <code/triangularWave.py.html>`__
-  `Forme de signal arbitraire <code/setWave.py.html>`__
-  `Table de forme de signal <code/table.py.html>`__
-  `Signal RC transitoire <code/RCtransient.py.html>`__
-  `Signal RL transitoire <code/RLtransient.py.html>`__
-  `Intégration avec un circuit RC <code/RCintegration.py.html>`__
-  `Décalage avec une diode <code/clipping.py.html>`__
-  `Écrêtage avec une diode <code/clamping.py.html>`__
-  `Redressement double alternance <code/fullwave.py.html>`__
-  `Transformation de Fourier <code/FourierTransform.py.html>`__ 

