Example Programs
################

:slug: Expt17/examplePrograms
:date: 2018-09-07
:title: Example Programs
:lang: en
       
These are the small code fragments that one can run from the ExpEYES17
GUI. You need to uncomment the first 2 lines to run it from the command
prompt.

-  `Reading Voltage <code/readInputs.py.html>`__
-  `Set DC Voltages <code/setVoltages.py.html>`__
-  `Capture Single Input <code/capture1.py.html>`__
-  `Capture Two Inputs <code/capture2.py.html>`__
-  `Capture Four Inputs <code/capture4.py.html>`__
-  `Triangular Waveform <code/triangularWave.py.html>`__
-  `Arbitrary Waveform <code/setWave.py.html>`__
-  `Waveform Table <code/table.py.html>`__
-  `RC Transient <code/RCtransient.py.html>`__
-  `RL Transient <code/RLtransient.py.html>`__
-  `RC Integration <code/RCintegration.py.html>`__
-  `Clipping with Diode <code/clipping.py.html>`__
-  `Clamping with Diode <code/clamping.py.html>`__
-  `Fullwave Rectifier <code/fullwave.py.html>`__
-  `Fourier Transform <code/FourierTransform.py.html>`__ 
