circuits RC, courbe XY
######################

:slug: Expt17/html/XYplot
:date: 2018-09-07
:category: expts_17
:tags: 2Electrical Circuits and LCR elements
:title: circuits RC, courbe XY
:lang: fr
       
Étude d'un circuit RC série
~~~~~~~~~~~~~~~~~~~~~~~~~~~

On applique une tension sinusoïdale à un circuit RC série et on trace
les tensions aux bornes de R et C. La courbe obtenue devient un cercle
quand \$ R=Z_C \$. Le cercle peut apparaître aplati à l'écran, il faut
rechercher la fréquence à laquelle \$X_{max}=Y_{max}\$.

+-------------------+---------------------------------------+
| |image0|          | |image1|                              |
+-------------------+---------------------------------------+
| Schéma de câblage | Photo du montage expérimental.        |
+-------------------+---------------------------------------+

|image2|

.. |image0| image:: schematics/RCsteadystate.svg
.. |image1| image:: photos/RCsteadystate.png
.. |image2| image:: screenshots/rc-xyplot.png

