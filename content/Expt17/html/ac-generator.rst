Simple AC generator
###################

:slug: Expt17/html/ac-generator
:date: 2018-09-07
:category: schoollevel
:tags: 1School Level Activities
:title: Simple AC generator
:lang: en
       
Schematic
~~~~~~~~~

|image0|

-  A rotating magnet causes a rotating magnetic field also.
-  Placing a coil in that field will induce a periodic voltage across
   it.

Instructions
~~~~~~~~~~~~

-  Use the DC motor to rotate the magnet. The coils are provided with
   ExpEYES.
-  Make the connections as shown:
-  The frequency of the induced AC will be the number of rotations per
   second of the magnet.
-  When you use two coils, the phase difference between the two signals
   is decided by the angle between the axes of the coils.
-  Adjust the time base to view 4 or 5 cycles

.. |image0| image:: schematics/ac-generator.svg
   :width: 500px
