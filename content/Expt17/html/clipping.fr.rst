Limiteur à Diode
################

:slug: Expt17/html/clipping
:date: 2018-09-07
:category: expts_17
:tags: 1Electronics
:title: Limiteur à diode
:lang: fr
	

Le générateur BF **WG** est réglé pour fournir un signal sinusoïdal de
1 kHz. Il est visualisé par le canal A1 de l'oscilloscope. Le signal
après la résistance de \$ 10 k\\Omega \$ est visualisée par A2. Afin de
limiter le signal sinusoïdal on fournit un source de tension continue
en PV1. Comme la limitation se produit dans la partie positive du signal
alternatif, c'est l'amplitude positive qui subit la limitation.

+----------------------+--------------------------------------+
| |image0|             | |image1|                             |
+----------------------+--------------------------------------+
| Diagramme de câblage | Photo du montage                     |
+----------------------+--------------------------------------+

|                                                                           

+-----------------------------------------------------------------------+
| |image3|                                                              |
+-----------------------------------------------------------------------+
| Copie d'écran de l'oscilloscope qui montre les entrées et sorties du  |
| limiteur à diode. La diode utilisée est une 1N4148, à 1000 Hz         |
| avec une résistance de charge de \$ 1 k\\Omega \$.                    |
+-----------------------------------------------------------------------+

Écrire un code en Python

On peut aussi faire cette expérience en lançant ce
`Code Python <../code/capture2.py>`__. Le résultat du programme est montré
ci-dessous.

+----------------------------------------------------------+--------------------+
| ..  code-block:: python                                  | |image4|           |
|                                                          |                    |
|    import eyes17.eyes                                    |                    |
|    p = eyes17.eyes.open()                                |                    |
|    from pylab import *                                   |                    |
|    p.set_sine(200)                                       |                    |
|    p.set_pv1(1.35) # limite à 1.35 + seuil de la diode   |                    |
|    t,v, tt,vv = p.capture2(500, 20) # capture A1 et A2   |                    |
|    xlabel('Time(mS)') ylabel('Voltage(V)')               |                    |
|    plot([0,10], [0,0], 'black')                          |                    |
|    ylim([-4,4])                                          |                    |
|    plot(t,v,linewidth = 2, color = 'blue')               |                    |
|    plot(tt, vv, linewidth = 2, color = 'red')            |                    |
|    show()                                                |                    |
+----------------------------------------------------------+--------------------+
| Programme Python du limiteur à diode                     | Résultat           |
+----------------------------------------------------------+--------------------+

.. |image0| image:: schematics/clipping.svg
.. |image1| image:: photos/clipping.png
.. |image2| image:: screenshots/clipping.png
.. |image3| image:: screenshots/clipping.png
.. |image4| image:: screenshots/clipping3.png

