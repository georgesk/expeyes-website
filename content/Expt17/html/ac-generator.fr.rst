Générateur alternatif simple
############################

:slug: Expt17/html/ac-generator
:date: 2018-09-07
:category: schoollevel
:tags: 1School Level Activities
:title: Générateur alternatif simple
:lang: fr
       

Schéma
~~~~~~

|image0|

-  Un aimant tournant cause un champ magnétique tournant aussi.
-  Si on place une bobine dans ce champ une tension périodique sera
   induite, à ses bornes.

Instructions
~~~~~~~~~~~~

-  Utiliser le moteur à courant continu pour faire tourner l'aimant. Les
   bobines sont fournies avec ExpEYES.
-  Réaliser le connexions comme montré.
-  La fréquence de la tension alternative induite est le nombre de tours
   de l'aimant par seconde.
-  Quand on utilise deux bobines, la différence de phase entre les deux
   bobines est gouvernée par l'angle que font les axes des deux bobines.
-  Ajuster la base de temps pour voir 4 ou 5 cycles.

.. |image0| image:: schematics/ac-generator.svg
   :width: 500px
