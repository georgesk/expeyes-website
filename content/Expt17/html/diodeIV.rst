Diode IV Characteristic
#######################

:slug: Expt17/html/diodeIV
:date: 2018-09-07
:category: expts_17
:tags: 1Electronics
:title: Diode Characteristics
:lang: en
       
The schematic is wired as shown in the diagram below. The voltage across
the diode is measured on A1. The anode of the diode is connected to PV1,
through a 1k resistor. Voltage at PV1 is incremented in steps and at
each point the voltage across the diode is measured. The current is
calculated from \$ i = \\frac{PV_1-A_1}{R} \$.
The diode used is 1N4148, silicon diode.

+-----------------------------------+-----------------------------------+
| |image2|                          | |image3|                          |
+-----------------------------------+-----------------------------------+
| Wiring Diagram                    | Photograph of the experimental    |
|                                   | setup.                            |
+-----------------------------------+-----------------------------------+


+----------------------------------------+
| |image4|                               |
+----------------------------------------+
| Screen shot of Diode IV characteristic |
+----------------------------------------+

| 

.. |image0| image:: schematics/diode_iv.svg
.. |image1| image:: photos/diodeIV.png
.. |image2| image:: schematics/diode_iv.svg
.. |image3| image:: photos/diodeIV.png
.. |image4| image:: screenshots/diode-iv.png

