Numériser un son
################

:slug: Expt17/html/sound-capture
:date: 2018-09-07
:category: schoollevel
:tags: 1School Level Activities
:title: Numériser un son
:lang: fr
       
Schéma
~~~~~~

|image0|

Instructions
~~~~~~~~~~~~

-  Réaliser les connexions. Activer le canal MIC.
-  Faire un son à l'aide d'instruments comme une flûte ou
   un sifflet.
-  Ajuster la base de temps pour voir des courbes nettes.
-  Utiliser la FFT pour faire une analyse fréquentielle.

.. |image0| image:: schematics/sound-capture.svg
   :width: 500px
