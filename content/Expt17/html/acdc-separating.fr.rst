Séparer continu et alternatif
#############################

:slug: Expt17/html/acdc-separating
:date: 2018-09-07
:category: schoollevel
:tags: 1School Level Activities
:title: Séparer continu et alternatif
:lang: fr
       
L'objectif de cette expérience est de montrer qu'un signal peut être
une combinaison de continu et d'alternatif. On peut séparer la part
alternative à l'aide d'un condensateur, comme celui-ci bloque le
courant continu. On va utiliser un signal carré oscillant entre
0 et 5 V. C'est une combinaison de signal alternatif de 2,5 V
crête et de tension continue à 2,5 V.

Schéma
~~~~~~

|image0|

Instructions
~~~~~~~~~~~~

-  La sortie de SQ1 oscille entre 0 et 5 V comme le montre le canal
   A1.
-  Cela signifie qu'il a une valeur moyenne de 2,5 V, qui est sa
   composante continue.
-  Après passage dans un condensateur, la tension oscille entre -2,5 V
   et +2,5 V, on a récupéré la composante alternative.
-  La composante continue est bloquée par le condensateur.
-  Il faut connecter une résistance de 100 kΩ entre A2 et la masse pour
   retirer proprement la composante continue.

.. |image0| image:: schematics/acdc-separating.svg
   :width: 500px
