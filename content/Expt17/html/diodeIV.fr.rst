Caractéristique courant-tension d'une diode
###########################################

:slug: Expt17/html/diodeIV
:date: 2018-09-07
:category: expts_17
:tags: 1Electronics
:title: Caractéristique courant-tension d'une diode
:lang: fr
       
Câbler le montage selon le schéma ci-dessous. La tension aux bornes
de la diode est mesurée par A1. L'anode de la diode est connectée à
PV1, à travers une résistance de 1 kΩ. On augmante la tension de PV1
par paliers et à chaque fois la tension aux bornes de la diode est
mesurée. Le courant est calculé par \$ i = \\frac{PV_1-A_1}{R} \$.
La diode utilisée est 1N4148, c'est une diode au silicium.

+-------------------+---------------------------------------+
| |image2|          | |image3|                              |
+-------------------+---------------------------------------+
| Schéma de câblage | Photo du montage expérimental.        |
+-------------------+---------------------------------------+


+----------------------------------------+
| |image4|                               |
+----------------------------------------+
| Copie d'écran de la caractéristique    |
| courant-tension d'une diode            |
+----------------------------------------+


.. |image0| image:: schematics/diode_iv.svg
.. |image1| image:: photos/diodeIV.png
.. |image2| image:: schematics/diode_iv.svg
.. |image3| image:: photos/diodeIV.png
.. |image4| image:: screenshots/diode-iv.png

