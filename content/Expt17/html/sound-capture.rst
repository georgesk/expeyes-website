Digitizing Sound
################

:slug: Expt17/html/sound-capture
:date: 2018-09-07
:category: schoollevel
:tags: 1School Level Activities
:title: Digitizing Sound
:lang: en
       
Schematic
~~~~~~~~~

|image0|

Instructions
~~~~~~~~~~~~

-  Make connections. Enable the channel MIC.
-  Make sound using instruments like flute, or a whistle.
-  Adjust the time base to view clear traces.
-  Use FFT to do a frequency analysis.

.. |image0| image:: schematics/sound-capture.svg
   :width: 500px
