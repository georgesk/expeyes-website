NPN Transistor Amplifier, CE configuration
##########################################

:slug: Expt17/html/npnCEamp
:date: 2018-09-07
:category: expts_17
:tags: 1Electronics
:title: Transistor Amplifier, CE
:lang: en
       
The schematic is wired as shown in the diagram below. It is very similar
to the schematic used for drawing the output characteristics. The AC
signal is connected to the base, through a capacitor so that the biasing
is not affected. Since the gain is high, we need a small input signal,
smaller than the 80 mV available from WG. A divider network using 1k and
2.2k gives  around 20 mV output and that is fed to the base. This input
signal is monitored on A2.  The DC operating point is decided by the
voltage applied to the base through the 100k resistor. By adjusting PV2,
we can take the transistor between cut-off and saturation conditions.
The value of PV2 is adjusted to get an output with minimum distortion.
To improve it further, one can reduce the input signal, use a higher
collector supply voltage instead of PV1, or use a transistor with a
lower gain value.

 

+-----------------------------------+-----------------------------------+
| |image2|                          | |image3|                          |
+-----------------------------------+-----------------------------------+
| Wiring Diagram                    | Photograph of the experimental    |
|                                   | setup. Transistor is  2N2222      |
+-----------------------------------+-----------------------------------+

|                                                                           

+-----------------------------------------------------------------------+
| |image5|                                                              |
+-----------------------------------------------------------------------+
| Screen shot of the input and output waveforms of Transistor Amplifier |
| in CE configuration                                                   |
+-----------------------------------------------------------------------+

| 

.. |image0| image:: schematics/npn_ce_amp.svg
.. |image1| image:: photos/npn-ce-amp.jpg
.. |image2| image:: schematics/npn_ce_amp.svg
.. |image3| image:: photos/npn-ce-amp.jpg
	    :width: 631px
	    :alt: setup photography
.. |image4| image:: screenshots/npn-ce-amp.png
.. |image5| image:: screenshots/npn-ce-amp.png

