Réponse en fréquence d'un buzzer piézo
######################################

:slug: Expt17/html/soundFreqResp
:date: 2018-09-07
:category: expts_17
:tags: 3Sound (velocity and beats)
:title: Réponse en fréquence d'un buzzer piézo
:lang: fr
       
Schéma
~~~~~~

|image0|

-  Tout objet mécanique a une fréquence de résonance, gouvernée par
   ses dimensions et ses propriétés d'élasticité. La fréquence de résonance
   du disque piézo-électrique fourni avec ExpEYES est proche de 3500 Hz.
   Elle peut changer d'un échantillon à l'autre.
-  Quand on applique une signal électrique le disque piézo-électrique
   subit des oscillations et produit du son.
-  L'amplitude des oscillations est maximale quand la fréquence de l'excitation
   est égale à la fréquence de résonance.

Instructions
~~~~~~~~~~~~

-  Réaliser les connexions. Fixer le microphone en face du buzzer, à quelques
   4 ou 5 cm.
-  Cliquer le bouton **START**
-  L'amplitude du signal du microphone est représentée en fonction de la
   fréquence.

.. |image0| image:: schematics/sound-freq-resp.svg
   :width: 500px

