Pile à citron
#############

:slug: Expt17/html/lemon-cell
:date: 2018-09-07
:category: schoollevel
:tags: 1School Level Activities
:title: Pile à citron
:lang: fr
       
Schéma
~~~~~~

|image0|

-  Les piles sont basées sur des réactions électro-chimiques. Typiquement,
   la réaction se produit au niveau de deux morceaux de métal, nommés
   électrodes, et un liquide ou un gel, nommé électrolyte.
-  On peut utliser de nombreux fruits et liquides comme électrolyte
   acide.
-  Des plaques de cuivre et zinc sont utilisés comme électrodes.

Instructions
~~~~~~~~~~~~

-  Faire une pile au citron et la connecter comme montré sur la figure,
   mesure la tension A1.
-  Connecter la résistance de 1 kΩ et mesurer la tension à nouveau. La
   tension diminue à cause de la résistance interne de la pile.
-  Essayer de recommencer ave une pile sèche de 1,5 V.

.. |image0| image:: schematics/lemon-cell.svg
   :width: 500px
