Generating Sound
================

:slug: Expt17/html/sound-generator
:date: 2018-09-07
:category: schoollevel
:tags: 1School Level Activities
:title: Generating Sound
:lang: en
       
Schematic
#########


|image0|

Instructions
############


-  Make connections as shown in the figure.
-  Change the frequency of WG and listen to the sound.
-  The sound is very high at some frequency. This is due to the
   resonance of the Piezo disk.

.. |image0| image:: schematics/sound-generator.svg
   :width: 500px
