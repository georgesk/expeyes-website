Velocity of Sound
#################

:slug: Expt17/html/soundVelocity
:date: 2018-09-07
:category: expts_17
:tags: 3Sound (velocity and beats)
:title: Velocity of Sound
:lang: en
      
The frequency waveform generator WG is set to the resonant frequency of
the Piezo, for maximum sound output. The compressions and expansions of
the Piezo generates traveling high and low pressure regions in front of
it. The microphone digitizes the pressure and plots it as a function of
time. The oscilloscope is triggered by the sine wave driving the Piezo.
When the microphone is moved, the phase difference between the two
waveforms changes. For half wavelength the phase difference is 180
degree. A connection diagram is shown below.

|image0|

The distance is adjusted to make the driving signal and microphone
signal  in phase, as shown below.

+-----------------------------------+-----------------------------------+
| |image3|                          | |image4|                          |
+-----------------------------------+-----------------------------------+
| Distance adjusted for zero phase  | Sine wave driving the Piezo and   |
| difference                        | the microphone output             |
+-----------------------------------+-----------------------------------+


The distance is again changed to make the driving signal and microphone
signal out of phase, as shown below. The change in distance 'd' is
measured and the velocity of sound is given by v = 2 \* d \* 3500


+-----------------------------------+-----------------------------------+
| |image7|                          | |image8|                          |
+-----------------------------------+-----------------------------------+
| Distance adjusted for zero phase  | Sine wave driving the Piezo and   |
| difference                        | the microphone output             |
+-----------------------------------+-----------------------------------+


.. |image0| image:: schematics/sound-velocity.svg
   :width: 500px
.. |image3| image:: photos/sound-vel-1.jpg
.. |image4| image:: screenshots/sound-velocity-zero.png
.. |image7| image:: photos/sound-vel-2.jpg
   :width: 400px
.. |image8| image:: screenshots/sound-velocity180.png

