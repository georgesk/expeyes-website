Plot I-V curve
##############

:slug: Expt17/html/plotIV
:date: 2018-09-07
:category: expts_17
:tags: 2Electrical Circuits and LCR elements
:title: Plot I-V curve
:lang: en

Plotting of Current vs Voltage, for resistors
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Two resistances are connected as shown in the diagram. R2 (generally 1 kΩ)
is used for measuring the current indirectly, ie from the voltage drop
across it. The voltage at PV1 can be set using a slider. The displayed
voltage is the voltage across R1. The current also will be displayed.
The voltage can be varied within selected values and IV plots can be
generated.

|image0|

The screen shot shows the graphs for a 2.2 kΩ and 10 kΩ resistors along
with a PN junction. The resistor is a linear element but the PN
junction is non-linear, as evident from the plot.

|image1|

.. |image0| image:: schematics/res-compare.svg
   :width: 631px
.. |image1| image:: screenshots/plot-iv.png

