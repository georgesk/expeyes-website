Caractéristiques d'un transistor en émetteur commun (NPN et PNP)
################################################################

:slug: Expt17/html/npnCEout
:date: 2018-09-07
:category: expts_17
:tags: 1Electronics
:title: Caractéristiques d'un transistor
:lang: fr
       
Faire le câblage selon le schéma ci-dessous. Le courant de base est
contrôlé par PV2, via une résistance de 100 kΩ. La tension de base est
mesurée pour calculer le courant de base par
\$I_b = \\frac{PV2-A2}{1 \\times 10^5}\$. La tension du collecteur est
suivie par A1. Le collecteur est relié à PV1, via une résistance de
1 kΩ. Pour un courant de base donné, la tension en PV1 est augmentée par
étapes et la tension de collecteur est mesurée. Le courant de collecteur
est calculé par \$I_c = \\frac{PV1-A1}{1000}\$. Le transistor utilisé est
un 2N2222, dont le gain en courant est proche de 200.

+-----------------------------------+-----------------------------------+
| |image2|                          | |image3|                          |
+-----------------------------------+-----------------------------------+
| Schéma de câblage                 | Photo du montage expérimental.    |
|                                   | Le transistor est un 2N2222.      |
+-----------------------------------+-----------------------------------+

|                                                                           

+----------------------------------------+
| |image4|                               |
+----------------------------------------+
| Copie d'écran de caractéristiques du   |
| transistor                             |
+----------------------------------------+


Les caractéristiques d'un transistor PNP (2N3906) sont présentées
ci-dessous.

|image5|

.. |image0| image:: schematics/npn_ce_out.svg
.. |image1| image:: photos/npnCEout.png
.. |image2| image:: schematics/npn_ce_out.svg
.. |image3| image:: photos/npnCEout.png
.. |image4| image:: screenshots/npn-screenshot.png
.. |image5| image:: screenshots/pnp-ce-char.png

