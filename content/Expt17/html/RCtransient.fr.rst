Réponse transitoire d'un circuit RC
###################################

:slug: Expt17/html/RCtransient
:date: 2018-09-07
:category: expts_17
:tags: 2Electrical Circuits and LCR elements
:title: Réponse transitoire RC
:lang: fr
       
Dans cette section, on explore la réponse transitoire d'un circuit RC
en lui appliquant un échelon de tension et en capturant la variation
de tension aux bornes du condensateur.

+-------------------+---------------------------------------+
| |image0|          | |image1|                              |
+-------------------+---------------------------------------+
| Schéma de câblage | Photo du montage expérimental.        |
+-------------------+---------------------------------------+


Les échelons de tension sont créés sur OD1 en cliquant sur les boutons
de l'interface graphique. Les signaux observés sont présentés ci-dessous,
montrant la variation de tension aux bornes du condensateur pendant
la charge et la décharge à travers une résistance.

+-----------------------------------------------------------------------+
| |image3|                                                              |
+-----------------------------------------------------------------------+
| Copie d'écran de la tension aux bornes du condensateur pendant        |
| la charge et la décharge                                              |
+-----------------------------------------------------------------------+

.. |image0| image:: schematics/RCtransient.svg
.. |image1| image:: photos/RCtransient.png
.. |image2| image:: screenshots/rctransient.png
.. |image3| image:: screenshots/rctransient.png

