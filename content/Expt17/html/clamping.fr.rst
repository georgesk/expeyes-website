Décalage à l'aide d'une diode
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

:slug: Expt17/html/clamping
:date: 2018-09-07
:category: expts_17
:tags: 1Electronics
:title: Décalage à l'aide d'une diode
:lang: fr
       
Le générateur BF WG est réglé pour fournir un signal sinusoïdal de
fréquence 1 kHz. On le suit sur le canal A1 de l'oscilloscope. Le
signal après le condensateur de 1 µF est suivi par A2. La tension continue
qui sert à décaler le signal sinusoïdal est donnée par PV1, *via* une diode.
On règle le calibre de A2 à 16 V, car la tension après décalage de 5 V
peut dépasser 8 V.

+-------------------+---------------------------------------+
| |image0|          | |image1|                              |
+-------------------+---------------------------------------+
| Schéma de câblage | Photo du montage expérimental.        |
+-------------------+---------------------------------------+

                                                                          

+-----------------------------------------------------------------------+
| |image3|                                                              |
+-----------------------------------------------------------------------+
| Copie d'écran de l'oscilloscope montrant les signaux d'entrée et de   |
| sortie du décalage positif avec une diode. La diode est une 1N4148,   |
| la fréquence est 1000 Hz. La charge est l'impédance d'entrée du       |
| canal A2, soit 1 MΩ.                                                  |
+-----------------------------------------------------------------------+


Écrire du code Python
~~~~~~~~~~~~~~~~~~~~~

On peut aussi faire cette expérience en lançant ce
`code Python <../code/capture2.py>`__. Le résultat du programme est montré
ci-dessous.

+----------------------------------------------------------+--------------------+
| ..  code-block:: python                                  | |image4|           |
|                                                          |                    |
|    import eyes17.eyes                                    |                    |
|    p = eyes17.eyes.open()                                |                    |
|    from pylab import *                                   |                    |
|    p.set_sine(200)                                       |                    |
|    p.set_pv1(1.35)                                       |                    |
|    t,v, tt,vv = p.capture2(500, 20) # capture A1 and A2  |                    |
|    xlabel('Temps (ms)') ylabel('Tension (V)')            |                    |
|    plot([0,10], [0,0], 'black')                          |                    |
|    ylim([-4,4])                                          |                    |
|    plot(t,v,linewidth = 2, color = 'blue')               |                    |
|    plot(tt, vv, linewidth = 2, color = 'red')            |                    |
|    show()                                                |                    |
+----------------------------------------------------------+--------------------+
| Programme Python pour le décalage à diode                | Résultat du code   |
+----------------------------------------------------------+--------------------+

.. |image0| image:: schematics/clamping.svg
.. |image1| image:: photos/clamping.jpg
.. |image2| image:: screenshots/clamping.png
.. |image3| image:: screenshots/clamping.png
.. |image4| image:: screenshots/clamping-mpl.png
	:width: 500px

