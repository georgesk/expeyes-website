Amplificateur à transistor NPN, en Émetteur commun
##################################################

:slug: Expt17/html/npnCEamp
:date: 2018-09-07
:category: expts_17
:tags: 1Electronics
:title: Amplificateur à transistor, en Émetteur commun
:lang: fr
       
Faire le montage selon le schéma de câblage ci-dessous. Il est très
semblable au schéma utilisé pour la caractéristique On connecte le
signal alternatif à la base, *via* un condensateur afin de ne pas
modifier la polarisation. Comme le gain est élevé, il faut un signal
faible, inférieur aux 80 mV disponibles sur WG. Un diviseur de tension
composé d'une résistance de 1 kΩ et d'une de 2,2 kΩ donnera environ
20 mV à injecter dans la base. Le point de fonctionnement en continu
est réglé par la tension appliquée à la base grâce à la résistance de
100 kΩ. En ajustant PV2, on peut placer le transistor dans des conditions
intermédiaires entre le blocage et la saturation. On ajuste la valeur
de PV2 pour récupérer un signal de sortie avec la plus petite
distorsion possible. Pour l'améliorer encore, on peut diminuer le signal
d'entrée, utiliser une tension d'alimentation au collecteur supérieure,
ou utiliser un transistor de gain moindre.


+-----------------------------------+-----------------------------------+
| |image2|                          | |image3|                          |
+-----------------------------------+-----------------------------------+
| Schéma de câblage                 | Photo du montage expérimental.    |
|                                   | Le transistor est un 2N2222       |
+-----------------------------------+-----------------------------------+

                                                                          

+-----------------------------------------------------------------------+
| |image5|                                                              |
+-----------------------------------------------------------------------+
| Copie d'écran des signaux d'entrée et de sortie du transistor         |
| en configuration émetteur commun                                      |
+-----------------------------------------------------------------------+

| 

.. |image0| image:: schematics/npn_ce_amp.svg
.. |image1| image:: photos/npn-ce-amp.jpg
.. |image2| image:: schematics/npn_ce_amp.svg
.. |image3| image:: photos/npn-ce-amp.jpg
	    :width: 631px
	    :alt: setup photography
.. |image4| image:: screenshots/npn-ce-amp.png
.. |image5| image:: screenshots/npn-ce-amp.png

