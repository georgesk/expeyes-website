Mesure de distance à l'aide d'un capteur piézo HY-SR04
######################################################

:slug: Expt17/html/sr04dist
:date: 2018-09-07
:category: expts_17
:tags: 4Mechanics and Thermal etc.
:title: Distance par un écho (SR04)
:lang: fr
       

+-----------------------+-----------------------+-----------------------+
| le HY-SR04 est un     | |image2|              | |image3|              |
| dispositif de mesure  |                       |                       |
| à ultrasons facile à  |                       |                       |
| se procurer. Il       |                       |                       |
| consiste en deux      |                       |                       |
| cristaux piézo à 40   |                       |                       |
| kHz, l'un agissant    |                       |                       |
| comme émetteur et     |                       |                       |
| l'autre comme         |                       |                       |
| récepteur, et un      |                       |                       |
| circuit électronique  |                       |                       |
| pour mesurer          |                       |                       |
| l'intervalle de temps |                       |                       |
| entre une salve de    |                       |                       |
| son émis et la        |                       |                       |
| réception de l'écho   |                       |                       |
| la salve. Les quatre  |                       |                       |
| bornes sont           |                       |                       |
| l'alimentation, la    |                       |                       |
| masse, la gachette    |                       |                       |
| et l'écho. On peut    |                       |                       |
| connecter ce module à |                       |                       |
| ExpEYES-17 comme      |                       |                       |
| montré sur le schéma, |                       |                       |
| pour mesurer la       |                       |                       |
| distance entre le     |                       |                       |
| module et la surface  |                       |                       |
| qui reflète l'écho    |                       |                       |
| avec une erreur       |                       |                       |
| inférieure à 3 mm.    |                       |                       |
| On peut l'utiliser    |                       |                       |
| pour concevoir        |                       |                       |
| plusieurs             |                       |                       |
| expériences en        |                       |                       |
| mécanique.            |                       |                       |
+-----------------------+-----------------------+-----------------------+

Le problème de masse et ressort
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Quand on suspend une masse à un ressort et qu'on la fait osciller, la
période de l'oscillation est donnée par
\$T = 2 \\times \\pi \\times \\sqrt{\\frac{m}{k}}\$,
où \$m\$ est la masse et \$k\$ est la constante de raideur du ressort.
On peut mesurer la période d'oscillation en mesurant la distance à la
masse en mouvement en fonction du temps. La courbe de la distance en fonction
du temps est modélisée par une sinusoïde pour calculer la fréquence.
La photographie ci-dessous montre un plateau métallique suspendu à un
ressort et le SR04 qui lui fait face par dessous. La distance au
plateau oscillant est mesurée durant 5 secondes et les données sont
modélisées par une fonction sinusoïdale.

|image4| Une vidéo de l'expérience est `ICI <https://www.youtube.com/watch?v=FhCYhDiIRuQ>`__.

Pendule simple
~~~~~~~~~~~~~~

La péride des oscillations d'un pendule peut aussi ere mesurée
à l'aide d'un SR04 connecté à ExpEYES. La figure montre un
pendule avec un poids rectangulaire, mais on peut aussi utiliser
un poids sphérique avec un morceau de papier collé dessus
(pour fournir une surface plane réfléchissante). La fréquence
mesurée est 1,05 Hz pour un pendule de longueur 22 cm. On peut
calculer la valeur de l'accélération de la pesanteur à partir
de ces données.
  
|image5| Une vidéo est `ICI <https://www.youtube.com/watch?v=fOTfMsKiXBo>`__.

Chute libre, déplacement en fonction du temps
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

C'est possible de mesure la distance à un objet tombant sous l'action
de la gravité. la valeur de \$g\$ peut être calculée en modélisant les
données avec un polynôme \$S(t)= a \\times t^2 + b \\times t + c\$

$\g\$ est donné par \$2 \\times a\$


.. |image0| image:: photos/sr04.jpg
.. |image1| image:: photos/sr04-dist.png
.. |image2| image:: photos/sr04.jpg
.. |image3| image:: photos/sr04-dist.png
.. |image4| image:: photos/massAndSpring.jpg
   :width: 100%
.. |image5| image:: photos/pendulum.jpg
   :width: 100%

