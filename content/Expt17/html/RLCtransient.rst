Transient response of RLC circuit
#################################

:slug: Expt17/html/RLCtransient
:date: 2018-09-07
:category: expts_17
:tags: 2Electrical Circuits and LCR elements
:title: RLC transient response
:lang: en

In this section, we explore the transient response of a series RLC
circuit by applying voltage step and capturing the voltage variation
across the capacitor. The coil used is 3000 turns of 44SWG wire. The
inductance is around 120 mH and the resistance is around 550 Ohms. The
capacitance value is 0.1 uF.

+------------------+---------------------------------------+
| |image0|         | |image1|                              |
+------------------+---------------------------------------+
| Wiring Diagram   | Photograph of the experimental setup. |
+------------------+---------------------------------------+


The observed waveforms are shown below. It shows that the circuit is
under damped. The frequency and damping factor are calculated by fitting
the data the a  sinusoid multiplied by an exponential function. The
damping can be increased by adding more resistance or by increasing the
C/L ratio.

+--------------------------------------------------------------------+
| |image2|                                                           |
+--------------------------------------------------------------------+
| Screen shot of the transient response of under damped LCR circuit. |
+--------------------------------------------------------------------+

.. |image0| image:: schematics/RLCtransient.svg
.. |image1| image:: photos/RLCtransient.png
.. |image2| image:: screenshots/rlctransient.png

