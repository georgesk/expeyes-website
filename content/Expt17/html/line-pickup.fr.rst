Signal d'antenne dû au secteur
##############################

:slug: Expt17/html/line-pickup
:date: 2018-09-07
:category: schoollevel
:tags: 1School Level Activities
:title: Signal d'antenne dû au secteur
:lang: fr
       
|image0|

Explorer le signal d'antenne dû au secteur
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

-  Connecter un fil long à A3.
-  Activer A3 et son option d'analyse.
-  Régler la base de temps pour avoir 10 ms/carreau.
-  Toucher l'extrémité libre du fil.
-  Approcher l'extrémité libre du fil d'un câble du secteur

.. |image0| image:: schematics/line-pickup.svg
   :width: 500px
