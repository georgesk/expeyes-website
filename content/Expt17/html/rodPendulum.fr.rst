L'accélaration de la gravité, mesurée par un pendule
####################################################

:slug: Expt17/html/rodPendulum
:date: 2018-09-07
:category: expts_17
:tags: 4Mechanics and Thermal etc.
:title: Gravité, par pendule
:lang: fr
       
On fait osciller le pendule entre une DEL et un photo-transistor.
La période est mesurée en surveillant de signal du photo-transistor.
La réolution de la mesure est proche de 100 µs, si bien que de petites
variations de la période avec l'amplitude peuvent être étudiées.

+----------+----------+
| |image0| | |image1| |
+----------+----------+

Ci-dessous, une barrière photo-électrique avec des lames de couteau pour supporter un pendule cylindrique.

|image2|

.. |image0| image:: schematics/rod-pendulum.svg
.. |image1| image:: photos/rod-pend.jpg
.. |image2| image:: photos/rod-pend-LB.jpg

