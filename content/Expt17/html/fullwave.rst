Full wave rectifier
###################

:slug: Expt17/html/fullwave
:date: 2018-09-07
:category: expts_17
:tags: 1Electronics
:title: Full wave rectifier
:lang: en
       
The implementation of full wave rectifier requires two AC waveforms,
having 180 degree phase difference. Generally it is done using a
transformer with center tap. We are using the WG and WG bar outputs for
the same. WG is monitored by oscilloscope channel A1 and WG bar on A2.
The rectified output is connected to A3. The observed waveform will be
a bit noisy without the load resistor, connecting a 1k resistor gives a
clean rectified waveform. The voltage drop across the diode is clearly
visible. Connect different values of capacitors to view the filtering
effect.

+------------------+--------------------------------------+
| |image0|         | |image1|                             |
+------------------+--------------------------------------+
| Wiring Diagram   | Photograph of the experimental setup |
+------------------+--------------------------------------+

|                                                                           

+-----------------------------------------------------------------------+
| |image3|                                                              |
+-----------------------------------------------------------------------+
| Screen shot of the oscilloscope program showing inputs and output of  |
| full wave rectifier. 1N4148 diode at 1000Hz and 1kOhm load resistor.  |
+-----------------------------------------------------------------------+

| 
| Output with R = 1kΩ and C = 1µF is shown below. It can be seen that
  the frequency of ripple in the case of full wave rectifier is double
  the frequency of the input.
| |image4|

.. |image0| image:: schematics/fullwave.svg
.. |image1| image:: photos/fullwave.jpg
.. |image2| image:: screenshots/fullwave.png
.. |image3| image:: screenshots/fullwave.png
.. |image4| image:: screenshots/fullwave-filter.png

