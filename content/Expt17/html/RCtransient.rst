Transient response of RC circuit
################################

:slug: Expt17/html/RCtransient
:date: 2018-09-07
:category: expts_17
:tags: 2Electrical Circuits and LCR elements
:title: RC transient response
:lang: en
       

In this section, we explore the transient response of a series RC
circuit by applying voltage step and capturing the voltage variation
across the capacitor.

+------------------+---------------------------------------+
| |image0|         | |image1|                              |
+------------------+---------------------------------------+
| Wiring Diagram   | Photograph of the experimental setup. |
+------------------+---------------------------------------+


The voltage steps are generated on OD1 by clicking on the buttons on the
GUI. The observed waveforms are shown below, showing the voltage
variation across a capacitor while charging and discharging through a
resistor.

+-----------------------------------------------------------------------+
| |image3|                                                              |
+-----------------------------------------------------------------------+
| Screen shot of the voltage across a capacitor during charging and     |
| discharging                                                           |
+-----------------------------------------------------------------------+

.. |image0| image:: schematics/RCtransient.svg
.. |image1| image:: photos/RCtransient.png
.. |image2| image:: screenshots/rctransient.png
.. |image3| image:: screenshots/rctransient.png

