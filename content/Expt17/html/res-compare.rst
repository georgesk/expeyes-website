Resistance by Ohm's law
#######################

:slug: Expt17/html/res-compare
:date: 2018-09-07
:category: schoollevel
:tags: 1School Level Activities
:title: Resistance by Ohm's law
:lang: en
       
Schematic
~~~~~~~~~


|image0|

Instructions
~~~~~~~~~~~~


-  Connect the resistors as shown in the figure.
-  Current through the circuits is I = PV1 / (R1 + R2)
-  As per Ohm's law, voltage at A1 will be I x R1

.. |image0| image:: schematics/res-compare.svg
   :width: 500px
