Half wave rectifier
###################

:slug: Expt17/html/halfwave
:date: 2018-09-07
:category: expts_17
:tags: 1Electronics
:title: Half wave rectifier
:lang: en
       
The waveform generator WG is set to give a sine wave of 1kHz. It is
monitored by oscilloscope channel A1. The signal after the diode is
monitored by A2. The observed waveform will be a bit noisy without the
load resistor, connecting a 1k resistor gives a clean rectified
waveform. The voltage drop across the diode is clearly visible. Connect
different values of capacitors to view the filtering effect.

+------------------+--------------------------------------+
| |image0|         | |image1|                             |
+------------------+--------------------------------------+
| Wiring Diagram   | Photograph of the experimental setup |
+------------------+--------------------------------------+

|                                         

+-----------------------------------------------------------------------+
| |image3|                                                              |
+-----------------------------------------------------------------------+
| Screen shot of the oscilloscope program showing input and output of   |
| half wave rectifier.  1N4148 diode at 1000Hz and 1kOhm load resistor. |
+-----------------------------------------------------------------------+


The output after adding a 1µF capacitor for filtering is shown below.
With increasing RC value, the ripple reduces.

| |image4|

Effect of Diode Junction Capacitance:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Every diode has a junction capacitance that acts like a capacitance
connected in parallel to the ideal PN junction. Junction capacitance of
1N4148 is only 4pF but 1N4007 has a junction capacitance of 20pF. The
rectified output, without external load resistor, for 1N4007 is shown
below. The 1MOhm input impedance of channel A2 will be always present.

| 
| |image5|
|

Writing Python Code
~~~~~~~~~~~~~~~~~~~

| This experiment can also be done by running this `Python
  Code <../code/capture2.py>`__. The output of the program is shown
  below.

+--------------------------------------------------------------+--------------------+
| .. code-block:: python                                       | |image7|           |
|                                                              |                    |
|    import eyes17.eyes                                        |                    |
|    p = eyes17.eyes.open()                                    |                    |
|    from pylab import \*                                      |                    |
|    p.set_sine(200)                                           |                    |
|    t,v, tt,vv = p.capture2(500, 20)   # captures A1 and A2   |                    |
|    xlabel('Time(mS)')                                        |                    |
|    ylabel('Voltage(V)')                                      |                    |
|    plot([0,10], [0,0], 'black')                              |                    |
|    ylim([-4,4])                                              |                    |
|    plot(t,v,linewidth = 2, color = 'blue')                   |                    |
|    plot(tt, vv, linewidth = 2, color = 'red')                |                    |
|    show()                                                    |                    |
+--------------------------------------------------------------+--------------------+
| Python program to study half wave                            | Output of the code |
| rectifier                                                    |                    |
+--------------------------------------------------------------+--------------------+


.. |image0| image:: schematics/halfwave.svg
.. |image1| image:: photos/halfwave.jpg
.. |image2| image:: screenshots/halfwave.png
.. |image3| image:: screenshots/halfwave.png
.. |image4| image:: screenshots/halfwave-filter.png
.. |image5| image:: screenshots/halfwave1N4007at1000Hz.png
.. |image6| image:: screenshots/halfwave-mpl.png
.. |image7| image:: screenshots/halfwave-mpl.png

