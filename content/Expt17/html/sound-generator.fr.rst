Générer un son
==============

:slug: Expt17/html/sound-generator
:date: 2018-09-07
:category: schoollevel
:tags: 1School Level Activities
:title: Générer un son
:lang: fr
       
Schéma
######


|image0|

Instructions
############


-  Faire les connexions selon la figure.
-  Modifier la fréquence de WG et écouter le son.
-  Le son est très puissant à certaines fréquences. Cela est dû
   à la résonance du disque piézo-électrique.

.. |image0| image:: schematics/sound-generator.svg
   :width: 500px
