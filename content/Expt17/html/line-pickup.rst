AC mains pickup
###############

:slug: Expt17/html/line-pickup
:date: 2018-09-07
:category: schoollevel
:tags: 1School Level Activities
:title: AC mains pickup
:lang: en
       
|image0|

Explore the signals from AC mains pickup
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

-  Connect a long wire to A3.
-  Enable A3 and its analysis option
-  Set timebase to 10 mS/division
-  Touch the free end of the wire
-  Take the free end near AC mains line

.. |image0| image:: schematics/line-pickup.svg
   :width: 500px
