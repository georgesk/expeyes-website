Étude de portes logiques
########################

:slug: Expt17/html/logic-gates
:date: 2018-09-07
:category: expts_17
:tags: 1Electronics
:title: Portes logiques
:lang: fr
       
L'alimentation 5 V pour le circuit 74LS08 peut être prise sur la prise
+5 V ou depuis OD1 (auqyel cas il faut activer OD1). Les entrées sont
connectées aux signaux carrés SQ1 et SQ2. On règle le générateur WG à
l'option SQ2. La sortie est connectée à A3 (à travers une résistance en
série comme le calibre de A3 est seulement 3,3 V). Les calibres de A1 et A2
sont réglés à 16 V et on les décale pour une meilleure visibilité, en évitant
que leurs courbes se cachent mutuellement.

+-------------------+---------------------------------------+
| |image0|          | |image1|                              |
+-------------------+---------------------------------------+
| Schéma de câblage | Photo du montage expérimental.        |
+-------------------+---------------------------------------+

Les courbes jaune et verte sont les entrées et la rouge est la sortie.
On peut voir que la sortie est HAUTE seulement quand les deux entrées
sont HAUTES en même temps.

+-------------------------------------------------------------------------+
| |image2|                                                                |
+-------------------------------------------------------------------------+
| Copie d'écran des signaux d'entrée et de sortie d'une porte ET (74LS74) |
+-------------------------------------------------------------------------+

Porte ET réalisée à l'aide de diodes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

On peut réaliser une porte ET à l'aide de deux diodes, comme montré
ci-dessous. Si une quelconque des entrées  passe à BAS, la diode se met
à conduire et la tension de sortie devient BASSE.

+-------------------+---------------------------------------+
| |image3|          | |image4|                              |
+-------------------+---------------------------------------+
| Schéma de câblage | Photo du montage expérimental.        |
+-------------------+---------------------------------------+



+-------------------------------------------------------------------+
| |image5|                                                          |
+-------------------------------------------------------------------+
| Copie d'écran de la porte ET réalisée avec deux diodes (1N4148)   |
+-------------------------------------------------------------------+

Porte OU
~~~~~~~~

Les signaux d'une porte 74LS32 sont montrés ci-dessous. Les circuits
sont compatibles au niveau du brochage des entrées et sorties, il suffit
d'échanger les puces.

|image6|

Porte OU à l'aide de diodes
~~~~~~~~~~~~~~~~~~~~~~~~~~~

La porte OU peut être réalisée comme montré ci-dessous. Si une quelconque
des entrées devient HAUTE, la sortie devient HAUTE.

+-------------------+---------------------------------------+
| |image7|          | |image8|                              |
+-------------------+---------------------------------------+
| Schéma de câblage | Photo du montage expérimental.        |
+-------------------+---------------------------------------+

+---------------------------------------------------------+
| |image9|                                                |
+---------------------------------------------------------+
| Copie d'écran de la port OU réalisée avec deux diodes.  |
+---------------------------------------------------------+

.. |image0| image:: schematics/logic-gates.svg
.. |image1| image:: photos/and-gate.jpg
   :width: 631px
.. |image2| image:: screenshots/andgate.png
.. |image3| image:: schematics/diode-and-gate.svg
.. |image4| image:: photos/diode-and-gate.jpg
   :width: 631px
.. |image5| image:: screenshots/diode-and-gate.png
   :alt: Unfortunately the screenshot is missing!
.. |image6| image:: screenshots/orgate.png
.. |image7| image:: schematics/diode-or-gate.svg
.. |image8| image:: photos/diode-or-gate.jpg
   :width: 631px
.. |image9| image:: screenshots/diode-or-gate.png

