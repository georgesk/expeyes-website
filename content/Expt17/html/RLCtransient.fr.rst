Réponse transitoire d'un circuit RLC
####################################

:slug: Expt17/html/RLCtransient
:date: 2018-09-07
:category: expts_17
:tags: 2Electrical Circuits and LCR elements
:title: réponse transitoire RLC
:lang: fr

Dans cette section, on explore la réponse transitoire d'un circuit RC
en lui appliquant un échelon de tension et en capturant les variations
de tension. Le bobinage utilisé a 300 tours de fil 44SWG. La valeur
de l'inductance est près de 120 mH et celle de la réistance est près de
550 Ω. La velur de la capacité est 0,1 µF.

+-------------------+---------------------------------------+
| |image0|          | |image1|                              |
+-------------------+---------------------------------------+
| Schéma de câblage | Photo du montage expérimental.        |
+-------------------+---------------------------------------+

Les signaux observés sont montrés ci-dessous. On voit que le circuit
est sous-amorti. La fréquence et le facteur d'amortissement sont
calculés en ajustant aux données un modèle de sinusoïde multiplié par
une fonction exponentielle. On peut augmenter l'amortissement en ajoutant
plus de résistance ou en augmentant le quotient \$\\frac{C}{L}\$.

+-----------------------------------------------------------------------+
| |image2|                                                              |
+-----------------------------------------------------------------------+
| Copie d'écran de la réponse transitoire d'un circuit RLC sous-amorti. |
+-----------------------------------------------------------------------+

.. |image0| image:: schematics/RLCtransient.svg
.. |image1| image:: photos/RLCtransient.png
.. |image2| image:: screenshots/rlctransient.png

