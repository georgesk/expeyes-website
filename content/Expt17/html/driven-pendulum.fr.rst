Pendule excité, résonance
#########################

:slug: Expt17/html/driven-pendulum
:date: 2018-09-07
:category: expts_17
:tags: 4Mechanics and Thermal etc.
:title: Pendule excité, résonance
:lang: fr
       
On réalise un pendule à l'aide de deux aimants-boutons et d'un morceau de
papier. On le suspend à une aiguille horizontale. Une bobine, alimentée par
SQ1, est mise en face de ça. Le pendule va commencer à osciller et
l'amplitude augmentera fortement à la résonance.

+----------+----------+
| |image0| | |image1| |
+----------+----------+

.. |image0| image:: schematics/driven-pendulum.svg
.. |image1| image:: photos/driven-pend.jpg
   :width: 500px
