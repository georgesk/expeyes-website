Transistor output characteristics in CE configuration (NPN and PNP)
###################################################################

:slug: Expt17/html/npnCEout
:date: 2018-09-07
:category: expts_17
:tags: 1Electronics
:title: Transistor Characteristics
:lang: en
       
The schematic is wired as shown in the diagram below. The base current
is set by the voltage from PV2, through a 100k resistor. The base
voltage is measured to calculate the base current from Ib =
(PV2-A2)/100K. The collector voltage is monitored by A1. The collector
is connected to PV1, through a 1k resistor. For a selected base current,
the voltage at PV1 is incremented in steps and at each step the
collector voltage is measured. Corresponding collector current is
calculated from i = (PV1-A1)/R. The transistor  used is 2N2222, having a
current gain of around 200.

+-----------------------------------+-----------------------------------+
| |image2|                          | |image3|                          |
+-----------------------------------+-----------------------------------+
| Wiring Diagram                    | Photograph of the experimental    |
|                                   | setup. Transistor is  2N2222      |
+-----------------------------------+-----------------------------------+

|                                                                           

+------------------------------------------+
| |image4|                                 |
+------------------------------------------+
| Screen shot of transistor characteristic |
+------------------------------------------+

| 
| Output characteristics of PNP transistor (2N3906) is shown below
| |image5|

.. |image0| image:: schematics/npn_ce_out.svg
.. |image1| image:: photos/npnCEout.png
.. |image2| image:: schematics/npn_ce_out.svg
.. |image3| image:: photos/npnCEout.png
.. |image4| image:: screenshots/npn-screenshot.png
.. |image5| image:: screenshots/pnp-ce-char.png

