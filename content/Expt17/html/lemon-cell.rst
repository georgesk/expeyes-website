Lemon Cell
##########

:slug: Expt17/html/lemon-cell
:date: 2018-09-07
:category: schoollevel
:tags: 1School Level Activities
:title: Lemon Cell
:lang: en
       
Schematic
~~~~~~~~~

|image0|

-  Voltaic cells are based on electrochemical reaction. The reaction
   typically occurs between two pieces of metal, called electrodes, and
   a liquid or paste, called an electrolyte.
-  Many fruits and liquids can be used for the acidic electrolyte.
-  Copper and Zinc plates are used as electrodes.

Instructions
~~~~~~~~~~~~

-  Make a lemon cell and connect as shown in the figure, measure the
   voltage at A1.
-  Connect the 1K resistor and measure the voltage again. The voltage
   drops because of the internal resistance of the cell.
-  Try repeating this with a 1.5 volts dry cell.

.. |image0| image:: schematics/lemon-cell.svg
   :width: 500px
