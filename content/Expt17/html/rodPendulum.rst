Acceleration due to gravity, by pendulum
########################################

:slug: Expt17/html/rodPendulum
:date: 2018-09-07
:category: expts_17
:tags: 4Mechanics and Thermal etc.
:title: Gravity by pendulum
:lang: en
       
The pendulum is made to oscillate between an LED and photo-transistor.
The time period is measured by monitoring the photo-transistor signal.
The resolution of measurement is around 100 micro seconds, so that the
small variation of the period with amplitude can be studied.

+----------+----------+
| |image0| | |image1| |
+----------+----------+

A Light Barrier with knife edges to support the Rod Pendulum is shown
below.

|image2|

.. |image0| image:: schematics/rod-pendulum.svg
.. |image1| image:: photos/rod-pend.jpg
.. |image2| image:: photos/rod-pend-LB.jpg

