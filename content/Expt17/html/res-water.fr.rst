Résistance de l'eau
###################

:slug: Expt17/html/res-water
:date: 2018-09-07
:category: schoollevel
:tags: 1School Level Activities
:title: Résistance de l'eau
:lang: fr

Schéma
~~~~~~

|image0|

Instructions
~~~~~~~~~~~~

-  Réaliser les connexions comme montré sur la figure.
-  Le courant à travers la résistance R1 est \$I = \\frac{A2}{R1}\$
-  La résistance électrique de l'eau est donnée par \$\\frac{A1-A2}{I}\$
-  Réaliser la même expérience en utilisant du courant continu de PV1
   au lieu du courant alternatif de WG.
-  **Est-ce qu'on obtient une lecture stable quand on utilise du courant continu ? Si non, pourquoi ?**

.. |image0| image:: schematics/res-water.svg
   :width: 500px
