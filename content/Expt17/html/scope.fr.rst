Oscilloscope à quatre canaux
############################
:slug: Expt17/html/scope
:date: 2018-09-07
:category: expts_17
:tags: 1Electronics
:title: Oscilloscope
:lang: fr
       

L'interface graphique principale d'ExpEYES ressemble à un oscilloscope
à quatre canaux, avec des éléments supplémentaires pour contrôler le
générateur BF, les sources de tension, etc. La copie d'écran ci-dessous
montre des données sur trois canaux. On peut réaliser un grand nombre
d'acitivités à l'aide de l'interface oscilloscope présentée ci-dessous.

| |image0|

Brève description du programme d'oscilloscope
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

-  Le programme fonctionne principalement comme un oscilloscope à
   quatre canaux, pour montrer les entrées A1, A2,A3 et MIC.

-  On ajuste la valeur maximale de l'axe des abscisses du graphique,
   à l'aide du curseur de base de temps, en général pour voir plusieurs
   cycles du signal.

-  Si le signal n'est pas stable, on peut choisir la source de
   synchronisation appropriée. Si besoin est, on peut ajuster le niveau de
   déclenchement.

-  Les courbes peuvent être enregistrées dans un fichier, au format
   texte. On peut prendre la transformée de Fourier et de voir le
   spectre de fréquence du signal d'entrée.

-  Ce programme d'oscilloscope possède aussi des widgets de contrôle
   et de suivi sur le panneau de droite, pour accéder à la plupart
   des fonctionnalités d'ExpEYES.

-  Les valeurs de tension en A1, A2, A3 et de résistance connectée à
   SEN sont mesurées et affichées chaque seconde. Mais ces valeurs n'ont
   pas de sens quand on connecte des signaux alternatifs.

-  Pour des entrées alternatives, activer le bouton à cocher à côté du
   widget du canal pour voir la tension crête et la fréquence.

-  Les bornes d'entrée-sortie d'ExpEYES sont brièvement décrites ci-dessous.

| |image1|

Bornes de sortie
~~~~~~~~~~~~~~~~

-  **CCS:** Source de courant contant de 1,1 mA. Activer/désativer la source
   à l'aide de la case à cocher.
-  **PV1:** Tension programmable, dans l'intervalle +/-5 V. On la règle à l'aide
   du curseur ou du widget de saisie texte.
-  **PV2:** Similaire à PV1, mais pour l'intervalle +/-3,3 V.
-  **SQ1:** Générateur de signal carré, entre 0 et 5 V. La fréquence est
   réglable de 1 Hz à 5 kHz.
-  **SQ2:** Identique à SQ1, mais disponible comme une option de WG.
-  **OD1:** Sortie numérique, la tension peut être mise à 0 ou à 5 V.
-  **WG:** Générateur basse fréquence (BF). Fréquence entre 1 Hz et 5 kHz. On
   peut régler l'amplitude à 3 V, 1 V ou 80 mV. Le signal peut être sinusoïdal,
   triangulaire ou carré. En mode signal carré, la sortie est en SQ2, elle
   varie de 0 à 5 V.
-  **-WG:** Sortie inversée de WG

| |image2|

Bornes d'entrée
~~~~~~~~~~~~~~~

-  **IN1:** Entrée pour la mesure de capcité. Un bouton est disponible pour
   démarrer la mesure.
-  **IN2:** Entrée pour mesurer la fréquence de signaux numériques, qui
   varient entre 0 et 3 à 5 V. Un bouton est disponible pour démarrer la
   mesure.
-  **SEN:** Entrée pour mesure des résistances. Cette borne est connectée
   en interne à 3,3 V via une résistance de 5,1 kΩ.
-  **A1:** Entrée pour mesurer la tension, fonctionne comme volmètre et
   comme oscilloscope. Le calibre maximal est +/-16 V, le calibre se
   choisit dans un menu déroulant. Le mode de couplage alternatif/continu
   peut être sélectionné par un interrupteur à glissière sur le boîtier.
-  **A2:** Identique à A1, sauf le couplage en alternatif.
-  **A3:** Entrée pour mesurer la tension, calibre +/-3,3 V. On peut amplifier
   les petits signaux en plaçant une résistance entre Rg et la masse (borne noire).
-  **MIC:** Entrée pour microphone à condensateur, la sortie apparaît comme
   quatrième canal de l'oscilloscope.
-  **Rg:** Branchement pour une résistance externe réglant le gain de A3. Le gain se
   calcule comme \$Gain = 1+\\frac{R_{ext}}{10000}\$. Par exemple, quand on connecte
   une résistance de 1 kΩ entre Rg et la masse, on obtient un gain de 11.

Les tensions en A1, A2, A3 et la résistance connectée entre SEN et la masse sont mesurées
et affichées chaque seconde. Cependant ces valeurs n'ont pas de sens quand on connecte des
signaux sont branchés. Quand on a des signaux alternatifs en entrée, on active la case à
cocher à côté du widget du canal.

.. |image0| image:: screenshots/scope.png
.. |image1| image:: images/scope-outputs.png
.. |image2| image:: images/scope-inputs.png

