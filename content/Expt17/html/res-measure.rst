Measuring Resistance
####################
 
:slug: Expt17/html/res-measure
:date: 2018-09-07
:category: schoollevel
:tags: 1School Level Activities
:title: Measuring Resistance
:lang: en

Schematic
~~~~~~~~~

|image0|

Instructions
~~~~~~~~~~~~

-  Connect the resistor from SEN to Ground( any black terminal)
-  The value will be displayed in the "Measurements & Control Section"
-  The valid range is from 100 Ohm to 100 kOhm

.. |image0| image:: schematics/res-measure.svg
   :width: 500px

