Induction électromagnétique
###########################

:slug: Expt17/html/induction
:date: 2018-09-07
:category: expts_17
:tags: 2Electrical Circuits and LCR elements
:title: Induction électromagnétique
:lang: fr

On fait tomber un aimant à travers une bobine et le signal de tension
qui en résulte est représenté en fonction du temps. Le montage expérimental
est présenté ci-dessous, ainsi que le signal capturé.

+----------+----------+
| |image0| | |image1| |
+----------+----------+


.. |image0| image:: schematics/induction.svg
.. |image1| image:: photos/em-induction.jpg
