Inverting Amplifier
###################

:slug: Expt17/html/opamp-inv
:date: 2018-09-07
:category: expts_17
:tags: 1Electronics
:title: Inverting Amplifier
:lang: en

The schematic is wired as shown in the diagram below. Ri = 1k and Rf =
10k. The WG amplitude is set to 80 mV, you may try a 1 volt input to
observe the clipping of the the output, since it exceeds the supply
voltage of +/- 6 volts.

+-----------------------------------+-----------------------------------+
| |image2|                          | |image3|                          |
+-----------------------------------+-----------------------------------+
| Wiring Diagram                    | Photograph of the experimental    |
|                                   | setup. Used OP07 (pin             |
|                                   | configuration of uA741)           |
+-----------------------------------+-----------------------------------+

|                                                                           

+-----------------------------------------------------------------------+
| |image5|                                                              |
+-----------------------------------------------------------------------+
| Screen shot of the oscilloscope program showing inputs and output of  |
| an Inverting Amplifier. Gain is -10                                   |
+-----------------------------------------------------------------------+


Writing Python Code
~~~~~~~~~~~~~~~~~~~

This experiment can also be done by running this `Python
Code <../code/capture2.py>`__. The output of the program is shown
below.

+----------------------------------------------------------+--------------------+
| ..  code-block:: python                                  | |image6|           |
|                                                          |                    |
|    import eyes17.eyes                                    |                    |
|    p = eyes17.eyes.open()                                |                    |
|    from pylab import \*                                  |                    |
|    p.set_sine(200)                                       |                    |
|    p.set_pv1(1.35) # will clip at 1.35 + diode drop      |                    |
|    t,v, tt,vv = p.capture2(500, 20) # captures A1 and A2 |                    |
|    xlabel('Time(mS)') ylabel('Voltage(V)')               |                    |
|    plot([0,10], [0,0], 'black')                          |                    |
|    ylim([-4,4])                                          |                    |
|    plot(t,v,linewidth = 2, color = 'blue')               |                    |
|    plot(tt, vv, linewidth = 2, color = 'red')            |                    |
|    show()                                                |                    |
+----------------------------------------------------------+--------------------+
| Python program to capture and display A1 and A2          | Output of the code |
+----------------------------------------------------------+--------------------+

.. |image0| image:: schematics/opamp-inv.svg
.. |image1| image:: photos/opamp-inv.jpg
.. |image2| image:: schematics/opamp-inv.svg
.. |image3| image:: photos/opamp-inv.jpg
	    :width: 631px
.. |image4| image:: screenshots/opamp-inv.png
.. |image5| image:: screenshots/opamp-inv.png
.. |image6| image:: screenshots/opamp-inv-mpl.png

