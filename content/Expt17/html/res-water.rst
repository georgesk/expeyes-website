Resistance of Water
###################

:slug: Expt17/html/res-water
:date: 2018-09-07
:category: schoollevel
:tags: 1School Level Activities
:title: Resistance of Water
:lang: en

Schematic
~~~~~~~~~

|image0|

Instructions
~~~~~~~~~~~~

-  Make connections as shown in the figure.
-  Current through the resistor R1, I = A2 / R1
-  Electrical resistance of water is given by (A1-A2)/I
-  Perform the same experiment using DC from PV1 instead of the AC from
   WG.
-  **Are you getting a stable reading using DC ? If not, Why ?**

.. |image0| image:: schematics/res-water.svg
   :width: 500px
