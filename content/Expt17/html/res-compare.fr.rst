Résistance par la loi d'Ohm
===========================

:slug: Expt17/html/res-compare
:date: 2018-09-07
:category: schoollevel
:tags: 1School Level Activities
:title: Résistance par la loi d'Ohm
:lang: fr
       
Schéma
~~~~~~


|image0|

Instructions
~~~~~~~~~~~~


-  Connecter les résistances comme montré sur la figure.
-  Le courant dans les circuits est \$ I = \\frac{PV1}{R_1 + R_2} \$
-  Selon la loi d'Ohm, la tension en A1 sera \$I \\times R_1\$

.. |image0| image:: schematics/res-compare.svg
   :width: 500px
