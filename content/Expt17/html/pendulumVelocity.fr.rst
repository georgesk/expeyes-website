Vitesse angulaire d'un pendule
##############################

:slug: Expt17/html/pendulumVelocity
:date: 2018-09-07
:category: expts_17
:tags: 4Mechanics and Thermal etc.
:title: Vitesse angulaire d'un pendule
:lang: fr
       
Schéma
~~~~~~

|image0|

Le mouvement d'un pendule est harmonique. Si on représente l'élongation
angulaire en fonction du temps, on obtient une sinusoïde. Mais les
encodeurs angulaires sont coûteux, on peut utiliser un moteur à courant
continu comme tachymètre (capteur de vitesse angulaire), la tension
induite est proportionnelle à la vitesse angulaire instantanée.

Instructions
~~~~~~~~~~~~

-  On attache le pendule à l'axe du moteur.
-  La tension induite est proportionnelle à la vitesse angulaire.
-  Le signal est mesuré par A3. La résistance de 100 Ω placée entre Rg
   et masse (borne noire) fournit un facteur d'amplification de
   \$1 + \\frac{10000}{100} = 101\$, c'est nécessaire car la tension
   induite est faible.
-  Mettre le pendule à osciller et cliquez le bouton **START** pour
   enregistrer les données.
-  Si le signal est bruité, essayer de tourner le corps du moteur de
   quelques degrés et recommencer.

.. |image0| image:: schematics/pendulum-velocity.svg
   :width: 500px
