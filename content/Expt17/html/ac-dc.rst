Direct and Alternating Currents
###############################

:slug: Expt17/html/ac-dc
:date: 2018-09-07
:category: schoollevel
:tags: 1School Level Activities
:title: Direct and Alternating Currents
:lang: en
       
Schematic
~~~~~~~~~


|image0|

-  To find out the nature of voltage at a terminal, we need to plot it
   against time.
-  If the voltage is not changing with time, the graph will be a
   horizontal line, and the voltage can be termed as DC.
-  Any change in voltage with time indicates an AC component.
-  If the average value is zero, the voltage is pure AC.

Instructions
~~~~~~~~~~~~


-  Connect the wires as shown in the figure.
-  Set PV1 to 2 volts.
-  Graph of PV1 will be a horizontal line.
-  WG generates a pure AC signal and you can measure the time for one
   cycle from the graph to calculate the frequency.

.. |image0| image:: schematics/ac-dc.svg
   :width: 500px
