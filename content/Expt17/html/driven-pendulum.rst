Driven pendulum, Resonance
##########################

:slug: Expt17/html/driven-pendulum
:date: 2018-09-07
:category: expts_17
:tags: 4Mechanics and Thermal etc.
:title: Driven pendulum
:lang: en
       
A pendulum is made using two button magnets and a piece of paper. It is
suspended on horizontal needle. A coil, powered by SQ1, is kept in front
of that. The pendulum will start oscillating and the amplitude will go
very high at resonance.

+----------+----------+
| |image0| | |image1| |
+----------+----------+

.. |image0| image:: schematics/driven-pendulum.svg
.. |image1| image:: photos/driven-pend.jpg
   :width: 500px

