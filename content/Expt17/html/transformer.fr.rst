Transformateur
##############

:slug: Expt17/html/transformer
:date: 2018-09-07
:category: schoollevel
:tags: 1School Level Activities
:title: Transformateur
:lang: fr
       

Schéma
~~~~~~


|image0|

Instructions
~~~~~~~~~~~~

-  Les deux bobines fournies avec ExpEYES sont utilisées pour faire le
   primaire et le secondaire.
-  le signal de la sortie WG est appliqué au primaire et suivi par A1.
-  la tension induite sur le secondaire est suivie par A2.
-  Réaliser les connexions et placer les bobines proches et parallèles
   l'une à l'autre.
-  Insérer un matériau ferromagnétique au centre des bobines pour
   augmenter la tension induite dans la bobine secondaire.

.. |image0| image:: schematics/transformer.svg
   :width: 500px
