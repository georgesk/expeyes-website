Résistance du corps humain
##########################

:slug: Expt17/html/res-body
:date: 2018-09-07
:category: schoollevel
:tags: 1School Level Activities
:title: Resistance of Human body
:lang: fr
       
Schéma
~~~~~~

|image0|

-  On peut mesurer la résistance du corps humain en toute sécurité
   à l'aide d'un signal de basse tension d'**ExpEYES**.

Instructions
~~~~~~~~~~~~

-  Conecter les files et les résistances comme montré sur le schéma.
   Régler PV1 à 3 V.
-  Tenir les bouts des fils marqués « mains », en les serrant entre les
   doigts des deux mains.
-  Le courant qui traverse la résistance de 100 kΩ vaut
   \$I=\\frac{A2}{1 \\times 10^5}\$.
-  La résistance électrique du corps est donnée par \$\\frac{A1-A2}{I}\$.
-  Faire la même expérience en utilisant du courant alternatif de WG au
   lieu du courant continu de PV1.
-  La résistance est principalement due à la peau. Avec le courant
   alternatif, la peau fonctionne comme le diélectrique d'un condensateur.
   Ainsi, il passe plus de courant, pour une tension donnée, en alternatif
   qu'en continu.

.. |image0| image:: schematics/res-body.svg
   :width: 500px
