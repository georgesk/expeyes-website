Conduction du corps humain
##########################

:slug: Expt17/html/conducting-human
:date: 2018-09-07
:category: schoollevel
:tags: 1School Level Activities
:title: Conduction du corps humain
:lang: fr
       
Schéma
~~~~~~

|image0|

Nous sommes faits de matériaux électriquement conducteurs (comme une
solution de sel dans l'eau). La peau de notre corps se comporte comme
une résistance électrique. Avec un signal de basse tension on peut
le démontrer en toute sécurité.

Instructions
~~~~~~~~~~~~

-  Connecter une extrémité d'un fil au générateur BF (WG), laisser
   l'autre extrémité en l'air.
-  Connecter une extrémité d'un autre fil à A1, l'entrée de
   l'oscilloscope.
-  Régler WG à 1000 Hz. Connecter les deux extrémités laissées en
   l'air l'une à l'autre pour voir le signal.
-  Ensuite, tenir une des extrémités de fil dans une main et
   l'autre dans l'autre main.

Si on serre bien les bouts des fils, le signal apparaît comme si on
les connectait l'un à l'autre. Cela signifie-t-il que notre corps
est aussi bon conducteur qu'un fil ? Non, pensez à la valeur du courant
qui y circule. L'impédance d'entrée de A1 est 1 MΩ, ce qui signifie
que le courant est seulement de 3 µA pour un signal de 3 V. Une
version modifiée de ce montage permettra de mesurer la résistance
de notre corps.

Là aussi, tout dépend si on utilise du courant alternatif ou continu.
Le corps conduit mieux le courant alternatif, car la peu fonctionne
comme une résistance pure avec le courant continu, et qu'elle agit comme
le diélectrique d'un condensateur avec le courant alternatif. Le
courant alternatif est plus dangereux pour nous.

.. |image0| image:: schematics/conducting-human.svg
   :width: 500px

