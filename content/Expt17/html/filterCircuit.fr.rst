Circuits de filtrage
####################

:slug: Expt17/html/filterCircuit
:date: 2018-09-07
:category: expts_17
:tags: 2Electrical Circuits and LCR elements
:title: Circuits de filtrage
:lang: fr

Schéma
~~~~~~

|image0|

Instructions
~~~~~~~~~~~~


-  Réaliser les connexions comme montré. L'image représente un filtre LC
   avec une résistance de charge.
-  On peut utiliser tout type de circuit de filtrage.
-  On applique un signal sinusoïdal d'une fréquence donnée à l'entrée du
   filtre et on mesure celui-ci à l'aide de A1.
-  Le signal de sortie du filtre est mesuré à l'aide de A2 et le quotient
   des amplitudes (le gain) est calculé.
-  Ce processus est répété pour plusieurs fréquences et on représente la
   courbe du gain en fonction de la fréquence.
-  Pour comemncer on essaie C = 1µF, L = 1mH, R = 1000 Ω.
   Calculer \$f_0 = \\frac{1}{2 \\pi \\sqrt{L \\times C}}\$.
-  Essayer des combinaisons LC en parallèle et en série.

.. |image0| image:: images/filterCircuit.png

