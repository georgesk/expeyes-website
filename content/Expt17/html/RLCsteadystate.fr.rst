Réponse en régime continu de circuits RLC
#########################################

:slug: Expt17/html/RLCsteadystate
:date: 2018-09-07
:category: expts_17
:tags: 2Electrical Circuits and LCR elements
:title: Réponse en régime continu RLC
:lang: fr

Dans cette section, on mesure l'amplitude et la pahse de tensions
et de courants aux borne d'une résistance, d'un condensateur et d'un bobinage
quand on applique une tension alternative. On va explorer trois cas différents ;
Les circuits RC série, RL série, et RLC série. On s'intéresse à la résonance
d'un circuit RLC.

Tension alternative appliquée à un circuit RC

+-------------------+---------------------------------------+
| |image0|          | |image1|                              |
+-------------------+---------------------------------------+
| Schéma de câblage | Photo du montage.                     |
+-------------------+---------------------------------------+

                                                                          
Les signaux observés sont montrés ci-dessous. La trace verte est la
tension aux bornes de la résistance. Comme le courant et la tension
aux bornes d'une résistance sont en phase, ça représente la phase du courant.
On peut
voir qu'elle est en avance de phase par rapport à la courbe rouge (tension
aux bornes du condensateur) de 90 degrés, comme attendu. Le déphasage de la
tension totale  par rapport au courant est donnée par
\$ arctan(\\frac{Z_C}{R})\$. La valeur mesurée est 45 degrés, et la valeur
attendue avec R=1 kΩ, C=1 µf et f=150 Hz est 46,7 degrés. Pour que cela coïncide
exactement, il faudrait prendre les valeurs réelles de la résistance et
de la capacité, pas les valeurs nominales qui sont marquées dessus.

+------------------------------------------------------+
| |image2|                                             |
+------------------------------------------------------+
| Copie d'écran de la tension dans le circuit RC série |
+------------------------------------------------------+

Tension alternative appliquée à un circuit RL

+-------------------+---------------------------------------+
| |image3|          | |image4|                              |
+-------------------+---------------------------------------+
| Schéma de câblage | Photo du montage expérimental.        |
+-------------------+---------------------------------------+

                                                                          
Les signaux observés sont ci-dessous. Le déphasage de la tension totale
est donné par \$ arctan(\\frac{Z_L}{R})\$. La valeur mesurée est 10,7
degré ; avec R = 1 kΩ, L = 10mH et f = 3000 Hz, on est en agrément avec
la valeur calculée.

+-----------------------------------------------------+
| |image5|                                            |
+-----------------------------------------------------+
| Copie d'écran des tensions dans le circuit RL série |
+-----------------------------------------------------+

Tension alternative appliquée à un circuit RLC série ; on explore
la condition de résonance. La fréquence de résonance est calculée et
la fréquence du générateur est ajustée à cette valeur.
Puis on ajuste finement la fréquence afin que le déphase de la tension
totale par rapport au courant s'annule. La tension aux bornes de LC
ne descend pas jusqu'à zéro àa cause de la résistance de 20 Ω du
bobinage.

+-------------------+---------------------------------------+
| |image6|          | |image7|                              |
+-------------------+---------------------------------------+
| Schéma de câblage | Photo du montage expérimental.        |
+-------------------+---------------------------------------+

                                                                          
La courbe rouge est la tension totale à la résonance. Les tensions
individuelles aux bornes de L et de C sont aussi présentées. On peut
voir que la tension totale s'approche de zéro, parce que la tension
aux bornes de chage composant est égale mais en opposition de phase,
si bien que leur somme peut s'annuler.

+-----------------------------------------------------------------------+
| |image8|                                                              |
+-----------------------------------------------------------------------+
| Copie d'écran des tensions dans un circuit RLC série, à la résonance. |
+-----------------------------------------------------------------------+

.. |image0| image:: schematics/RCsteadystate.svg
.. |image1| image:: photos/RCsteadystate.png
.. |image2| image:: screenshots/rc-steadystate.png
.. |image3| image:: schematics/RLsteadystate.svg
.. |image4| image:: photos/RLsteadystate.png
.. |image5| image:: screenshots/rl-steadystate.png
.. |image6| image:: schematics/RLCsteadystate.svg
.. |image7| image:: photos/RLCsteadystate.png
.. |image8| image:: screenshots/rlc-steadystate.png

