Circuit diviseur d'horloge, à l'aide d'une bascule D
####################################################

:slug: Expt17/html/clock-divider
:date: 2018-09-07
:category: expts_17
:tags: 1Electronics
:title: Circuit diviseur d'horloge
:lang: fr
       
On envoie un signal carré à l'entrée horloge d'une bascule 74LS74. La
sortie Q-barre est connectée à l'entrée D. Les entrées Clear et Preset
doivent être laissées HAUTES. Tous les fronts montants du signal d'entrée
basculent le signal de sortie, mais rien ne se passe durant les fronts
descendants.

+-------------------+---------------------------------------+
| |image0|          | |image1|                              |
+-------------------+---------------------------------------+
| Schéma de câblage | Photo du montage expérimental.        |
+-------------------+---------------------------------------+

|image2|

Le rapport cyclique de la sortie sera 50 % quel que soit le rapport
cyclique du signal d'entrée, comme on voir ci-dessous.

|image3|

.. |image0| image:: schematics/clock-divider.svg
.. |image1| image:: photos/clock-divider.jpg
   :width: 631px
.. |image2| image:: screenshots/clock-divider.png
.. |image3| image:: screenshots/clock-divider-dcycle.png

