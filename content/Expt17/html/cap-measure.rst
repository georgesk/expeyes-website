Measuring Capacitance
#####################

:slug: Expt17/html/cap-measure
:date: 2018-09-07
:category: schoollevel
:tags: 1School Level Activities
:title: Measuring Capacitance
:lang: en
       
Schematic
~~~~~~~~~


|image0|

Instructions
~~~~~~~~~~~~


-  Connect the capacitors one by one from IN1 to Ground( any black
   terminal)

-  Measure the value by clicking on "Capacitance on IN1"

-  The terminal IN1 measures capacitance by charging it with a constant
   current for a fixed duration. The charge is the product of the
   current and the time interval.

-  The voltage across the capacitor is measured and C is calculated from
   I x t /V.

-  **You should not touch the capacitor while measuring because it
   causes leakage current.**

.. |image0| image:: schematics/cap-measure.svg
   :width: 500px

