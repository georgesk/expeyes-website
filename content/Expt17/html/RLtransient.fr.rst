Réponse transitoire d'un circuit RL
###################################

:slug: Expt17/html/RLtransient
:date: 2018-09-07
:category: expts_17
:tags: 2Electrical Circuits and LCR elements
:title: réponse transitoire RL
:lang: fr
       

Un échelon de tension est appliqué à un circuit RL et la tension
qui en résulte aux bornes du bobinage est capturée et analysée.

+-------------------+---------------------------------------+
| |image0|          | |image1|                              |
+-------------------+---------------------------------------+
| Schéma de câblage | Photo du montage expérimental.        |
+-------------------+---------------------------------------+


Les échelons de tension sont créés sur OD1 en cliquant sur les boutons
de l'interface graphique. Les signaux observés sont présentés ci-dessous,
montrant la variation de tension aux bornes du bobinage dans chacun des cas.
On peut observer que quand on applique un créneau de 5 à 0 V, la polarité de
la tension aux bornes de l'inductance est renversée et que la tension
devient immédiatement négative. Après cela, la tension subit une décroissance
exponentielle, ce qui cause un courant dans la résistance. On ajuste la
courbe de tension obtenue avec le modèle
\$ U = U_0 exp(\\frac{R}{L} \\times t) \$ afin d'extraire la valeur de
\$\\frac{R}{L}\$. La valeur de l'inductance est calculée à partir de ça.


+----------------------------------------------------+
| |image2|                                           |
+----------------------------------------------------+
| Copie d'écran de la tension aux bornes du bobinage |
+----------------------------------------------------+

.. |image0| image:: schematics/RLtransient.svg
.. |image1| image:: photos/RLtransient.png
.. |image2| image:: screenshots/rltransient.png

