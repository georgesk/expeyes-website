Redressement simple alternance
##############################

:slug: Expt17/html/halfwave
:date: 2018-09-07
:category: expts_17
:tags: 1Electronics
:title: Redressement simple alternance
:lang: fr
       
Le générateur BF (*Waveform generator*, *WG*) est réglé pour fournir
un signal sinusoïdal de fréquence 1 kHz. Il est suivi par le canal
A1 de l'oscilloscope. Le signal après la diode est suivi par A2. Le
signal observé sera un peu bruité sans la résistance de charge ; quand
on branche une résistance de 1 kΩ on obtient un signal redressé plus clair.
La chute de tension due à la diode est bien visible. Connecter des
condensateurs de différentes capacités pour voir l'effet de filtrage.

+-------------------+---------------------------------------+
| |image0|          | |image1|                              |
+-------------------+---------------------------------------+
| Schéma de câblage | Photo du montage expérimental.        |
+-------------------+---------------------------------------+

|                                         

+-----------------------------------------------------------------------+
| |image3|                                                              |
+-----------------------------------------------------------------------+
| Copie d'écran de l'oscilloscope qui montre les siganux d'entrée et de |
| sortie du redresseur simple alternance. La diode est de type 1N4148   |
| et la résistance de charge vaut 1 kΩ.                                 |
+-----------------------------------------------------------------------+

Le signal de sortie après ajout d'un condensateur de filtrage de 1 µF
est présenté ci-dessous. Quand le produit RC augmente, la variation
de tension diminue.

| |image4|

Effet de la capacité de la jonction de la diode :
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Toute jonction de diode a une capacité équivalente, qu'on peut considérer
comme connectée en parallèle à un jonction PN idéale. La capacité
de jonction de la diode 1N4148 est de 4 pF seulement, mais le type
1N4007, cette valeur monte à 20 pF. La sortie du redresseur, sans
résistance de charge, pour une diode 1N4007, est présentée ci-dessous.
L'impédance d'entrée de 1 MΩ du canal A2 sera neanmoins toujours
présente.

| 
| |image5|
|

Écriture de code Python
~~~~~~~~~~~~~~~~~~~~~~~

On peut aussi faire cette expérience à l'aide de ce `code Python
<../code/capture2.py>`__. Les courbes affichées par le programme
sont présentées ci-dessous.

+--------------------------------------------------------------+--------------------+
| .. code-block:: python                                       | |image7|           |
|                                                              |                    |
|    import eyes17.eyes                                        |                    |
|    p = eyes17.eyes.open()                                    |                    |
|    from pylab import \*                                      |                    |
|    p.set_sine(200)                                           |                    |
|    t,v, tt,vv = p.capture2(500, 20)   # capture A1 et A2     |                    |
|    xlabel('Time(mS)')                                        |                    |
|    ylabel('Voltage(V)')                                      |                    |
|    plot([0,10], [0,0], 'black')                              |                    |
|    ylim([-4,4])                                              |                    |
|    plot(t,v,linewidth = 2, color = 'blue')                   |                    |
|    plot(tt, vv, linewidth = 2, color = 'red')                |                    |
|    show()                                                    |                    |
+--------------------------------------------------------------+--------------------+
| programme Python pour l'étude du redresseur                  | Résultat affiché   |
| simple alternance                                            | par ce code        |
+--------------------------------------------------------------+--------------------+


.. |image0| image:: schematics/halfwave.svg
.. |image1| image:: photos/halfwave.jpg
.. |image2| image:: screenshots/halfwave.png
.. |image3| image:: screenshots/halfwave.png
.. |image4| image:: screenshots/halfwave-filter.png
.. |image5| image:: screenshots/halfwave1N4007at1000Hz.png
.. |image6| image:: screenshots/halfwave-mpl.png
.. |image7| image:: screenshots/halfwave-mpl.png
    :width: 500px

