Courbes courant-tension
#######################

:slug: Expt17/html/plotIV
:date: 2018-09-07
:category: expts_17
:tags: 2Electrical Circuits and LCR elements
:title: Courbes courant-tension
:lang: fr

Courbes courant-tension, pour des résistances
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

On connecte deux résistances comme montré sur le diagramme. R2
(généralement 1 kΩ) est utilisée pour mesurer le courant indirectement,
c'est à dire à partir de la tension à ses bornes. On peut régler la
tension de PV1 à l'aide d'un curseur. La tension présentée à l'affichage
est la tension aux bornes de R1. On présente aussi la valeur du courant.
La tension peut être variée dans des intervalles choisis et
on peut tracer des courbes courant-tension.

|image0|

On voit à l'écran les graphiques pour des résistances de 2,2 kΩ et 10 kΩ
et aussi pour une jonction PN. La résistance est un élément
linéaire, mais la jonction PN est non-linéaire, comme les courbes le
montrent.

|image1|

.. |image0| image:: schematics/res-compare.svg
   :width: 631px
	   
.. |image1| image:: screenshots/plot-iv.png
