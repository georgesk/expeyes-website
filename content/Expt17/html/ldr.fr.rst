Photorésistance (**LDR**)
#########################

:slug: Expt17/html/ldr
:date: 2018-09-07
:category: schoollevel
:tags: 1School Level Activities
:title: Photorésistance (LDR)
:lang: fr
       
Schéma
~~~~~~

|image0|

Instructions
~~~~~~~~~~~~

-  La résistance d'une LDR diminue quand elle reçoit de la lumière.
-  ExpEYES peut mesurer la résistance d'un dipôle connecté entre
   SEN et la masse (borne noire)
-  On connecte SEN à A1, ce qui permet d'observer les changements de résistance.
-  Réaliser les connexions comme montré sur la figure.
-  Comparer la tension en A1 en utilisant la lumière du soleil et celle d'un
   tube fluorescent.
-  **Y a-t-il une composante alternative dans le cas d'un tube fluorescent ?**

.. |image0| image:: schematics/ldr.svg
		    :width: 500px
