Transient response of RL circuit
################################

:slug: Expt17/html/RLtransient
:date: 2018-09-07
:category: expts_17
:tags: 2Electrical Circuits and LCR elements
:title: RL transient response
:lang: en
       
A voltage step is applied to a series RL circuit and the resulting
voltage variation across the coil inductor is captured and analyzed.


+------------------+---------------------------------------+
| |image0|         | |image1|                              |
+------------------+---------------------------------------+
| Wiring Diagram   | Photograph of the experimental setup. |
+------------------+---------------------------------------+


The voltage steps are generated on OD1 by clicking on the buttons on the
GUI. The observed waveforms are shown below, showing the voltage
variation across the inductor in each case. It can be observed that when
a 5 to 0 volt step is applied, the polarity of the voltage across the
inductor is reversed and the voltage goes to negative immediately. After
that the voltage reduces exponentially, driving a current through the
resistor. The resulting wave form is fitted with
\$V = V_0 exp(\\frac{R}{L} \\times t) \$
to extract the \$\\frac{R}{L}\$ value. The value of inductor is calculated
from that.


+------------------------------------------------+
| |image2|                                       |
+------------------------------------------------+
| Screen shot of the voltage across an inductor. |
+------------------------------------------------+

.. |image0| image:: schematics/RLtransient.svg
.. |image1| image:: photos/RLtransient.png
.. |image2| image:: screenshots/rltransient.png

