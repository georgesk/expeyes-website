Mesure de capacité
##################

:slug: Expt17/html/cap-measure
:date: 2018-09-07
:category: schoollevel
:tags: 1School Level Activities
:title: Mesure de capacité
:lang: fr
       
Schéma
~~~~~~


|image0|

Instructions
~~~~~~~~~~~~


-  Connecter les condensateurs un par un entre IN1 et la masse (toute
   prise noire)

-  Mesurer sa valeur en cliquant sur « Capacité en IN1 »

-  La borne IN1 mesure la capacité en la chargeant avec un courant
   constant pendant une durée fixe. La charge est le produit du
   courant par l'intervalle de temps.

-  La tension aux bornes du condensateur se calcule comme
   \$ C = \\frac{I \\times t}{U} \$.

-  **Il ne faut pas toucher le condensateur durant la mesure car
   cela cause une fuite de courant**


.. |image0| image:: schematics/cap-measure.svg
   :width: 500px

