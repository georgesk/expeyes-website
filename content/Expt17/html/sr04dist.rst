Distance measurement using HY-SR04 Piezo sensor
###############################################

:slug: Expt17/html/sr04dist
:date: 2018-09-07
:category: expts_17
:tags: 4Mechanics and Thermal etc.
:title: Distance by Echo (SR04)
:lang: en
       
+-----------------------+-----------------------+-----------------------+
| HY-SR04 is a widely   | |image2|              | |image3|              |
| available ultrasound  |                       |                       |
| ranging module. It    |                       |                       |
| consists of two 40    |                       |                       |
| kHz Piezo crystals,   |                       |                       |
| one acts as a         |                       |                       |
| transmitter and other |                       |                       |
| as a receiver, and    |                       |                       |
| some electronics      |                       |                       |
| circuitry to measure  |                       |                       |
| the time interval     |                       |                       |
| between a burst of    |                       |                       |
| sound transmitted and |                       |                       |
| the echo received.    |                       |                       |
| The four pins are the |                       |                       |
| power supply, ground, |                       |                       |
| trigger and Echo.     |                       |                       |
| This module can be    |                       |                       |
| connected to          |                       |                       |
| ExpEYES-17 as shown   |                       |                       |
| in the schematic, to  |                       |                       |
| measure distance      |                       |                       |
| between the module    |                       |                       |
| and the reflecting    |                       |                       |
| surface, with an      |                       |                       |
| error less than 3mm.  |                       |                       |
| This can be used to   |                       |                       |
| design several        |                       |                       |
| experiments in        |                       |                       |
| mechanics.            |                       |                       |
+-----------------------+-----------------------+-----------------------+

The Mass and Spring problem
~~~~~~~~~~~~~~~~~~~~~~~~~~~

When a mass suspended on a spring is made to oscillate, the period of
oscillation is given by \$T = 2 \\times \\pi \\times \\sqrt{\\frac{m}{k}}\$,
where \$m\$ is the mass
and \$k\$ is the spring constant. We can measure the period of oscillation
by measuring the distance to the moving mass as a function of time. The
distance versus time graph is fitted with a sine function to calculate
the frequency. The photograph below shows a metal plate suspended on a
spring and the SR04 facing it from the bottom. The distance to the
oscillating plate is measured for 5 seconds and the data is fitted to a
sine function.

|image4| A video of the experiment is `HERE <https://www.youtube.com/watch?v=FhCYhDiIRuQ>`__.

Simple Pendulum
~~~~~~~~~~~~~~~

     The period of oscillations of a pendulum also can be measured
using SR04 connected to ExpEYES. The figure shows a pendulum with a
rectangular bob, but one can use a spherical bob with a piece of paper
(to provide a flat reflecting surface) pasted to it. The measured
frequency is 1.05 Hz for a pendulum having a length of 22cm. One can
calculate the value of acceleration due to gravity from this data.
  
|image5| A video is `HERE <https://www.youtube.com/watch?v=fOTfMsKiXBo>`__.

Free Fall, Displacement as a function of Time
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

It is possible to measure the distance to a body falling under
gravity. The value of \$g\$ can be calculated by fitting the data with
the polynomial \$S(t)= a \\times t^2 + b \\times t + c\$

$\g\$ is given by \$2 \\times a\$


.. |image0| image:: photos/sr04.jpg
.. |image1| image:: photos/sr04-dist.png
.. |image2| image:: photos/sr04.jpg
.. |image3| image:: photos/sr04-dist.png
.. |image4| image:: photos/massAndSpring.jpg
   :width: 100%
.. |image5| image:: photos/pendulum.jpg
   :width: 100%

