Transformer
###########

:slug: Expt17/html/transformer
:date: 2018-09-07
:category: schoollevel
:tags: 1School Level Activities
:title: Transformer
:lang: en
       
Schematic
~~~~~~~~~


|image0|

Instructions
~~~~~~~~~~~~

-  The two coils provided with ExpEYES are used as the primay
   and secondary.
-  WG output is applied to the primary and voltage is monitored on A1.
-  Induced voltage on secondary is monitored on A2.
-  Make connections and keep the coils close and parallel.
-  Insert some ferromagnetic material through the coils to increase the
   voltage induced on the secondary coil.

.. |image0| image:: schematics/transformer.svg
   :width: 500px
