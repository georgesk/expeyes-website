Vitesse du son
##############

:slug: Expt17/html/soundVelocity
:date: 2018-09-07
:category: expts_17
:tags: 3Sound (velocity and beats)
:title: Vitesse du son
:lang: fr
      
La fréquence du générateur BF (*waveform generator*, *WG*) est réglée à
la fréquence de résonance du disque piézo-électrique, pour une émission
sonore maximale. Les compressions et expansions du disque piézo-électrique
génèrent une onde progressive faite de zones de haute et basses pressions
devant lui. Le microphone permet de numériser la pression, et on en fait
une courbe en fonction du temps. L'oscilloscope est synchronisé par le
signal sinusoïdal qui commande le disque piézo. Quand on déplace le
microphone, le déphasage entre les deux signaux change. Pour un
déplacement d'une demi-longueur d'onde, on passe de la concordance de
phase (0°) à l'opposition de phase (180°). Un plan de câblage est présenté
ci-dessous.

|image0|

On ajuste la distance pour que les signaux du générateur et du microphone
soient en phase, comme ci-dessous.

+-----------------------------------+-----------------------------------+
| |image3|                          | |image4|                          |
+-----------------------------------+-----------------------------------+
| Distance ajustée pour un          | Signal sinusoïdal commandant le   |
| déphasage nul                     | piézo et signal du microphone     |
+-----------------------------------+-----------------------------------+


On change alors la distance pour que les signaux du générateur et du
microphone soient déphasés, comme on voit ci-dessous. Le déplacement
\$d\$ est mesuré, la vitesse du son est donnée par
\$v = 2 \\times d \\times 3500\$

+-----------------------------------+-----------------------------------+
| |image7|                          | |image8|                          |
+-----------------------------------+-----------------------------------+
| Distance ajustée pour une         | Signal sinusoïdal commandant le   |
| opposition de phase               | piézo et signal du microphone     |
+-----------------------------------+-----------------------------------+


.. |image0| image:: schematics/sound-velocity.svg
   :width: 500px
.. |image3| image:: photos/sound-vel-1.jpg
.. |image4| image:: screenshots/sound-velocity-zero.png
.. |image7| image:: photos/sound-vel-2.jpg
   :width: 400px
.. |image8| image:: screenshots/sound-velocity180.png

