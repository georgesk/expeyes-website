Sound Beats
###########

:slug: Expt17/html/soundBeats
:date: 2018-09-07
:category: expts_17
:tags: 3Sound (velocity and beats)
:title: Sound beats
:lang: en
       
Two Piezo buzzers are driven by WG and SQ1 respectively. The frequencies
are set close to each other.


|image0|

|image1|

.. |image0| image:: schematics/sound-beats.svg
   :width: 500px
.. |image1| image:: photos/sound-beats.jpg

