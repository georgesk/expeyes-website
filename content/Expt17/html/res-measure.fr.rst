Mesure de résistances
#####################

:slug: Expt17/html/res-measure
:date: 2018-09-07
:category: schoollevel
:tags: 1School Level Activities
:title: Mesure de résistances
:lang: fr

Schéma
~~~~~~

|image0|

Instructions
~~~~~~~~~~~~

-  Connecter la résistance entre SEN et la masse (bornes noires)
-  La valeur de résistance sera affichée dans la section de mesure & contrôle
-  L'intervalle de validité est entre 100 Ω et 100 kΩ

.. |image0| image:: schematics/res-measure.svg
   :width: 500px

