Steady state response of RLC circuits
#####################################

:slug: Expt17/html/RLCsteadystate
:date: 2018-09-07
:category: expts_17
:tags: 2Electrical Circuits and LCR elements
:title: RLC steady state response
:lang: en

In this section, we measure the amplitude and phases of voltages and
currents across Resistors, Capacitors and Inductors when an AC voltage
is applied. Three separate cases will be explored; Series RC circuit, RL
circuit and RLC circuit. The resonance of LC circuit is explored.
AC voltage applied to a series RC circuit.

+------------------+---------------------------------------+
| |image0|         | |image1|                              |
+------------------+---------------------------------------+
| Wiring Diagram   | Photograph of the experimental setup. |
+------------------+---------------------------------------+

                                                                          
The observed waveforms are shown below. The green trace is the voltage
across the resistor. Since the current and voltage are in phase across a
resistor, this trace represents the phase of current. It can be seen
that it is ahead of the red trace (voltage across C) by 90 degree, as
expected. The phase difference of total voltage vs. the current
is given by
\$ arctan(\\frac{Z_C}{R})\$.
The measured value is 45 degree and the expected
value for R=1kΩ, C = 1µF and f = 150 Hz is 46.7 degree. To get them
matching, we need to use the actual values of the resistance and
capacitance in the calculations, not the nominal values printed on them.

+--------------------------------------------------+
| |image2|                                         |
+--------------------------------------------------+
| Screen shot of the voltages in series RC circuit |
+--------------------------------------------------+

AC voltage applied to a series RL circuit

+------------------+---------------------------------------+
| |image3|         | |image4|                              |
+------------------+---------------------------------------+
| Wiring Diagram   | Photograph of the experimental setup. |
+------------------+---------------------------------------+

                                                                          
The observed waveforms are shown below.  The phase difference of total voltage
vs. the current is given by \$ arctan(\\frac{Z_L}{R})\$. The measured
value is 10.7 degree R=1 kΩ, L = 10mH and f = 3000 Hz, in agreement with
calculated value.

+--------------------------------------------------+
| |image5|                                         |
+--------------------------------------------------+
| Screen shot of the voltages in series RL circuit |
+--------------------------------------------------+

AC voltage applied to a series RLC circuit, explore Resonance Condition
In this case both the Inductor and capacitor are present. The resonant
frequency is calculated and the input frequency is set at that value.
The frequency is then adjusted to make the phase difference of voltage
across LC zero. The voltage across LC does not go to zero due to the 20 Ω
resistance of the inductor coil.

+------------------+---------------------------------------+
| |image6|         | |image7|                              |
+------------------+---------------------------------------+
| Wiring Diagram   | Photograph of the experimental setup. |
+------------------+---------------------------------------+

                                                                          
The RED trace is the voltage across LC at resonance. The individual
voltage across L and C are also shown. It can be seen that the total
volatge is going to zero because the voltage across each element is
equal and out of phase, so they add to zero.

+------------------------------------------------------------------+
| |image8|                                                         |
+------------------------------------------------------------------+
| Screen shot of the voltages in series RLC circuit, at resonance. |
+------------------------------------------------------------------+

.. |image0| image:: schematics/RCsteadystate.svg
.. |image1| image:: photos/RCsteadystate.png
.. |image2| image:: screenshots/rc-steadystate.png
.. |image3| image:: schematics/RLsteadystate.svg
.. |image4| image:: photos/RLsteadystate.png
.. |image5| image:: screenshots/rl-steadystate.png
.. |image6| image:: schematics/RLCsteadystate.svg
.. |image7| image:: photos/RLCsteadystate.png
.. |image8| image:: screenshots/rlc-steadystate.png

