Courants continu et alternatif
##############################

:slug: Expt17/html/ac-dc
:date: 2018-09-07
:category: schoollevel
:tags: 1School Level Activities
:title: Courants continu et alternatif
:lang: fr
       
Schéma
~~~~~~

|image0|

-  Pour déterminer la nature de la tension à une borne, il faut
   tracer le graphique de sa valeur en fonction du temps.
-  Si la tension ne change pas dans le temps, le graphique est
   une ligne horizontale, on dit que la tension est continue.
-  Tout changement dans la tension indique une composante
   alternative.
-  Quand la valeur moyenne est zéro, la tension est purement
   alternative.

Instructions
~~~~~~~~~~~~


-  Connecter les fils comme indiqué sur le schéma.
-  Régler PV1 à 2 V.
-  La courbe de PV1 sera une ligne horizontale.
-  WG génère un signal alternatif pur et on peut mesurer
   le temps d'un cycle sur le graphique pour calculer la
   fréquence.

.. |image0| image:: schematics/ac-dc.svg
   :width: 500px
