Redressement double alternance
##############################

:slug: Expt17/html/fullwave
:date: 2018-09-07
:category: expts_17
:tags: 1Electronics
:title: Redressement double alternance
:lang: fr
       
La réalisation d'un redresseur double alternance nécessite deux
signaux alternatifs, déphasés de 180° (en opposition de phase).
En général on fait ça à l'aide d'un transformateur avec une borne
centrale. On utilise les sorties \$Wg\$ et \$\\bar{WG}\$ pour faire
de même. \$Wg\$ est suivi sur le canal A1 de l'oscilloscope et
\$\\bar{WG}\$ sur le canal A2. La sortie du redresseur est connectée
à A3. Le signal observé sera un peu bruité sans la résistance de
charge, si on connecte une résistance de 1 kΩ on obtien un résultat
plus net. Connecter des condensateurs de diverses capacités pour voir
l'effet de filtrage.

+-------------------+---------------------------------------+
| |image0|          | |image1|                              |
+-------------------+---------------------------------------+
| Schéma de câblage | Photo du montage expérimental.        |
+-------------------+---------------------------------------+

                                                                          

+-----------------------------------------------------------------------+
| |image3|                                                              |
+-----------------------------------------------------------------------+
| Copie d'écran de l'oscilloscope montrant les signaux d'entré et de    |
| sortie du redresseur double alternance. Les diodes sont des 1N4148,   |
| la fréquence est 1000 Hz et la résistance de charge est 1 kΩ.         |
+-----------------------------------------------------------------------+

Le signal de sortie avec R = 1 kΩ et C = 1 µF est montré ci-dessous.
On peut voir que la fréquence des oscillations du signal de sortie est le
double de la fréquence des signaux d'entrée.

|image4|

.. |image0| image:: schematics/fullwave.svg
.. |image1| image:: photos/fullwave.jpg
.. |image2| image:: screenshots/fullwave.png
.. |image3| image:: screenshots/fullwave.png
.. |image4| image:: screenshots/fullwave-filter.png

