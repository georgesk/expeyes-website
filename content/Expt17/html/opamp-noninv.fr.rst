Amplificateur non-inverseur
^^^^^^^^^^^^^^^^^^^^^^^^^^^

:slug: Expt17/html/opamp-noninv
:date: 2018-09-07
:category: expts_17
:tags: 1Electronics
:title: Amplificateur non-inverseur
:lang: fr
       
Faire les branchements selon le diagramme ci-dessous. Ri = 1 kΩ et
Rf = 10 kΩ. L'amplitude de WG est réglée à 80 mV, on peut essayer un
signal d'entrée d'amplitude 1 V pour observer l'écrêtage du signal de
sortie, comme il dépasserait les tensions d'alimentation de
\$\\pm 6 V\$.

+-----------------------------------+-----------------------------------+
| |image2|                          | |image3|                          |
+-----------------------------------+-----------------------------------+
| Schéma de câblage                 | Photo du montage expérimental.    |
|                                   | On utilise le circuit OP07 (même  |
|                                   | brochage que le µA741)            |
+-----------------------------------+-----------------------------------+


+-----------------------------------------------------------------------+
| |image5|                                                              |
+-----------------------------------------------------------------------+
| Copie d'écran de l'oscilloscope montrant les signaux d'entrée et de   |
| sortie d'un amplificateur non inverseur. Le gain vaut 11.             |
+-----------------------------------------------------------------------+


Écriture de code Python
~~~~~~~~~~~~~~~~~~~~~~~

On peut aussi faire cette expérience à l'aide de ce `code Python
<../code/capture2.py>`__. Les courbes affichées par le programme
sont présentées ci-dessous.

+----------------------------------------------------------+--------------------+
| ..  code-block:: python                                  | |image6|           |
|                                                          |                    |
|    import eyes17.eyes                                    |                    |
|    p = eyes17.eyes.open()                                |                    |
|    from pylab import *                                   |                    |
|    p.set_sine(200)                                       |                    |
|    p.set_pv1(1.35) # va écrêter à 1,35 + seuil de diode  |                    |
|    t,v, tt,vv = p.capture2(500, 20) # capture A1 et A2   |                    |
|    xlabel('Temps (ms)'); ylabel('Tension (V)')           |                    |
|    plot([0,10], [0,0], 'black')                          |                    |
|    ylim([-4,4])                                          |                    |
|    plot(t,v,linewidth = 2, color = 'blue')               |                    |
|    plot(tt, vv, linewidth = 2, color = 'red')            |                    |
|    show()                                                |                    |
+----------------------------------------------------------+--------------------+
| programme Python pour  capturer et tracer A1 et A2       | Résultat du code   |
+----------------------------------------------------------+--------------------+

.. |image0| image:: schematics/opamp-noninv.svg
.. |image1| image:: photos/opamp-noninv.jpg
.. |image2| image:: schematics/opamp-noninv.svg
.. |image3| image:: photos/opamp-noninv.jpg
	    :width: 631px
.. |image4| image:: screenshots/opamp-noninv.png
.. |image5| image:: screenshots/opamp-noninv.png
.. |image6| image:: screenshots/opamp-inv-mpl.png
    :width: 500px
