Clock divider circuit, using D flipflop
#######################################

:slug: Expt17/html/clock-divider
:date: 2018-09-07
:category: expts_17
:tags: 1Electronics
:title: Clock divider circuit
:lang: en
       
A square wave is given to the clock input of the 74LS74 flipflop. The
Q-bar output is connected to the D input. Clear and Preset inputs should
be held HIGH. Every rising edge toggles the output, but nothing happens
at the falling edge.

+------------------+---------------------------------------+
| |image0|         | |image1|                              |
+------------------+---------------------------------------+
| Wiring Diagram   | Photograph of the experimental setup. |
+------------------+---------------------------------------+

|image2|

The duty cycle of the output waveform will be 50% irrespective of the
duty cycle of the input waveform, as shown below.

|image3|

.. |image0| image:: schematics/clock-divider.svg
.. |image1| image:: photos/clock-divider.jpg
   :width: 631px
.. |image2| image:: screenshots/clock-divider.png
.. |image3| image:: screenshots/clock-divider-dcycle.png

