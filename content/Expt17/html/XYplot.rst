RC circuits, XY plot
####################

:slug: Expt17/html/XYplot
:date: 2018-09-07
:category: expts_17
:tags: 2Electrical Circuits and LCR elements
:title: RC circuits, XY plot
:lang: en
       
Study of Series RC circuit
~~~~~~~~~~~~~~~~~~~~~~~~~~

A sinusoidal voltage is applied to a series RC circuit and the voltages
across R and C are plotted. The resulting trace becomes a circle when R
= Zc. The circle may not appear as a circle on the screen, need to look
for the frequency where Xmax = Ymax.

+------------------+---------------------------------------+
| |image0|         | |image1|                              |
+------------------+---------------------------------------+
| Wiring Diagram   | Photograph of the experimental setup. |
+------------------+---------------------------------------+

|image2|

.. |image0| image:: schematics/RCsteadystate.svg
.. |image1| image:: photos/RCsteadystate.png
.. |image2| image:: screenshots/rc-xyplot.png

