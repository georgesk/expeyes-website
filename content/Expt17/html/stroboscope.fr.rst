Stroboscope
###########

:slug: Expt17/html/stroboscope
:date: 2018-09-07
:category: schoollevel
:tags: 1School Level Activities
:title: Stroboscope
:lang: fr
       
Schéma
~~~~~~

|image0|

Instructions
~~~~~~~~~~~~

-  On fait tourner le disque en alimentant le moteur avec une pile de
   1,5 V.
-  On illumine le disque avec la lumière d'une DEL, aucune autre lumière
   ne doit être présente.
-  On ajuste la fréquence de SQ1, le disque apparaîtra stationnaire quand
   elle devient égale à la fréquence de rotation du disque.

.. |image0| image:: schematics/stroboscope.svg
   :width: 500px
