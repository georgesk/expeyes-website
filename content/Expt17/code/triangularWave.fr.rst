ExpEyes
#######
:slug: Expt17/code/triangularWave.py
:date: 2018-09-07
:lang: fr
:title: ExpEyes
       
.. code-block:: python
   :linenos:

   #import eyes17.eyes          # réactivez ces deux lignes
   #p = eyes17.eyes.open()      # pour lancer en ligne de commande
  
   # Connecter WG à A1
   
   from pylab import *
   
   p.set_wave(500,'tria')
   x,y = p.capture1('A1', 500,10)
   plot(x,y)
   show()
