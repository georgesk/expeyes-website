ExpEyes
#######
:slug: Expt17/code/clamping.py
:date: 2018-09-07
:lang: fr
:title: ExpEyes
       
.. code-block:: python
   :linenos:

   #import eyes17.eyes          # réactivez ces deux lignes
   #p = eyes17.eyes.open()      # pour lancer en ligne de commande
   
   from pylab import *
   
   p.set_sine(200)
   p.set_pv1(1.7)       # va écrêter à 2,0 V + seil de la diode
   
   maxV = 8
   
   p.select_range('A1', maxV)
   p.select_range('A2', maxV)
   
   t,v, tt,vv = p.capture2(500, 20)   # capture A1 et A2
   
   xlabel('Temps (ms)')
   ylabel('Tension (V)')
   plot([0,10], [0,0], 'black')
   ylim([-maxV, maxV])
   
   plot(t,v,linewidth = 2, color = 'blue', label='Entrée')
   plot(tt, vv, linewidth = 2, color = 'red', label='Écrêtée')
   
   legend(framealpha=0.5)
   
   show()
