ExpEyes
#######
:slug: Expt17/code/RCintegration.py
:date: 2018-09-07
:lang: fr
:title: ExpEyes
      
.. code-block:: python
   :linenos:

   #import eyes17.eyes          # réactivez ces deux lignes
   #p = eyes17.eyes.open()      # pour lancer en ligne de commande
   
   from pylab import *
   
   p.set_wave(500,'tria')
   p.select_range('A1',4)
   p.select_range('A2',4)
   
   t,v, tt,vv = p.capture2(500, 20)   # capture A1 et A2
   
   xlabel('Temps (ms)')
   ylabel('Tension (V)')
   plot([0,10], [0,0], 'black')
   ylim([-4,4])
   
   plot(t,v,linewidth = 2, color = 'blue')
   plot(tt, vv, linewidth = 2, color = 'red')
   
   show()
