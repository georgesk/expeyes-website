ExpEyes
#######
:slug: Expt17/code/RCtransient.py
:date: 2018-09-07
:lang: fr
:title: ExpEyes

.. code-block:: python
   :linenos:

   #import eyes17.eyes          # réactivez ces deux lignes
   #p = eyes17.eyes.open()      # pour lancer en ligne de commande
   
   from pylab import *
   import time
   
   p.set_state(OD1=0)			# OD1 réglé à BAS
   time.sleep(.5)
   t,v = p.capture_action('A1', 100, 5, 'SET_HIGH')
   plot(t,v,linewidth = 2, color = 'blue')
   
   p.set_state(OD1=1)			# OD1 réglé à HAUT
   time.sleep(.5)
   t,v = p.capture_action('A1', 100, 5, 'SET_LOW')
   
   plot(t,v,linewidth = 2, color = 'red')
   show()
