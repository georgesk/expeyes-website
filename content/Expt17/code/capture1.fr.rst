ExpEyes
#######
:slug: Expt17/code/capture1.py
:date: 2018-09-07
:lang: fr
:title: ExpEyes
       
.. code-block:: python
   :linenos:

   #import eyes17.eyes          # réactivez ces deux lignes
   #p = eyes17.eyes.open()      # pour lancer en ligne de commande
   
   from pylab import *
   x,y = p.capture1('A1',10,10)
   plot(x,y)
   show()
