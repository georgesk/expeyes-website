ExpEyes
#######
:slug: Expt17/code/capture4.py
:date: 2018-09-07
:lang: fr
:title: ExpEyes
       
.. code-block:: python
   :linenos:

   #import eyes17.eyes          # réactivez ces deux lignes
   #p = eyes17.eyes.open()      # pour lancer en ligne de commande
   
   from pylab import *
   
   p.set_sine(200)
   
   res = p.capture4(500, 20)   # capture A1, A2,A3 et MIC
   
   plot(res[0], res[1], linewidth = 2, color = 'blue')
   plot(res[6], res[7], linewidth = 2, color = 'red')
   show()
