ExpEyes
#######
:slug: Expt17/code/FourierTransform.py
:date: 2018-09-07
:lang: fr
:title: ExpEyes
       
.. code-block:: python
   :linenos:

   #import eyes17.eyes          # réactivez ces deux lignes
   #p = eyes17.eyes.open()      # pour lancer en ligne de commande
   
   from pylab import *
   import eyes17.eyemath17 as em
   
   p.set_sine(1000)
   p.set_sqr1(500)
   
   t,v, tt,vv = p.capture2(5000, 20)   # capture A1 et A2
   
   xlabel('Fréq')
   ylabel('Amplitude')
   xlim([0,10000])
   
   xa,ya = em.fft(v,20*0.001)
   plot(xa,ya, linewidth = 2, color = 'blue')
   
   xa,ya = em.fft(vv, 20*0.001)
   plot(xa, ya, linewidth = 2, color = 'red')
   
   show()
