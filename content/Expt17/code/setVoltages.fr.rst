ExpEyes
#######
:slug: Expt17/code/setVoltages.py
:date: 2018-09-07
:lang: fr
:title: ExpEyes
       
.. code-block:: python
   :linenos:

   #import eyes17.eyes          # réactivez ces deux lignes
   #p = eyes17.eyes.open()      # pour lancer en ligne de commande

   # Connecter PV1 à A1 et PV2 à A2, à l'aide de deux fils
   
   print p.set_pv1(2.5)    
   print p.set_pv2(1)     
   
   print p.get_voltage('A1')
   print p.get_voltage('A2')
   
