ExpEyes
#######
:slug: Expt17/code/setWave.py
:date: 2018-09-07
:lang: fr
:title: ExpEyes
       
.. code-block:: python
   :linenos:

   #import eyes17.eyes          # réactivez ces deux lignes
   #p = eyes17.eyes.open()      # pour lancer en ligne de commande
   
   # connecter WG à A1
   
   from pylab import *
   
   p.set_wave(100)
   x,y = p.capture1('A1', 500,50)
   plot(x,y)
   
   p.set_wave(100, 'tria')
   x,y = p.capture1('A1', 500,50)
   plot(x,y)
   
   show()
