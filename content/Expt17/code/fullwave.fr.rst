ExpEyes
#######
:slug: Expt17/code/fullwave.py
:date: 2018-09-07
:lang: fr
:title: ExpEyes
       
.. code-block:: python
   :linenos:

   #import eyes17.eyes          # réactivez ces deux lignes
   #p = eyes17.eyes.open()      # pour lancer en ligne de commande

   from pylab import *
   
   p.set_sine(200)
   
   res = p.capture4(500, 20)   # capture A1 et A2
   
   xlabel('Temps (ms)')
   ylabel('Tension (V)')
   plot([0,10], [0,0], 'black')
   ylim([-4,4])
   
   plot(res[0], res[1], linewidth = 2, color = 'blue')
   plot(res[2], res[3], linewidth = 2, color = 'red')
   plot(res[4], res[5], linewidth = 2, color = 'magenta')
   
   show()
