ExpEyes
#######
:slug: Expt17/code/readInputs.py
:date: 2018-09-07
:lang: en
:title: ExpEyes
       
.. code-block:: python
   :linenos:

   #import eyes17.eyes          # uncomment these two lines while running stand-alone
   #p = eyes17.eyes.open()
   
   print p.get_voltage('A1')
   print p.get_voltage('A2')
   print p.get_voltage('A3')
