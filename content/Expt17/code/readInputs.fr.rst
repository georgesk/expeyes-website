ExpEyes
#######
:slug: Expt17/code/readInputs.py
:date: 2018-09-07
:lang: fr
:title: ExpEyes
       
.. code-block:: python
   :linenos:

   #import eyes17.eyes          # réactivez ces deux lignes
   #p = eyes17.eyes.open()      # pour lancer en ligne de commande
   
   print p.get_voltage('A1')
   print p.get_voltage('A2')
   print p.get_voltage('A3')
