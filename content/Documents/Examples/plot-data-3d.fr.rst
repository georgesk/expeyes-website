ExpEyes
#######
:slug: Documents/Examples/plot-data-3d.py
:date: 2018-09-07
:lang: fr
:title: ExpEyes
       
.. code-block:: python
   :linenos:

   # Lecture de données texte multi-colonnes à partir d'un fichier et graphique des trois premières colonnes
   
   from pylab import *
   from mpl_toolkits.mplot3d import Axes3D
   ax = Axes3D(figure())
   
   dat = loadtxt('xyz.txt', unpack=True)	# lecture des données multi-colonnes
   
   ax.plot(dat[0],dat[1],dat[2])
   ax.set_xlabel('X axis')
   ax.set_ylabel('Y axis')
   ax.set_zlabel('Z axis')
   show()
