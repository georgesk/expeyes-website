ExpEyes
#######
:slug: Documents/Examples/Efield-plot.py
:date: 2018-09-07
:lang: fr
:title: ExpEyes
       
.. code-block:: python
   :linenos:

   from pylab import *
   
   N = 101
   a = linspace(-5, 5, N)
   b = linspace(-5, 5, N)
   xa, ya = meshgrid(a, b)		# Une grille dans le plan xy. xa contient les coordonnées-x
   Ex = zeros_like(xa)			# tableau 2D array pour contenir les composantes Ex
   Ey = zeros_like(ya)			# et Ey
   
   Q = [(-15, 0, 3), (5, -2, -2), (-5, 2, -2)]	# Charges (veleur, x-coord, y-coord)
   
   for q in Q:  # marquage des emplacements des charges
   text(q[1], q[2], 'o', color = 'r', fontsize=15, va='center', ha='center')
   
   for i in range(N):		# calcul de Ex et Ey en chaque point de la grille, dus à toutes les charges
   for j in range(N):
   x = xa[i,j]
   y = ya[i,j]
   for k in range(len(Q)): # somme sur les charges, à l'aide de l'équation du livre
   Ex[i,j] += Q[k][0]*(x-Q[k][1])/ ((x-Q[k][1])**2+(y-Q[k][2])**2)**(1.5)
   Ey[i,j] += Q[k][0]*(y-Q[k][2])/ ((x-Q[k][1])**2+(y-Q[k][2])**2)**(1.5)
   
   streamplot(xa, ya, Ex, Ey, density= 1.6)
   show()
