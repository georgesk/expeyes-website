ExpEyes
#######
:slug: Documents/Examples/rdecay-scipy-2.py
:date: 2018-09-07
:lang: fr
:title: ExpEyes
       
.. code-block:: python
   :linenos:

   # Décroissance radioactive. On résout 2 équations dN/dt = -L * N à la fois
   from pylab import *
   from scipy import integrate
   
   L1 = .5          # constante de décroissance
   N1 = 1000        # valeur à t = 0
   L2 = 1.0
   N2 = 2000
   
   def derivative(Y, t0):      
       return [-L1 * Y[0], -L2 * Y[1] ]  # dN/dt = -L * N, décroissance radioactive
   
   t = arange(0, 3, 0.1)                      # intervalles de temps et dates
   nt0 = [N1, N2]                             # Valeurs de Ns à t=0
   nt = integrate.odeint(derivative, nt0, t)  # intégration
   
   plot(t, nt[:,0])  # on extrait la 1ère colonne du tableau 2D
   plot(t, nt[:,1])  # on extrait la 2ème colonne du tableau 2D
   show()
   
