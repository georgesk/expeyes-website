ExpEyes
#######
:slug: Documents/Examples/integrate-scipy-quad.py
:date: 2018-09-07
:lang: fr
:title: ExpEyes
       
.. code-block:: python
   :linenos:

   # Intégration numérique pour calculer la valeur de Pi
   
   from numpy import *
   from scipy import integrate
   
   r = 1.0                          # rayon du cercle
   def f(x):				
   return sqrt(r**2 - x**2)	    # équation du cercle
   
   res = integrate.quad(f, 0, 1)    # intégrer de 0 à r
   
   print res[0] * 4, pi             # comparer l'aire x 4 à l'aire d'un cercle unité
