ExpEyes
#######
:slug: Documents/Examples/rdecay-scipy.py
:date: 2018-09-07
:lang: fr
:title: ExpEyes
       
.. code-block:: python
   :linenos:

   from pylab import *
   from scipy import integrate
   
   L = 5                  # constante de décroissance
   
   def derivative(y, t0):      
       return -L * y      # dN/dt = -L * N, décroissance radioactive
       
   N = 1000               # valeur à t = 0
   t = arange(0, 1, 0.01)                  # intervalle et dates
   
   nt = integrate.odeint(derivative, N, t) # intégration, voir la doc de scipy
   print nt
   
   plot(t, nt[:,0])                        # on extrait la première colonne du tableau 2D
   show()
   
