ExpEyes
#######
:slug: Documents/Examples/Lorentz-force-scipy.py
:date: 2018-09-07
:lang: fr
:title: ExpEyes
       
.. code-block:: python
   :linenos:

   # Trajectoire d'une particule chargée dans des champs électrique et magnétique
   from pylab import *
   from scipy import integrate
   
   m = 25.0	# Masse et
   q = 5.0	# Charge de la particule
   Ex = 0.0	# Vecteur champ électrique
   Ey = 0.0
   Ez = 0.1
   Bx = 0.0	# Vecteur champ magnétique
   By = 0.0
   Bz = 5.0
   
   def solver(X, t0): # X contient x,y,z et dx,dy,dz : 6 éléments
   vx = X[3]
   vy = X[4]
   vz = X[5]
   ax = q * (Ex + (vy * Bz) - (vz * By) ) /m	# force de Lorentz / masse
   ay = q * (Ey - (vx * Bz) + (vz * Bx) ) /m
   az = q * (Ez + (vx * By) - (vy * Bx) ) /m
   return [vx, vy, vz, ax, ay, az ]
   
   pv0 = [0,0,0, 0,1,0]		# position & velocityvitesse à t = 0
   t = arange(0, 50, 0.01)	# durée et nombre de pas
   pv = integrate.odeint(solver, pv0, t) # intégration
   
   from mpl_toolkits.mplot3d import Axes3D
   ax = Axes3D(figure())
   ax.plot(pv[:,0], pv[:,1], pv[:,2])	# graphique 3d de x, y et z
   ax.set_zlabel('Z axis')
   show()
   
   
