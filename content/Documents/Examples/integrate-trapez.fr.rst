ExpEyes
#######
:slug: Documents/Examples/integrate-trapez.py
:date: 2018-09-07
:lang: fr
:title: ExpEyes
       
.. code-block:: python
   :linenos:

   # Intégration numérique pour calculer la valeur de Pi
   from pylab import *
   
   r = 1.0                      # rayon du cercle
   dx = .05			# le delta x pour intégrer
   x = arange(0, r+dx, dx)	# tableau d'abscisses
   y = sqrt(r**2 - x**2)        # équation du cercle, r^2 = x^2 + y^2
   plot(x,y, 'x-')
   show()
   
   N = len(y)
   A = 0.0
   for k in range(N-1):		# N-1 trapézoïdes 
   at = (y[k] + y[k+1])* dx/2	# aire d'un trapézoïde
   A += at			# addition des aires
   
   print A*4, pi		# on compare l'aire du cercle calculé à Pi*r^2
   
