ExpEyes
#######
:slug: Documents/Examples/mass-spring-scipy.py
:date: 2018-09-07
:lang: fr
:title: ExpEyes
       
.. code-block:: python
   :linenos:

   # Système d'équation masse/ressort, solution à l'aide de scipy.integrate.odeint()
   from pylab import *
   from scipy import integrate
   
   k = 10.0
   m = 1.0
   
   def diff_eqn(pv, t0):                    # F = -k*x  ; a = dv/dt = F/m
   return [pv[1], -k/m * pv[0] ]
   
   pv0 = [2,0]                              # déplacement et vitesse à t = 0
   t = np.arange(0, 10, 0.01)               # intervalle de temps et étapes
   pv = integrate.odeint(diff_eqn, pv0, t)  # intégration
   
   plot(t, pv[:,0])                         # On extrait la première colonne du deuxième tableau
   plot(t, pv[:,1])
   show()
