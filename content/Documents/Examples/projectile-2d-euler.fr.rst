ExpEyes
#######
:slug: Documents/Examples/projectile-2d-euler.py
:date: 2018-09-07
:lang: fr
:title: ExpEyes
       
.. code-block:: python
   :linenos:

   # Trajectoire d'un projectile dans le plan xy, à l'aide de la méthode d'Euler
   from pylab import *
   
   t = 0.0         # on déclare la date et l'intervalle de temps
   dt = 0.1           
   x = 0.0         # positions x et y initiales
   y = 0.0
   vx = 20.0       # vitesse initiale
   vy = 20.0
   ay = -9.8       # accélération de la gravité, en direction y
   
   tm = arange(0, 4, dt)  # tableau de dates
   N = len(tm)
   xa = zeros(N)          # tableau pour les valeurs calculées de x et y
   ya = zeros(N)
   
   for k in range(N):
     xa[k] = x            # on ajoute x au tableau x
     ya[k] = y
     vy = vy + ay * dt    # on calcule la vitesse selon dt, en utilisant l'accélération
     x = x + vx * dt      # on met x à jour selon dx = v * dt
     y = y + vy * dt              
   
   plot(xa,ya)
   xlabel('x')
   show()
