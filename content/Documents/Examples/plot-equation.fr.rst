ExpEyes
#######
:slug: Documents/Examples/plot-equation.py
:date: 2018-09-07
:lang: fr
:title: ExpEyes
       
.. code-block:: python
   :linenos:

   from pylab import *
   
   x = linspace(0, 10*pi, 300)  # tableau de 300 points, de 0 à 2*Pi
   y = sin(x)                   # la fonction sinus de numpy peut traiter des tableaux
   plot(x,y)                    # Voir la documentation de Matplotlib
   xlabel('angle')
   ylabel('sine(x)')
   show()
