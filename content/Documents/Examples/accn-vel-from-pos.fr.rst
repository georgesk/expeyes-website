ExpEyes
#######
:slug: Documents/Examples/accn-vel-from-pos.py
:date: 2018-09-07
:lang: fr
:title: ExpEyes
       
.. code-block:: python
   :linenos:

   from pylab import *
   
   tmax = 5
   dt = .5
   t = arange(0, tmax, dt)              # tableau du temps
   pos = 2 * t**2          	        # simule les données x(t)
   plot(t, pos, 'o-')			# marque les points comme des cercles
   
   tm = arange(dt/2, tmax-dt, dt)	# tableau de milieux de t
   size = len(tm)
   
   vel = []	                        # liste vide pour la vitesse, dy/dt
   for k in range(size):
   vel.append( (pos[k+1]-pos[k])/dt)    # calcul de delta x/ delta y
   plot(tm, vel, 'x-')  	        # marque les points comme des croix
   
   accn = gradient(vel)  	        # function numpy pour avoir le gradient
   plot(tm, accn, 's-')	                # marque les points comme des carrés
   show()
   
