ExpEyes
#######
:slug: Documents/Examples/mass-spring-euler.py
:date: 2018-09-07
:lang: fr
:title: ExpEyes
       
.. code-block:: python
   :linenos:

   # Solution du problème masse/ressort à l'aide de l'intégration d'Euler
   
   from pylab import *
   
   k = 10.0   # constante de raideur du ressort
   m = 1.0
   dt = 0.01
   t = 0
   x = 2.0
   v = 0.0
   
   ta = [t]   # liste pour le temps et la date de début
   xa = [x]   # liste des déplacements, x = 2 à t= 0
   va = [v]   # liste des vitesses, v=0 à t=0
   
   while t < 5:
   f = -k * x
   v =  v + (f/m) * dt    # a = F/m;  a = dv/dt
   x =  x + v * dt        # v = dx/dt
   t = t + dt
   ta.append(t)
   xa.append(v)
   va.append(x)
   
   plot(ta, xa)
   plot(ta, va)
   show()
