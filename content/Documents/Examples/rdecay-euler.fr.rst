ExpEyes
#######
:slug: Documents/Examples/rdecay-euler.py
:date: 2018-09-07
:lang: fr
:title: ExpEyes
       
.. code-block:: python
   :linenos:

   # graphique de décroissance radioactive, à l'aide de la méthode d'Euler pour résoudre l'équa. diff.
   from pylab import *
   
   t = 0.
   dt = 0.1
   L = .5      # la constante de décroissance
   N = 10000.0 # Nombre d'atomes à t = 0
   
   ta = [t]    # liste pour enregistrer les dates
   na = [N]    # et le nombre instantané d'atomes
   
   while t < 5:
       dn = -L * N * dt
       N = N + dn 
       t = t + dt
       ta.append(t)
       na.append(N)
   
   plot(ta,na)
   show()
   
