ExpEyes
#######
:slug: Documents/Examples/second-order-de-scipy.py
:date: 2018-09-07
:lang: fr
:title: ExpEyes
       
.. code-block:: python
   :linenos:

   # Équa. diff. de deuxième ordre : d2x/dt2 = -x ; dp/dt = -x ; p = dx/dt
   
   from pylab import *
   from scipy import integrate
   
   
   def derivative(X, t0):        # X[0] est x, X[1] est dx/dt;
       return [X[1], -X[0] ]     # la dérivée de X[0] est X[1], celle de X[1] est -x
   
   start = [0,1]                 # x et dx/dt à t= 0
   t = np.arange(0, 30, 0.01)    # dates de début, de fin et intervalle de temps
   result = integrate.odeint(derivative, start, t)    # intégration
   
   plot(t, result[:, 0])         # on extrait la 1ère colonne du tableau 2d
   plot(t, result[:, 1])         # on extrait la 2ème colonne
   show()
   
