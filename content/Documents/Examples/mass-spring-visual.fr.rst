ExpEyes
#######
:slug: Documents/Examples/mass-spring-visual.py
:date: 2018-09-07
:lang: fr
:title: ExpEyes
       
.. code-block:: python
   :linenos:

   # Animation d'un système masse/ressort à l'aide de l'intégration d'Euler
   from visual import *
   
   wall = box (pos=(-.5,0,0), length=1, height=1, width=1, color=color.yellow)
   ball = sphere (pos=(4,0,0), radius=1, color=color.red)
   spring = helix(pos=(0,0,0), axis=(4,0,0), radius=0.35, color=color.white)
   
   t = 0         # date initiale
   dt = 0.1
   x = 2.0       # déplacement initial et vitesse
   v = 0.0
   k = 30.0
   m = 1.0
   damping = .1  # facteur d'amortissement, on peut le changer et relancer
   
   while 1:
   rate(20)
   f = -k * x  - damping * v  # amortissement dépendant de la vitesse
   v =  v + (f/m) * dt        # a = F/m;  a = dv/dt
   x =  x + v * dt            # v = dx/dt
   t = t + dt
   spring.length =  4 + x     # x est la compression ou l'expansion
   ball.x =  x + 4
   
