ExpEyes
#######
:slug: Documents/Examples/product-of-sines.py
:date: 2018-09-07
:lang: fr
:title: ExpEyes
       
.. code-block:: python
   :linenos:

   from pylab import *
   
   t = linspace(0, .01, 500)  # tableau pour le temps, variable indépendante
   
   f = 200   
   y1 = sin(2*pi*f*t)         # signal sinusoïdal à 200 Hz
   
   f = 5000 
   y2 = sin(2*pi*f*t)         # porteuse : signal sinusoïdal à 5 kHz
   
   plot(t, y1)
   plot(t, y1 * y2)           # le produit de y1 et y2
   xlabel('temps (s)')
   show()
