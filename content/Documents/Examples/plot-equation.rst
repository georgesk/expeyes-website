ExpEyes
#######
:slug: Documents/Examples/plot-equation.py
:date: 2018-09-07
:lang: en
:title: ExpEyes
       
.. code-block:: python
   :linenos:

   from pylab import *
   
   x = linspace(0, 10*pi, 300)  # 300 point array, from o to 2pi
   y = sin(x)                   # sine function of numpy can handle arrays
   plot(x,y)                    # Read Matplotlib documentation
   xlabel('angle')
   ylabel('sine(x)')
   show()
