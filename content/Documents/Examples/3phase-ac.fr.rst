ExpEyes
#######
:slug: Documents/Examples/3phase-ac.py
:date: 2018-09-07
:lang: fr
:title: ExpEyes
       
.. code-block:: python
   :linenos:

   # signaux sinusoïdaux triphasés, tension entre deux phases
   
   from pylab import *
   t = linspace(0, .05, 300)  # tableau de 300 points, entre 0 et 0,1 s
   f = 50                     # courant alternatif 50 Hz
   Vm = 230 * sqrt(2)
   y1 = Vm * sin(2*pi*f*t) 	        # phase 1
   y2 = Vm * sin(2*pi*f*t + 120*pi/180) # phase 2, à 120°
   y3 = Vm * sin(2*pi*f*t + 240*pi/180) # phase 3, à 240°
   
   plot(t, y1)		
   plot(t, y2)		
   plot(t, y3)		
   plot(t, y2-y1, color='black')		
   show()
