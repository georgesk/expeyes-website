ExpEyes
#######
:slug: Documents/Examples/pos-time-plot-euler.py
:date: 2018-09-07
:lang: fr
:title: ExpEyes
       
.. code-block:: python
   :linenos:

   from pylab import *
   
   t = 0.
   dt = .1
   x = 0
   v = 2
   
   ta = []    # liste pour enregistrer les dates
   xa = []    # et le nombre instantané d'atomes
   
   while t < 5:
     ta.append(t)
     xa.append(x)
     x = x + v * dt
     t = t + dt
   
   plot(ta,xa)
   show()
   
