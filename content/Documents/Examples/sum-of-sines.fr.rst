ExpEyes
#######
:slug: Documents/Examples/sum-of-sines.py
:date: 2018-09-07
:lang: fr
:title: ExpEyes
       
.. code-block:: python
   :linenos:

   from pylab import *
   
   t = linspace(0, 1, 500)  # tableau de dates, variable indépendante
   
   f = 100     
   y1 = sin(2*pi*f*t)       # signal sinusoïdal de 100 Hz
   
   f = 104     
   y2 = sin(2*pi*f*t)       # signal sinusoïdal de 104 Hz
   
   plot(t, y1 + y2)
   show()
