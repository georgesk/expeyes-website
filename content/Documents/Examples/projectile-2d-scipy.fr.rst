ExpEyes
#######
:slug: Documents/Examples/projectile-2d-scipy.py
:date: 2018-09-07
:lang: fr
:title: ExpEyes
       
.. code-block:: python
   :linenos:

   # Mouvement à 2 dimensions, calcul de trajectoire à l'aide de scipy.integrate
   from pylab import *
   from scipy import integrate
   
   ax = 0.0                            # accélération horizontale
   ay = -9.8                           # accélération verticale
   
   def deriv(PV, t0):                  # PV[0] est x, PV[1] est y ; PV[2] est Vx, PV[3] est Vy
     return [ PV[2], PV[3], ax, ay ]   # ax = dVx/dt, ay = dVy/dt
   
   dt = .1
   t = arange(0, 4, dt)                  # tableau des dates
   pv0 = [0,0, 20, 20]                   # x, y, Vx and Vy,  à t = 0
   pv = integrate.odeint(deriv, pv0, t)  # on intègre pour la position et la vitesse
   
   plot(pv[:,0], pv[:,1])
   show()
