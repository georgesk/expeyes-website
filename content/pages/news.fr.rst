ExpEyes
#######
:slug: news
:lang: fr
:title: ExpEyes
       
`ICFOSS <http://icfoss.in/event/expeyes-masters-training-programme>`__,
à Trivandrum, en association avec le *Department of Education, Kerala*,
a initié un programme pour inclure ExpEYES dans le programme scolaire et
la formation des professeurs. Ils ont acheté 500 kits et démarré le
développement du programme et la formation des professeurs. Le document
en Malayalam est `ICI </Documents/ExpEYES-malayalam%20.pdf>`__.

Sélection pour le *Google Summer of Code* de 2014.

**Titre du project :** `Plugins for
ExpEYES <http://www.google-melange.com/gsoc/project/details/google/gsoc2014/praveenkumar103/5685265389584384>`__
- *An Open Source Portable Science Lab*

**Organisation :**
`FOSSASIA <https://www.google-melange.com/gsoc/org2/google/gsoc2014/fossasia>`__

**Mentors:** Mario Behling, Phuc Hau & Hong Phuc

**Description courte :** ExpEYES
(*Experiments for Young Engineers and Scientists*)
est un système basé sur du matériel et du logiciel libres pour
développer des expériences scientifiques et des démonstrations de cours. Ce
projet travaillera sur le développement d'un ensemble de *plug-ins* pour
ExpEYES. Ces *plug-ins* au sein de l'interface graphique augmenteront la portée
d'ExpEYES afin de l'utiliser pour réaliser de nombreuses expériences en
mécanique, acoustique et ondes. Ce projet sera aussi destiné à ajouter des
fonctionnalités pour l'usage de capteurs acoustique et de déplacement, pour
de nombreuses expériences scientifiques.

**Étudiant :** Praveen Patil

-  `FOSS.IN 2012,  29-Nov-2012 to 1-Déc-2012,
   Bengaluru <http://www.youtube.com/watch?v=7wNq-MWa0vc>`__
-  `Turning your PC into a Science
   Lab <http://www.dnaindia.com/bangalore/report_turning-your-pc-into-a-science-lab_1749442>`__,
   Oct 6, Daily News and Analysis
-  `Lab on the Lap <http://www.downtoearth.org.in/content/lab-lap>`__,
   Down to Earth Magazine, Jul 31, 2012
-  `ExpEYES aux
   RMLL 2012 <http://schedule2012.rmll.info/Atelier-expEYES?lang=en>`__,
   presenté par K K Abdulla & P A Subha
-  `expEYES meets Raspberry
   Pi <http://www.raspberrypi.org/archives/tag/expeyes>`__ , May 16,
   2012
-  `Presentation at
   Scipy.in <http://urtalk.kpoint.com/playlist/43/kapsule/gcc-b73217cf-469a-4c52-ad22-e2c40c88bd5d>`__,
   Dec-2011, IITB, Mumbai
-  `PHOENIX at
   ADOC2.0 <http://www.youtube.com/watch?feature=player_embedded&v=F1h4q-Kw7W8>`__,
   5-6 Oct-2010, Taipei.
-  `Example of PHOENIX usage at
   UPSI <http://ijedict.dec.uwi.edu/viewarticle.php?id=1202>`__,
   Malaysia, 2011
-  `Présentation de PHOENIX
   <http://2010.rmll.info/Experimental-Physics-with-Phoenix-and-Python.html?lang=fr>`__,
   par Kishore, accepté aux RMLL 2010
-  `Innovative Science Experiments Using
   PHOENIX <http://iopscience.iop.org/0031-9120/44/5/002/?rss=1.0>`__,
   IOP Physics Education, 2009
-  `The story of
   PHOENIX <http://kishoreathrasseri.wordpress.com/2008/06/28/the-story-of-phoenix/>`__,
   by Kishore, 28-Jun2,2008
-  `Experiments Physics with Phoenix and
   Python <http://linuxgazette.net/111/pramode.html>`__, Feb-2005, by
   Pramode
