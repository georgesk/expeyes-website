Disponibilité du matériel
#########################
:slug: hardware
:date: 2018-09-07
:lang: fr
:title: Disponibilité du matériel
       

ExpEYES-17 est actuellement disponible chez les compagnies suivantes :
notez bien qu'on peut avoir le même produit chez chacune, et qu'on peut choisir
sur la base de l'opportunité et du prix.

+-----------------------+-----------------------+-----------------------+
| Fab.to.Lab (          | `S2S2  Services`_     | `S.V. Techno Crafts`_ |
| `Order Online (1)`_   | TV 33/268, Third      | 86, J.D. Nagar,       |
| )                     | Floor Elite Complex   | Patamata,             |
| Koramangala,          | Netaji Road, Kannur   | VIJAYAWADA - 520010   |
| Bangalore – 560095    | 670 001               | Ph:  09440978459      |
| Ph: +91 8050032228/9  | Kerala                | email :               |
| email: sales at       | Ph: 09447449107       | pur.svtc@gmail.com    |
| fabtolab.com          | email : s2s2service   |                       |
|                       | at gmail.com          |                       |
+-----------------------+-----------------------+-----------------------+
| `Pi Supply (1)`_      | `Shankar Systems`_    | Scientific Tech       |
| Unit 4 Bells Yew      | Plot 21, Gali 6/2,    | Services              |
| Green Business        | Block C,              | PB NO.64, North       |
| Court,                | Dechave Enclave,      | Kalarickal Lane       |
| Bells Yew Green, East | Najafgarh, NEW        | Tripunithura P O      |
| Sussex, TN3 9BJ,      | DELHI-110043.         | Cochin682301          |
| United Kingdom        | Ph:- 09810841403      | Ph: 484 2777495 ,     |
|                       | email :               | 9847113501            |
|                       | shankarsystemsdelhi19 | email: scientifictech |
|                       | 97                    | at gmail.com          |
|                       | at gmail.com          |                       |
+-----------------------+-----------------------+-----------------------+
|                       | `Amazon India`_       | `Flipkart India`_     |
|                       | distribué par :       | distribué par :       |
|                       | EMBRONICS             | EMBRONICS             |
+-----------------------+-----------------------+-----------------------+

Pour les demandes de grosse quantités, ou de partenariat commercial contacter le fabricant.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

| Cspark Research
| C202, plot-9, Sector 9,Dwarka,
| New Delhi 75
| email : csparkresearch at gmail.com
| Ph: 09780143145


Disponibilité d'ExpEYES Junior:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

+-----------------------+-----------------------+-----------------------+
| Shankar Systems       | `S2S2 Services`_      | `S.V. Techno Crafts`_ |
| Plot 21, Gali 6/2,    | TV 33/268, Third      | 86, J.D. Nagar,       |
| Block C,              | Floor Elite Complex   | Patamata,             |
| Dechave Enclave,      | Netaji Road, Kannur   | VIJAYAWADA - 520010   |
| Najafgarh, NEW        | 670 001               | Ph:  09440978459      |
| DELHI-110043.         | Kerala                | email :               |
| Ph:- 09810841403      | Ph: 09447449107       | pur.svtc@gmail.com    |
| email :               | email : s2s2service   |                       |
| shankarsystemsdelhi19 | at gmail.com          |                       |
| 97 at gmail.com       |                       |                       |
+-----------------------+-----------------------+-----------------------+
| Fab.to.Lab  (         | Vibrant Systems and   | Sys-Con Engineering   |
| `Order Online (2)`_   | Softwares             | 53B Mirza Galib       |
| )                     | 1/4869H, 1st Floor    | Street                |
| Koramangala,          | Koyisco Building,     | Kolkata 700 016       |
| Bangalore – 560095    | Wyanad Road, East     | Ph: 09830417377 , 033 |
| Ph: +91 8050032228/9  | Nadakkavu,            | 40014680              |
| email: sales at       | Calicut-673011.       | email : sceskm at     |
| fabtolab.com          | Ph: 9847193371.       | yahoo.com             |
|                       | email: vibsys_n_soft  |                       |
|                       | at yahoo.com          |                       |
+-----------------------+-----------------------+-----------------------+
| `Pi Supply (2)`_      | `BuyaPi.ca`_          |                       |
| Kent                  | 25 Northside Rd, Unit |                       |
| United Kingdom        | C                     |                       |
| Phone : 01732590665   | Ottawa, ON K2H 8S1    |                       |
| email: sales at       | Canada                |                       |
| pi-supply.com         |                       |                       |
|                       |                       |                       |
|                       |                       |                       |
|                       |                       |                       |
|                       |                       |                       |
+-----------------------+-----------------------+-----------------------+


Prix :
~~~~~~

| Le développent d'ExpEYES est fait par l'Inter-University Accelerator
  Centre, New Delhi. Comme l'objectif du projet PHOENIX est de rendre les
  produits accessibles au plus grand nombre de personnes, il restent libres
  de royalties. Le coût d'ExpEYES dépend du coût des composants bruts, de
  la manufacture, et des frais commerciaux. Les vendeurs ont fixé le prix
  de vente maximal en prenant en considération les dépenses et le secrétariat
  que cela implique, pour sécuriser les commandes et les délais de paiement
  des institutions. Cependant, ils peuvent proposer des réductions en cas
  de paiement à l'avance si bien que des acheteurs individuels peuvent
  l'avoir moins cher.
  
Le but est de toucher plus de monde :
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    ExpEYES est créé par l'IUAC et distribué sans royalties. Le logiciel est
sous licence GNU GPL et le matériel est sous licence `CERN
OHL </Documents/cern_ohl_v_1_2.pdf>`__. Ceux qui souhaitent revendre
ou manufacturer expEYES peut nous contacter. Afin d'assurer la qualité du
produit et du support utilisateur, les vendeurs potentiels doivent obéir
aux conditions suivantes :

#.     Un exemplaire doit avoir été certifié par l'IUAC pour la qualité
   du matériel et de la manufacture.
#.     Le prototype approuvé par l'IUAC doit être dupliqué sans changements.
#.     Le vendeur doit informer l'IUAC au sujet des ventes afin qu'on ait
   une trace de la communauté d'utilisateurs.

     Actuellement expEYES est disponible auprès de nombreux vendeurs en ligne.
Cependant, c'est pratique de pouvoir le trouver dans des magasins locaux,
particulièrement pour les institutions qui ne peuvent pas faire de commande
en ligne en payant par avance. Une des options est de demander à un vendeur
de votre ville de devenir revendeur d'ExpEYES. Il bénéficiera de prix
de gros de la part des fabricants et pourra le vendre à un prix
compétitif.

.. _Order Online (1): https://www.fabtolab.com/expeyes
.. _Order Online (2): http://fabtolab.com/index.php?route=product/product&product_id=80
.. _S2S2  Services: http://s2s2.in/
.. _S.V. Techno Crafts: http://www.svtechnocrafts.in/
.. _Pi Supply (1): https://uk.pi-supply.com/products/expeyes-17-your-lab-home
.. _Pi Supply (2): http://www.pi-supply.com/product-category/raspberry-pi-add-on-boards/phoenix/
.. _Shankar Systems: http://shankarsystems.github.io/
.. _S2S2 Services: http://s2s2.in/
.. _S.V. Techno Crafts: http://www.svtechnocrafts.in/
.. _BuyaPi.ca: http://www.buyapi.ca/
.. _Amazon India: http://amzn.in/d/0EdgAtx
.. _Flipkart India: http://dl.flipkart.com/dl/embronics-expeyes17-educational-electronic-hobby-kit/p/itmf9r5wafyfgxs4?pid=EHKF9Q7ASFZZRN7J&cmpid=product.share.pp
