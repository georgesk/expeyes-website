KNOW BUGS
#########
:slug: known-bugs
:tags: no-translation
:title: KNOW BUGS
       

/Expt17/html/logic-gates.html
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
- Expt17/html/screenshots/diode-and-gate.png is missing! It should be a photo of an AND
  gate implemented with two Si diodes.

/Expt17/html/scope.html
~~~~~~~~~~~~~~~~~~~~~~~
- Expt17/html/images/scope-outputs.png is missing! It should be a view of eyes17's outputs
- Expt17/html/images/scope-inputs.png is missing! It should be a view of eyes17's inputs

/Expt17/html/filterCircuit.html
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
- Expt17/html/images/filterCircuit.png is missing! it should be a schematic design for an LC filter.

/Expt17/html/inducation.html
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- "The captured waveform is shown below", at the end, ... but nothing is shown.

/Expt17/html/soundVelocity.html
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- Expt17/html/photos/sound-vel-1.jpg is missing!
- Expt17/html/screenshots/sound-velocity-zero.png!

Both files are referenced from http://expeyes.in/Expt17/html/soundVelocity.html as:

- file:///home/ajith/GIT/expeyes.github.io/Expt17/html/photos/sound-vel-1.jpg
- file:///home/ajith/GIT/expeyes.github.io/Expt17/html/screenshots/sound-velocity-zero.png

/Expt17/html/soundBeats.html
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- /Expt17/html/photos/sound-beats.jpg is missing! it was supposed to come from file:///home/ajith/GIT/expeyes.github.io/Expt17/html/photos/sound-beats.jpg
  
/MicroHOPE/mh-hardware.html
~~~~~~~~~~~~~~~~~~~~~~~~~~~

- /MicroHOPE/images/schematic.png is missing! it was supposed to give schematics for MicroHOPE; the file http://expeyes.in/expeyes-website/MicroHOPE/images/schematic.png neither exists (error 404).

/MicroHOPE/mh-software.html
~~~~~~~~~~~~~~~~~~~~~~~~~~~

- The link http://www.expeyes.herobo.com/microhope.php is broken! it redirect to a web hosting site, https://www.000webhost.com/migrate?static=true

/pages/people.html
~~~~~~~~~~~~~~~~~~

- The link to http://hackable-devices.org/shop/product/expeyes is definitely
  dead; so I remove the complete sentence.

/pages/news.html
~~~~~~~~~~~~~~~~

- the link to http://www.raspberrypi.org/archives/tag/expeyes is no longer active,
  maybe the page still exists in another place?
- the link to http://urtalk.kpoint.com/playlist/43/kapsule/gcc-b73217cf-469a-4c52-ad22-e2c40c88bd5d
  is no longer active, maybe the page still exists in another place?

/MicroHOPE/mh-hardware.html
~~~~~~~~~~~~~~~~~~~~~~~~~~~

The image /MicroHOPE/images/schematic.png is not reachable, and the link
http://expeyes.in/expeyes-website/MicroHOPE/images/schematic.png is also broken

/MicroHOPE/plugins.html
~~~~~~~~~~~~~~~~~~~~~~~

The image /MicroHOPE/images/switch8-schematic.png is missing, and it is
also missing from http://expeyes.in/expeyes-website/MicroHOPE/images/switch8-schematic.png

In the first paragraph, the phrase "For details refer to" has no href!

/MicroHOPE/ir-receiver.html
~~~~~~~~~~~~~~~~~~~~~~~~~~~

The phrase "Dowload ir-recv.c" has a typo, but most importantly, it misses a
link. Same thing in http://expeyes.in/expeyes-website/MicroHOPE/ir-receiver.html

/Expt17/html/induction.html
~~~~~~~~~~~~~~~~~~~~~~~~~~~

The plot of the captured waveform is announced but it is missing; Same thing
in http://expeyes.in/Expt17/html/induction.html

/Expt17/html/soundVelocity.html
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Two missing illustrations :
/Expt17/html/photos/sound-vel-1.jpg and /Expt17/html/screenshots/sound-velocity-zero.png
same thing at http://expeyes.in/Expt17/html/soundVelocity.html

/Expt17/html/scope.html
~~~~~~~~~~~~~~~~~~~~~~~

Two missing illustrations :
/Expt17/html/images/scope-outputs.png and /Expt17/html/images/scope-inputs.png
same thing at http://expeyes.in/Expt17/html/scope.html

