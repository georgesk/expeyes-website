One Day Workshops
#################

:slug: oneDayWorkshops
:lang: en

The objective of the "One Day Workshop" is to introduce the
computer interfaced experiments, using ExpEYES. The experiments are
demonstrated and the implementation details are explained during this
program. The requirement is a lecture hall with LCD projector.  The
host institution willing to organize such a program should be able to
arrange the participation of teachers/students from nearby
institutions.
