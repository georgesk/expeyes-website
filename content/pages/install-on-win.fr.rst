ExpEyes
#######
:slug: install-on-win
:lang: fr
:title: ExpEyes
   
| Extraire le fichier .zip téléchargé, par exemple dans C:\\

-  Installer l'interpréteur Python en double-cliquant sur
   python-2.7.14.amd64.msi (Il faut activer l'option *Add Python to the Path*
   sur un des écrans d'installation)
-  Installer le pilote USB/Série depuis le sous-répertoire
   *Driver Installation Tool*
   (les fichier présents là sont testés pour Windows 64 bit)

Redémarrer l'ordinateur (c'est la méthode avec MSwin)

Ouvrir un terminal de commande et utiliser 'pip install' pour installer
tous les fichiers .whl, par exemple

:code:`> pip install numpy-1.13.3+mkl-cp27-cp27m-win_amd64.whl`

Il faut changer le répertoire où les fichiers .whl se trouvent

| Depuis le répertoire eyes17-3.6.0, lancer le fichier main.py
| :code:`> python main.py`
