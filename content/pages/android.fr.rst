ExpEyes
#######
:slug: android
:lang: fr
:title: ExpEyes

Le support d'Android pour ExpEYES-17 n'est pas encore développé.

La réduction du **coût total** (ExpEYES + ordinateur) a été l'un de nos buts.
On a testé plus tôt ExpEYES sur la `Tablette Aakash2
<https://www.youtube.com/watch?v=oMFFx91vdKI>`__\ , sous
GNU/Linux. Cependant, la plupart des tablettes sur le marché actuellement
viennent avec Android pré-installé. La version Android du logiciel pour
ExpEYES utilise cela pour réduire le coût total. On peut l'installer
à partir du `Google play
store <https://play.google.com/store/search?q=expeyes&c=apps>`__.
Les fichiers sources sont
`ICI <https://github.com/jithinbp/ExpEYES_android>`__.
Il faut avoir un câble OTG (celui qu'on utilise pour relier une clé
USB à la tablette) pour connecter ExpEYES. Cela ne fonctionne qu'avec
les versions 4.0.0 d'Android et au-dessus. Il faut que le matériel supporte
l'USB OTG et que le système d'exploitation d'ispose d'un logiciel
d'API USB.

    L'inconvénient de la version Android est qu'on ne peut pas écrire
de simples scripts Python pour développer de nouvelles expériences, comme
cela se fait dans la version GNU/Linux. Afin d'y remédier, il y a une
option pour invoquer une séquence d'appels de fonction.


+---------------------------------------+---------------------------+
| |image0|                              | |image1|                  |
+---------------------------------------+---------------------------+
| Copie d'écran du démarrage d'ExpEYES  | L'écran de l'oscilloscope |
+---------------------------------------+---------------------------+

 La version Android est une contribution de Jithin B P , IISER Mohali,
(jithinbp at gmail.com).


|image2|


.. |image0| image:: /images/main_menu.png
	    :width: 500px
.. |image1| image:: /images/screenshot-android-oscilloscope_0.png
	    :width: 500px
.. |image2| image:: /images/jithin-bp.jpg
	    :width: 500px

