ExpEYES-17
##########
:slug: software
:lang: en
:title: ExpEYES-17
   

On Debian/Ubuntu:
~~~~~~~~~~~~~~~~~
   On Ubuntu, 14.04 or later,  download and install
   `eyes17-3.9.0.deb </Documents/eyes17-3.9.0.deb>`__ using the command

   :code:`$ sudo gdebi eyes17-3.8.0.deb`

   The PC should be connected to Internet during this, since eyes17
   depends on other packages like python-serial, numpy, scypy,  PyQt and
   python-pyqtgraph. This will only install software for ExpEYES-17, not
   for earlier version.

   For Ubuntu 18.04 onwards (it is already a part of the pre-release
   images), you may install it using

   :code:`$sudo apt install eyes17`

On Other GNU/Linux Distributions
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

   Download `eyes17.zip </Documents/eyes17.zip>`__ , unzip it and run

   :code:`$ sudo python main.py`

   | You need to install all the required python modules. To use the
     device as a normal user, `download this file </Code/postinst.sh>`__
     to set the USB permissions by running:
     
   :code:`$ sudo sh postinst.sh`

On Raspberry Pi
~~~~~~~~~~~~~~~

   Download  and install the `deb file </Documents/eyes17-rp-4.0.0.deb>`__.

Running from a Live CD
~~~~~~~~~~~~~~~~~~~~~~

   Download the `ISO
   image <http://www.iuac.res.in/%7Eelab/expeyes-16.04-v4.iso>`__ , that
   is a modified version of Ubuntu 16.04. MSWindows users may download
   the program
   `rufus <https://rufus.akeo.ie/downloads/rufus-2.15.exe>`__ and use
   that to make a USB pendrive bootable with this ISO image. After
   making the bootable pendrive, shut down the PC, insert the pendrive
   and select the boot device while starting again. For PCs having
   Windows 8 or later may have UEFI and secure boot enabled in the BIOS.
   You need to disable secure boot and choose Legacy mode instead of
   UEFI, from the BIOS settings. The keys to press to enter BIOS depends
   on the brand of the PC you have (for HP it is ESC key, for Lenovo
   Fn+F2 and Fn+F12 to select the boot  device, more info
   `HERE <http://www.disk-image.com/faq-bootmenu.htm>`__)

Installing on MS Windows
~~~~~~~~~~~~~~~~~~~~~~~~

   **Bundled Installer** : Download and install `eyes17-setup
   installer <https://drive.google.com/open?id=1p9Bfc9JpXCFWzz3cJ5WbJDrZatekglsj>`__,
   and launch eyes17 from the icon created on the desktop. This has been
   tested on Windows10.

   **Or, Install Python and the required modules to run the ExpEYES17 code**
   
   Since the programs are written in Python, the same source code works
   on GNU/Linux and Windows. However, it is much more difficult to
   gather all the installable files from Internet and get it working.
   All the required files can be downloaded from
   `HERE <http://www.iuac.res.in/%7Eelab/EYES17-Win10-files.zip>`__, and
   the instructions are given `HERE <install-on-win.html>`__.

ExpEYES Junior
~~~~~~~~~~~~~~

      Software for expeyes junior is part of Ubuntu since version 14.04.
      You may install it by:

      :code:`$ sudo apt-get install expeyes` ; or by using any GUI
      installers

For Ubuntu versions 10.04 to 12.10
----------------------------------

      you may use `this deb
      file </Documents/expeyes-ub-10.04-4.0.0.deb>`__.

On Other GNU/Linux Distributions
--------------------------------

      Download `eyes-junior.zip </Code/eyes-junior.zip>`__ and do the
      following:

      :code:`$unzip eyes-junior.zip`

      :code:`$ cd eyes-junior`

      :code:`$ sudo python croplus.py`

      | To use the device as a normal user, `download the
        file </Code/postinst.sh>`__ to set the USB permissions by
        running:
	
      :code:`$ sudo sh postinst.sh`

Windows version of the EYES Junior program
------------------------------------------

      A native Windows program is also available for expeyes junior. It
      includes the main oscilloscope application and several
      experiments. More details are
      `HERE <http://expeyes.000webhostapp.com/>`__.

