Les personnes du projet
#######################
:slug: people
:date: 2018-09-07
:lang: fr
:title: Les personnes du projet


Le projet PHOENIX a commencé en 2005 et beaucoup de personnes, tant de
l'`IUAC <http://www.iuac.res.in/indexhighres.html>`__ que d'ailleurs, ont
contribué à divers stades. Ne sont mentionnés ci-dessous que celles qui
ont travaillé au développement du matériel. Sont aussi importantes les
nombreuses personnes de la communauté académique qui s'y sont intéressées
et ont travaillé à le répandre, en menant des programmes et en l'introduisant
dans leur enseignement à certains endroits.

--------------

-  `Pramode C E <http://pramode.net/>`__, IC Software, Trissur, Kerala

A introduit l'idée d'utiliser le langage Python, voir l'article
`Python language for Phoenix <http://linuxgazette.net/111/pramode.html>`__ ,
ce qui a eu un impact fondamental sur le projet. Il a suggéré de nouvelles
idées et solutions, et il travaille aussi à faire connaître le projet.

-  `Georges
   Khaznadar <https://usb.freeduc.org>`__,
   Professeur de physique au `lycée Jean Bart, Dunkerque,
   France <https://www.lyceejeanbart.fr/>`__ (georgesk \_at\_
   debian.org)

Grâce à ses efforts le logiciel expEYES fait maintenant partie de
Debian GNU/Linux et de ses dérivés comme Ubuntu. Il s'est occupé de la
localisation du logiciel et de sa documentation,des versions sont actuellement
disponibles, il a créé un logo. Il a introduit PHOENIX en Europe, d'abord
en l'utilisant dans son lycée, puis en le faisant connaître à d'autres.
Il s'occupe du `dépôt expEYES <https://github.com/expeyes>`__ sur
github. A suggéré plusieurs idées créatives pour le projet.

-  Praveen Patil, Department of Physics,G S S College, Tilakwadi,
   Belgaum 590006

A créé et maintient un `site de blog expEYES
<http://expeyes.wordpress.com/>`__. Il est actif et organise des programmes
de formation. Il travaille à un nouveau CD vif.

-  `Jithin B P <http://jithinbp.in/>`__\ , Department of Physical and
   Material Sciences, Central University of Himachal Pradesh.

ExpEYES17 est un dérivé de `Seelab
Junior <https://github.com/jithinbp/seelab-jr>`__, le matériel libre
développé par Jithin. Il travaille aussi sur du matériel de laboratoire
pour l'enseignement de la physique nucléaire.


**Personnes de l'IUAC**

-  Ajith Kumar B.P. , RF & Electronics Laboratory, IUAC

A initié le projet PHOENIX en créant une interface basée sur le port parallèle
des PC et a organisé un programme de formation pour les enseignants en
2005, puis a persévéré. Il s'occupe de la conception, du développement des
expériences et de l'écriture du logiciel et de la documentation pour tous les
matériels de ce projet. Il passe une partie de son temps à l'organisation de
programmes de formation.

-  V V V Satyanarayana, Data Support laboratory, IUAC

Il est impliqué activement dans ce programme depuis le début, fournissant
un appui dans la fabrication et les tests du matériel, la recherche de pannes,
et ,si nécessaire, la modification des circuits analogiques impliqués. Il
a créé les circuits analogiques du spectromètre alpha. Il est aussi impliqué
dans les activités de maintenance de laboratoire d'enseignement.

D'autres personnes de l'IUAC qui ont contribué au projet sont :  Jimson
Sacharias, Kundan Singh, Parmanand Singh, Deepak Munda et S
Venkataramanan.

