Hardware Availability
#####################
:slug: hardware
:date: 2018-10-13
:lang: en
:title: Hardware Availability
       

ExpEYES-17 is currently available from the following firms: Please note
that you get the same product from all of them, and may choose based on 
price / convenience.

+-----------------------+-----------------------+-----------------------+
| Fab.to.Lab (          | `S2S2  Services`_     | `S.V. Techno Crafts`_ |
| `Order Online (1)`_   | TV 33/268, Third      | 86, J.D. Nagar,       |
| )                     | Floor Elite Complex   | Patamata,             |
| Koramangala,          | Netaji Road, Kannur   | VIJAYAWADA - 520010   |
| Bangalore – 560095    | 670 001               | Ph:  09440978459      |
| Ph: +91 8050032228/9  | Kerala                | email :               |
| email: sales at       | Ph: 09447449107       | pur.svtc@gmail.com    |
| fabtolab.com          | email : s2s2service   |                       |
|                       | at gmail.com          |                       |
+-----------------------+-----------------------+-----------------------+
| `Pi Supply (1)`_      | `Shankar Systems`_    | Scientific Tech       |
| Unit 4 Bells Yew      | Plot 21, Gali 6/2,    | Services              |
| Green Business        | Block C,              | PB NO.64, North       |
| Court,                | Dechave Enclave,      | Kalarickal Lane       |
| Bells Yew Green, East | Najafgarh, NEW        | Tripunithura P O      |
| Sussex, TN3 9BJ,      | DELHI-110043.         | Cochin682301          |
| United Kingdom        | Ph:- 09810841403      | Ph: 484 2777495 ,     |
|                       | email :               | 9847113501            |
|                       | shankarsystemsdelhi19 | email: scientifictech |
|                       | 97                    | at gmail.com          |
|                       | at gmail.com          |                       |
+-----------------------+-----------------------+-----------------------+
|                       | `Amazon India`_       | `Flipkart India`_     |
|                       | Marketed By :         | Marketed By :         |
|                       | EMBRONICS             | EMBRONICS             |
+-----------------------+-----------------------+-----------------------+

For dealership inquiries and bulk requirements contact the manufacturer.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

| Cspark Research
| C202, plot-9, Sector 9,Dwarka,
| New Delhi 75
| email : csparkresearch at gmail.com
| Ph: 09780143145


Availability of ExpEYES Junior:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

+-----------------------+-----------------------+-----------------------+
| Shankar Systems       | `S2S2 Services`_      | `S.V. Techno Crafts`_ |
| Plot 21, Gali 6/2,    | TV 33/268, Third      | 86, J.D. Nagar,       |
| Block C,              | Floor Elite Complex   | Patamata,             |
| Dechave Enclave,      | Netaji Road, Kannur   | VIJAYAWADA - 520010   |
| Najafgarh, NEW        | 670 001               | Ph:  09440978459      |
| DELHI-110043.         | Kerala                | email :               |
| Ph:- 09810841403      | Ph: 09447449107       | pur.svtc@gmail.com    |
| email :               | email : s2s2service   |                       |
| shankarsystemsdelhi19 | at gmail.com          |                       |
| 97 at gmail.com       |                       |                       |
+-----------------------+-----------------------+-----------------------+
| Fab.to.Lab  (         | Vibrant Systems and   | Sys-Con Engineering   |
| `Order Online (2)`_   | Softwares             | 53B Mirza Galib       |
| )                     | 1/4869H, 1st Floor    | Street                |
| Koramangala,          | Koyisco Building,     | Kolkata 700 016       |
| Bangalore – 560095    | Wyanad Road, East     | Ph: 09830417377 , 033 |
| Ph: +91 8050032228/9  | Nadakkavu,            | 40014680              |
| email: sales at       | Calicut-673011.       | email : sceskm at     |
| fabtolab.com          | Ph: 9847193371.       | yahoo.com             |
|                       | email: vibsys_n_soft  |                       |
|                       | at yahoo.com          |                       |
+-----------------------+-----------------------+-----------------------+
| `Pi Supply (2)`_      | `BuyaPi.ca`_          |                       |
| Kent                  | 25 Northside Rd, Unit |                       |
| United Kingdom        | C                     |                       |
| Phone : 01732590665   | Ottawa, ON K2H 8S1    |                       |
| email: sales at       | Canada                |                       |
| pi-supply.com         |                       |                       |
|                       |                       |                       |
|                       |                       |                       |
|                       |                       |                       |
|                       |                       |                       |
+-----------------------+-----------------------+-----------------------+


Pricing:
~~~~~~~~

| The development of ExpEYES is done by Inter-University Accelerator
  Centre, New Delhi. Since the objective of the PHOENIX project is to make
  the products available to maximum number of people, they  are kept
  royalty-free. The cost of ExpEYES depends on the cost of raw
  materials, labour involved, and the sales overheads. Vendors have
  fixed the Maximum Retail Price considering the expenses and paperwork
  involved in securing orders, and delays in securing payments from
  institutions. However, they will offer discounts for advance payments
  so that individual buyers get it cheaper.

Reaching out to more People:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    ExpEYES is designed by IUAC and it is distributed royalty-free. The
software is under GNU GPL and hardware is under `CERN
OHL </Documents/cern_ohl_v_1_2.pdf>`__ Those interested in retailing /
manufacturing expEYES may contact us. In order ensure quality of the
product and user support, potential vendors need to meet the following
conditions:

#.     One sample piece has to be certified by IUAC for quality of
   material and workmanship.
#.     The prototype approved by IUAC should be duplicated without
   changes
#.     Vendors should inform IUAC about the sales statistics so that 
   we are aware of the user community and distribution.

     Currently expEYES is available from several online vendors. However, it
would be convenient if it is available from local stores, especially
for institutions that cannot order online with advance payment. One
option is to ask electronics vendor in your locality to become a
dealer of ExpEYES. They will be supported with dealer prices from the
manufacturers, and will be able to sell at competitive rates.

.. _Order Online (1): https://www.fabtolab.com/expeyes
.. _Order Online (2): http://fabtolab.com/index.php?route=product/product&product_id=80
.. _S2S2  Services: http://s2s2.in/
.. _S.V. Techno Crafts: http://www.svtechnocrafts.in/
.. _Pi Supply (1): https://uk.pi-supply.com/products/expeyes-17-your-lab-home
.. _Pi Supply (2): http://www.pi-supply.com/product-category/raspberry-pi-add-on-boards/phoenix/
.. _Shankar Systems: http://shankarsystems.github.io/
.. _S2S2 Services: http://s2s2.in/
.. _S.V. Techno Crafts: http://www.svtechnocrafts.in/
.. _BuyaPi.ca: http://www.buyapi.ca/
.. _Amazon India: http://amzn.in/d/0EdgAtx
.. _Flipkart India: http://dl.flipkart.com/dl/embronics-expeyes17-educational-electronic-hobby-kit/p/itmf9r5wafyfgxs4?pid=EHKF9Q7ASFZZRN7J&cmpid=product.share.pp
