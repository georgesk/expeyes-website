Programmation en Python
#######################

:slug: python
:lang: fr

`Python <http://www.python.org/>`__ est un langage interprété avec une
syntaxe facile à apprendre, et il dispose de très bonne bibliothèques
graphiques, pour le réseau, pour le calcul scientifique, etc. Il peut
être téléchargé gratuitement et il est disponible pour tous les systèmes
d'exploitation. Le projet Phoenix utilise Python pour développer tout le code,
excepté celui qui tourne sur le micro-contrôleur en temps réel. Il y a
quantité de livres disponibles sur Internet pour apprendre Python. Le livre
en anglais `Python for Education
<http://www.iuac.res.in/%7Eelab/expeyes/Documents/mapy.pdf>`__ est un essai
pour introduire Python en tant qu'outil pour l'apprentissage des
Sciences et des Mathématiques. Pour lancer et modifier les exemples de ce livre,
un éditeur de code est disponible, sous forme de paquet Debian. On peut
le télécharger `ICI
<http://www.iuac.res.in/%7Eelab/expeyes/Documents/learn-by-coding-1.4.0.deb>`__.

Voici une copie d'écran de `ce programme <images/learn-by-coding.png>`__.

Pour des livres en français, on peut
`tenter cette recherche
<https://www.qwant.com/?q=livre+python+sciences+math%C3%A9matiques+t%C3%A9l%C3%A9chargement&client=opensearch>`__.


Python pour apprendre les Sciences et les Mathématiques
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    En collège et en lycée, on étudie diverses branches des mathématiques,
comme la géométrie, l'algèbre, la trigonométrie, le calcul symbolique, etc.
sans trop se soucier de leurs interconnexions. Tracer les graphiques
de diverses fonctions algébriques et trigonométriques aide à mieux les
comprendre. Des méthodes numériques simples peuvent démontrer la connexion
entre l'algèbre et le calcul symbolique. Cette connaissance est utile pour
aborder les problèmes en physique d'une façon différente.

Pour cela, il faut un interprète Python avec les paquets `Numpy,
Scipy <http://www.scipy.org/getting-started.html>`__ et
`Matplotlib <http://matplotlib.org/>`__.

`Vidéos Youtube
<https://www.youtube.com/channel/UCzIRARfyO-cd1VezgZlEaQw>`__

Plusieurs vidéos ont été faites quand on a écrit ces exemples et elles
ont été remontées vers YouTube. (on peut laisser fonctionner le programme
RecordmyDesktop, tout en murmurant tant qu'on tape du code, et ça génère
une ambiance comparable à celle d'une locomotive à vapeur du 19ème
siècle).

**Petits programmes Python, sur des sujets de Mathématique et de Physique de niveau Lycée**


Tous les exemples ci-dessous font moins de 20 lignes de code Python,
si on laisse de côté les lignes vides et les commentaires. Pour tout exemple
de plus de 20 lignes, il y a une version plus courte, donnée en plus.

- `plot-equation.py <../Documents/Examples/plot-equation.py.html>`__ : Tracé de
  graphiques à l'aide de numpy et matplotlib. L'exemple utilise sin(x),
  on peut aussi utiliser d'autres fonction. `Copie d'écran <../Documents/Examples/plot-equation.png>`__.

- `plot-ncurve.py <../Documents/Examples/plot-ncurve.py.html>`__ :  Tracé de
  quelques graphiques intéressants. `Copie d'écran <../Documents/Images/plot-ncurve.png>`__
- `sum-of-sines.py <../Documents/Examples/sum-of-sines.py.html>`__ : Addition
  de deux signaux sinusoïdaux qui forme des battements.
  `Copie d'écran <../Documents/Images/sum-of-sines.png>`__.
- `product-of-sines.py <../Documents/Examples/product-of-sines.py.html>`__ :
  Modulation d'amplitude par multiplication de deux fonction sinus.
  `Copie d'écran <../Documents/Images/product-of-sines.png>`__
- `3phase-ac.py <../Documents/Examples/3phase-ac.py.html>`__
  : Graphique des trois phases d'une tension du secteur de \$230 V_{eff}\$,
  et la différence de potentiel entre deux phases.
  `Copie d'écran <../Documents/Images/3phase-ac.png>`__
- `mass-spring-euler.py <../Documents/Examples/mass-spring-euler.py.html>`__ :
  Solution du problème d'une masse accrochée à un ressort par la
  méthode d'intégration d'Euler.
  `Copie d'écran <../Documents/Images/mass-spring-euler.png>`__
- `mass-spring-visual.py <../Documents/Examples/mass-spring-visual.py.html>`__ :
  Animation d'un système masse/ressort, à l'aide de python-visual, calculs
  basés sur la méthode d'Euler.
- `accn-vel-from-pos.py <../Documents/Examples/accn-vel-from-pos.py.html>`__ :
  Calcul d'accélération et de vitesses à partir de données de position.
  `Copie d'écran <../Documents/Images/accn-vel-from-pos.png>`__
- `integrate-trapez.py <../Documents/Examples/integrate-trapez.py.html>`__ :
  Calcul de l'aire sous un arc de rayon unité entre \$x=0\$ et \$1\$,
  avec la méthode des trapèzes. Comparez le résultat avec \$\\pi/4\$.
  `Copie d'écran <../Documents/Images/integrate-trapez.png>`__
- `integrate-scipy-quad.py <../Documents/Examples/integrate-scipy-quad.py.html>`__
  : Calcul de l'aire sous un arc de rayon unité entre \$x=0\$ et \$1\$,
  en utilisant la fonction scipy.integrate.quad().
- `pos-time-plot-euler.py <../Documents/Examples/pos-time-plot-euler.py.html>`__
  : Mouvement à une dimension. On donne la position initiale et la vitesse.
  On utilise l'intégration d'Euler pour tracer \$x(t)\$.
  `Copie d'écran <../Documents/Images/pos-time-plot-euler.png>`__
- `rdecay-euler.py <../Documents/Examples/rdecay-euler.py.html>`__ : courbe
  de la décroissance radioactive, calculée par la méthode d'Euler en
  résolvant l'équation \$\\frac{dN}{dt}=-L \\times N\$.
  `Copie d'écran <../Documents/Images/rdecay-euler.png>`__
- `rdecay-scipy.py <../Documents/Examples/rdecay-scipy.py.html>`__ : courbe
  de décroissance radioactive \$\\frac{dN}{dt}=-L \\times N\$, résolue
  à l'aide de la fonction scipy.integrate.odeint() .
  `Copie d'écran <../Documents/Images/rdecay-scipy.png>`__
- `rdecay2-scipy.py <../Documents/Examples/rdecay-scipy-2.py.html>`__ :
  courbe   de décroissance radioactive \$\\frac{dN}{dt}=-L \\times N\$,
  deux équations résolues à la fois à l'aide de scipy.integrate.odeint() .
  `Copie d'écran <../Documents/Images/rdecay-scipy-2.png>`__
- `second-order-de-scipy.py <../Documents/Examples/second-order-de-scipy.py.html>`__
  : résolution d'équation du second ordre en la découpant en deux équations
  du premier ordre. La solution de \$\\frac{d^2x}{dy^2}=-y\$ donne
  \$y=sin(x)\$.
  `Copie d'écran <../Documents/Images/second-order-de-scipy.png>`__
- `projectile-2d-euler.py <../Documents/Examples/projectile-2d-euler.py.html>`__
  : mouvement d'un projectile, calcul de la trajectoire plane x-y à l'aide
  de la méthode d'Euler.
  `Copie d'écran <../Documents/Images/projectile-2d-euler.png>`__
- `projectile-2d-scipy.py <../Documents/Examples/projectile-2d-scipy.py.html>`__
  : mouvement d'un projectile, calcul de la trajectoire plane x-y à l'aide
  de la fonction scipy.integrate.odeint().
  `Copie d'écran <../Documents/Images/projectile-2d-scipy.png>`__
- `mass-spring-scipy.py <../Documents/Examples/mass-spring-scipy.py.html>`__ :
  Solution du problème masse/ressort à l'aide de scipy.integrate.odeint().
  `Copie d'écran <../Documents/Images/mass-spring-scipy.png>`__
- `Lorentz-force-scipy.py <../Documents/Examples/Lorentz-force-scipy.py.html>`__
  : trajectoire d'une particule chargée dans des champs électrique et
  magnétique.
  `Copie d'écran <../Documents/Images/Lorentz-force-scipy.png>`__
- `Lorentz-force-scipy-vector.py <../Documents/Examples/Lorentz-force-scipy-vector.py.html>`__
  : trajectoire d'une particule chargée dans des champs électrique et
  magnétique, à l'aide d'équations vectorielles.
  `Copie d'écran <../Documents/Images/Lorentz-force-scipy-vector.png>`__
- `Lorentz-force-euler.py <../Documents/Examples/Lorentz-force-euler.py.html>`__
  : trajectoire d'une particule chargée dans des champs électrique et
  magnétique.
  `Copie d'écran <../Documents/Images/Lorentz-force-euler.png>`__ , le cercle
  devient une spirale à cause de l'erreur numérique.
- `plot-data-3d.py <../Documents/Examples/plot-data-3d.py.html>`__
  : lecture de données texte multi-colonnes à partir d'un fichier et
  tracé des trois premières colonnes, la
  `Copie d'écran <../Documents/Images/plot-data-3d.png>`__ montre les données
  du fichier.
- `plot-data-2d.py <../Documents/Examples/plot-data-3d.py.html>`__ :
  lecture de données texte multi-colonnes à partir d'un fichier et
  tracé des deux premières colonnes,
  `Copie d'écran <../Documents/Images/plot-data-2d.png>`__ montre les données
  du fichier.
- `Efield-plot.py <../Documents/Examples/Efield-plot.py.html>`__ :  tracé
  du champ électrique produit par plusieurs charges ponctuelles situées
  dans un plan.
  `Copie d'écran <../Documents/Images/Efield-plot.png>`__
