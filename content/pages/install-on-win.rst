ExpEyes
#######
:slug: install-on-win
:lang: en
:title: ExpEyes
   
| Extract the downloaded .zip file, for example to C:\\

-  Install the Python interpreter by double clicking on
   python-2.7.14.amd64.msi (Must enable the Add Python to the Path
   option, from one of the install screens)
-  Install the USB to Serial driver from the subdirectory Driver
   Installation Tool (the files given here are tested on 64 bit windows)

Reboot the computer ( that's the way with MSwin)

open a command terminal and use 'pip install ' to install all the .whl
files, for example

:code:`>pip install numpy-1.13.3+mkl-cp27-cp27m-win_amd64.whl`

You must be change to the subdirectory where the .whl files are

| From the directoryeyes17-3.6.0, run the file main.py
| :code:`> python main.py`
