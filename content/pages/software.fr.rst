ExpEYES-17
##########
:slug: software
:lang: fr
:title: ExpEYES-17
   

Pour Debian/Ubuntu :
~~~~~~~~~~~~~~~~~~~~

   Pour Ubuntu, 14.04 ou plus, télécharger et installer
   `eyes17-3.9.0.deb </Documents/eyes17-3.9.0.deb>`__ avec la commande

   :code:`$ sudo gdebi eyes17-3.8.0.deb`

   Il faut que le PC soit connecté à INternet pendant la manœuvre, car
   eyes17 dépend d'autres paquets commepython-serial, numpy, scypy, PyQt et
   python-pyqtgraph. Cela installera seulement le logiciel pour ExpEYES-17,
   pas pour les versions précédentes.

   Pour Debian/testing, Ubuntu 18.04 et plus, on peut l'installer à l'aide de

   :code:`$sudo apt install eyes17`

Pour les autres distributions GNU/Linux
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

   Télécharger `eyes17.zip <../Documents/eyes17.zip>`__ , le décompresser et
   lancer

   :code:`$ sudo python main.py`

   | Il faut aussi installer le modules Python requis. Afin de pouvoir
     accéder au périphérique en tant qu'utilisateur ordinaire,
     `télécharger ce fichier <../Code/postinst.sh>`__
     pour régler les permissions USB en lançant :
     
   :code:`$ sudo sh postinst.sh`

Sur Raspberry Pi
~~~~~~~~~~~~~~~~

   Télécharger et installer `le fichier .deb <../Documents/eyes17-rp-4.0.0.deb>`__.

Lancement depuis un CD vif (*Live CD*)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

   Télécharger l'`image ISO
   <http://www.iuac.res.in/%7Eelab/expeyes-16.04-v4.iso>`__ , qui
   est une version modifiée d'Ubuntu 16.04.
   Les utilisateurs de MSWindows peuvent télécharger le programme
   `rufus <https://rufus.akeo.ie/downloads/rufus-2.15.exe>`__ et
   l'utiliser pour en faire une clé USB vive (*bootable*) à partir de l'image
   ISO. Quand la clé USB vive est réalisée, éteindre le PC, insérer la clé USB
   et sélectionner le média de démarrage quand l'ordinateur s'initialise.
   Pour les PC ayant Windows 8 ou plus, on peut activer l'*UEFI* et le
   *secure boot* dans le BIOS. Sinon, il faut désactiver le *secure boot*
   et coisir le mode *Legacy* au lieu de *UEFI*, dans les réglages du BIOS.
   Les touches à presser pour commander au BIOS dépendent de la marque du PC
   qu'on a (pour HP c'est la touche Échap, pour Lenovo
   Fn+F2 et Fn+F12 pour choisir le media de démarrage, on trouve plus
   d'information 
   `ICI <http://www.disk-image.com/faq-bootmenu.htm>`__)

Installation sous MS Windows
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

   **Installeur intégré** : Télécharger et installer `l'installeur eyes17-setup
   <https://drive.google.com/open?id=1p9Bfc9JpXCFWzz3cJ5WbJDrZatekglsj>`__,
   et lancer eyes17 à partir de l'icône créée sur le bureau. Ceci a été
   testé pour Windows 10.

   **Ou encore, installer Python et les modules requis pour faire fonctionner le code d'ExpEYES17**
   
   Comme les programmes sont écrits en Python, le même code source fonctionne
   sous GNU/Linux et Windows. Cependant, il est plus difficile de rassembler
   tous les fichiers à installer depuis Internet et les faire fonctionner.
   On peut trouver tous les fichiers nécessaires
   `ICI <http://www.iuac.res.in/%7Eelab/EYES17-Win10-files.zip>`__, et
   les instructions sont données `LÀ <install-on-win.html>`__.

ExpEYES Junior
~~~~~~~~~~~~~~

      Le logiciel pour ExpEYES Junior fait partir de Debian dans la
      version stable, et d'Ubuntu depuis la version 14.04. On peut
      l'installer par :

      :code:`$ sudo apt-get install expeyes` ; ou en utilisant les
      installer avec l'interface graphique.

Pour Ubuntu versions 10.04 à 12.10
----------------------------------

      On peut utiliser `ce fichier .deb
      </Documents/expeyes-ub-10.04-4.0.0.deb>`__.

Pour les autres distributions GNU/Linux
---------------------------------------

      Télécharger `eyes-junior.zip <../Code/eyes-junior.zip>`__ et faire
      comme ci-dessous :

      :code:`$unzip eyes-junior.zip`

      :code:`$ cd eyes-junior`

      :code:`$ sudo python croplus.py`

      | Pour utiliser le périphérique comme utilisateur ordinaire,
        `télécharger le fichier </Code/postinst.sh>`__ pour régler les
	permission USB en lançant :
	
      :code:`$ sudo sh postinst.sh`

Versions Windows du programme pour EYES Junior
----------------------------------------------

      Un programme Windows natif est aussi disponible pour ExpEYES Junior.
      Il inclut l'oscilloscope principal et plusieurs expériences. Vous trouvez
      plus de détails
      `ICI <http://expeyes.000webhostapp.com/>`__.

