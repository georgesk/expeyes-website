Python Programming
##################

:slug: python
:lang: en

`Python <http://www.python.org/>`__ is an interpreted language with an
easy to learn syntax and has very good libraries for graphics,
networking, scientific computation etc. It is freely downloadable and
available on almost all operating systems. The Phoenix project uses
Python for developing all the code except the one running on the
micro-controller. There are so many books available on Internet for
learning Python. The book `Python for
Education <http://www.iuac.res.in/%7Eelab/expeyes/Documents/mapy.pdf>`__
tries to introduce Python as a tool for learning Science & Mathematics.
To run and modify the examples given in this book, a code browser
program also is available as a Debian package. 

Download the `Book in PDF
forma <http://www.iuac.res.in/%7Eelab/expeyes/Documents/mapy.pdf>`__\ t
and the  `code example
browser <http://www.iuac.res.in/%7Eelab/expeyes/Documents/learn-by-coding-1.4.0.deb>`__.
A screen-shot of this program is `here <images/learn-by-coding.png>`__.
  (`Python book <Documents/mapy.pdf>`__ and `Code
Browser <Documents/learn-by-coding-1.4.0.deb>`__)

Python for learning Science and Maths
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    In the high school classes, we learn about the various branches of
mathematics, like geometry, algebra, trigonometry, calculus etc. ,
without bothering much about their interconnections. Plotting the graphs
of different algebraic and trigonometric functions helps understanding
them better. Simple numerical methods can demonstrate the connection
between algebra and calculus. This knowledge is useful for for exploring
the problems in physics in a different manner.

The software requirement is Python Interpreter along with `Numpy, 
Scipy <http://www.scipy.org/getting-started.html>`__ and
`Matplotlib <http://matplotlib.org/>`__ packages.

`Youtube
Videos <https://www.youtube.com/channel/UCzIRARfyO-cd1VezgZlEaQw>`__

Several videos made during writing these examples also have been
uploaded to the Youtube. (Keeping the RecordmyDesktop program running
and mumbling while writing code can generate the drag and audio
comparable to that of a 19th century steam locomotive)
	 
**Short Python Programs, based on Higher Secondary Mathematics and Physics topics**


All the examples below have less than 20 lines of Python code, excluding
empty lines and comments. If any example exceeds 20 lines, you will find
a shorter version also given.

- `plot-equation.py <../Documents/Examples/plot-equation.py.html>`__ : Plotting
  Graphs using numpy and matplotlib. The example demonstrates sine(x), you
  may also try other functions. `screenshot <../Documents/Examples/plot-equation.png>`__

- `plot-ncurve.py <../Documents/Examples/plot-ncurve.py.html>`__ :  Plot some
  interesting graphs . `screenshot <../Documents/Images/plot-ncurve.png>`__
- `sum-of-sines.py <../Documents/Examples/sum-of-sines.py.html>`__ : Sum of two
  sine waves result in beats. 
  `screenshot <../Documents/Images/sum-of-sines.png>`__.
- `product-of-sines.py <../Documents/Examples/product-of-sines.py.html>`__ :
  Amplitude modulation by multiplying two sine waves. 
  `screenshot <../Documents/Images/product-of-sines.png>`__
- `3phase-ac.py <../Documents/Examples/3phase-ac.py.html>`__
  : Plots the 3 phases of 230 Vrms AC mains, and the voltage difference
  between two phases.  `screenshot <../Documents/Images/3phase-ac.png>`__
- `mass-spring-euler.py <../Documents/Examples/mass-spring-euler.py.html>`__ :
  Solve the mass on a spring system using Euler method of integration.
  `screenshot <../Documents/Images/mass-spring-euler.png>`__
- `mass-spring-visual.py <../Documents/Examples/mass-spring-visual.py.html>`__ :
  Mass Spring system animation using python-visual, computation using
  Euler method of integration
- `accn-vel-from-pos.py <../Documents/Examples/accn-vel-from-pos.py.html>`__ :
  Calculate acceleration and velocity from position data.
  `screenshot <../Documents/Images/accn-vel-from-pos.png>`__
- `integrate-trapez.py <../Documents/Examples/integrate-trapez.py.html>`__ :
  Calculate the area under an arc of unit radius from x=0 to 1, using
  trapezoid rule . Compare the result with pi/4.
  `screenshot <../Documents/Images/integrate-trapez.png>`__
- `integrate-scipy-quad.py <../Documents/Examples/integrate-scipy-quad.py.html>`__
  : Calculate the area under an arc of unit radius from x=0 to 1, using
  scipy.integrate.quad() function
- `pos-time-plot-euler.py <../Documents/Examples/pos-time-plot-euler.py.html>`__
  :  One dimensional motion. Velocity and initial position given. Use
  Euler integration to plot x(t) .
  `screenshot <../Documents/Images/pos-time-plot-euler.png>`__
- `rdecay-euler.py <../Documents/Examples/rdecay-euler.py.html>`__ : Plot the
  radioactive decay curve calculated using Euler method of solving
  differential equation dN/dt = -L \* N.
  `screenshot <../Documents/Images/rdecay-euler.png>`__
- `rdecay-scipy.py <../Documents/Examples/rdecay-scipy.py.html>`__ : Radioactive
  decay curve. dN/dt = -L \* N, solved using scipy.integrate.odeint() .
  `screenshot <../Documents/Images/rdecay-scipy.png>`__
- `rdecay2-scipy.py <../Documents/Examples/rdecay-scipy-2.py.html>`__ :
  Radioactive decay curve. dN/dt = L \* N. Two of them solved at a time
  using scipy.integrate.odeint() .
  `screenshot <../Documents/Images/rdecay-scipy-2.png>`__
- `second-order-de-scipy.py <../Documents/Examples/second-order-de-scipy.py.html>`__
  : Solving second order equation by splitting into two first order
  ones. solve d2y/dx2 = -y  gives  y=sin(x). 
  `screenshot <../Documents/Images/second-order-de-scipy.png>`__
- `projectile-2d-euler.py <../Documents/Examples/projectile-2d-euler.py.html>`__
  : Projectile motion, trajectory in x-y plane calculated using Euler's
  method. `screenshot <../Documents/Images/projectile-2d-euler.png>`__
- `projectile-2d-scipy.py <../Documents/Examples/projectile-2d-scipy.py.html>`__
  : Projectile motion, trajectory in x-y plane calculated using
  scipy.integrate.odeint().
  `screenshot <../Documents/Images/projectile-2d-scipy.png>`__
- `mass-spring-scipy.py <../Documents/Examples/mass-spring-scipy.py.html>`__ :
  Solve the mass on a spring system using scipy.integrate.odeint().
  `screenshot <../Documents/Images/mass-spring-scipy.png>`__
- `Lorentz-force-scipy.py <../Documents/Examples/Lorentz-force-scipy.py.html>`__
  : Trajectory of a charged particle in electric and magnetic fields.
  `screenshot <../Documents/Images/Lorentz-force-scipy.png>`__
- `Lorentz-force-scipy-vector.py <../Documents/Examples/Lorentz-force-scipy-vector.py.html>`__
  : Trajectory of a charged particle in electric and magnetic fields,
  using vector equations.
  `screenshot <../Documents/Images/Lorentz-force-scipy-vector.png>`__
- `Lorentz-force-euler.py <../Documents/Examples/Lorentz-force-euler.py.html>`__
  : Trajectory of a charged particle in electric and magnetic fields.
  `screenshot <../Documents/Images/Lorentz-force-euler.png>`__ , circle is
  becoming spiral due to computational error.
- `plot-data-3d.py <../Documents/Examples/plot-data-3d.py.html>`__
  : Reads multi-column text data from files and plots the first 3
  columns, `screenshot <../Documents/Images/plot-data-3d.png>`__ shows the
  data from file.
- `plot-data-2d.py <../Documents/Examples/plot-data-3d.py.html>`__ : Reads
  multi-column text data from files and plots the first 2 columns,
  `screenshot <../Documents/Images/plot-data-2d.png>`__ shows the data from
  file.
- `Efield-plot.py <../Documents/Examples/Efield-plot.py.html>`__ :  Plots the
  electric field produced by several point charges located in a plane.
  `screenshot <../Documents/Images/Efield-plot.png>`__
