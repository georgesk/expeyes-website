Parts list
##########
:slug: partList
:date: 2018-09-07
:lang: en
:title: Parts list
       

+------------------------------+-----+
| Part #                       | Qty |
+------------------------------+-----+
| `PIC24EP256GP204-I/PT`_      |   1 |
+------------------------------+-----+
| `MCP2200-I/SS`_              |   1 |
+------------------------------+-----+
| `TC1240A`_                   |   1 |
+------------------------------+-----+
| `MCP6S21-I/SN`_              |   2 |
+------------------------------+-----+
| `TC7660`_                    |   1 |
+------------------------------+-----+
| `74HC126`_                   |   2 |
+------------------------------+-----+
| `REF3033`_                   |   1 |
+------------------------------+-----+
| `LM324`_                     |   1 |
+------------------------------+-----+
| `TL082`_                     |   5 |
+------------------------------+-----+
| `LM1117-3.3`_                |   1 |
+------------------------------+-----+
| `12MHz`_ Crystal             |   1 |
+------------------------------+-----+
| `.5A resettable Fuse`_       |   1 |
+------------------------------+-----+
| 6v8 zener /sot23             |   2 |
+------------------------------+-----+
| PN3906/sot23                 |   2 |
+------------------------------+-----+
| PN3904                       |   1 |
+------------------------------+-----+
| BAT54S/sot23                 |   1 |
+------------------------------+-----+
| `100uF/16V Tantalum/Case C`_ |   4 |
+------------------------------+-----+
| `22u /16V 1206`_             |   2 |
+------------------------------+-----+
| `10u /10V 0805`_             |   5 |
+------------------------------+-----+
| `1uF /10V 0805`_             |   3 |
+------------------------------+-----+
| 0.1uF/0805                   |  28 |
+------------------------------+-----+
| 10nF/ 0805                   |   1 |
+------------------------------+-----+
| 1nF/0805                     |   1 |
+------------------------------+-----+
| 3.3pF/0805                   |   2 |
+------------------------------+-----+
| 10K/0805 1%                  |  31 |
+------------------------------+-----+
| 200K/0805                    |   3 |
+------------------------------+-----+
| 1M/0805                      |   2 |
+------------------------------+-----+
| 100R/0805                    |   2 |
+------------------------------+-----+
| 18R/0805                     |   2 |
+------------------------------+-----+
| 10M/0805                     |   1 |
+------------------------------+-----+
| 1K/0805                      |   4 |
+------------------------------+-----+
| 100K/0805                    |   1 |
+------------------------------+-----+
| 5.1K/0805                    |   9 |
+------------------------------+-----+
| SPST Switch                  |   1 |
+------------------------------+-----+
| 16pin Spring loaded terminal |   2 |
| block                        |     |
+------------------------------+-----+
| USB mini B                   |   1 |
+------------------------------+-----+
| 124mm x 38mm 4layer PCB      |   1 |
+------------------------------+-----+
| Plastic babinet              |   1 |
+------------------------------+-----+

.. _PIC24EP256GP204-I/PT: http://in.element14.com/microchip/pic24ep256gp204-i-pt/mcu-16bit-pic24-70mhz-tqfp-44/dp/2212155
.. _MCP2200-I/SS: http://in.element14.com/microchip/mcp2200-i-ss/ic-usb2-0-to-uart-w-gpio-20ssop/dp/1781149?st=mcp2200
.. _TC1240A: http://in.element14.com/microchip/tc1240aechtr/ic-charge-pump-cmos-sot23a-6/dp/1605579
.. _MCP6S21-I/SN: http://in.element14.com/microchip/mcp6s21-i-sn/amp-pg-1ch-12-mhz-spi-smd-soic8/dp/1332152
.. _TC7660: http://in.element14.com/microchip/tc7660eoa/ic-charge-pump-dc-dc-conv-8soic/dp/1852313?st=TC7660
.. _74HC126: http://in.element14.com/nexperia/74hc126d/ic-74hc-cmos-smd-74hc126-soic14/dp/1085233
.. _REF3033: http://in.element14.com/texas-instruments/ref3033aidbzt/voltage-ref-series-3-3v-sot-23/dp/1470325
.. _LM324: http://in.element14.com/texas-instruments/lm324adr/op-amp-quad-low-power-smd-soic14/dp/8389233
.. _TL082: http://in.element14.com/stmicroelectronics/tl082idt/op-amp-dual-4mhz-l-bias-8soic/dp/1750155
.. _LM1117-3.3: http://in.element14.com/stmicroelectronics/ld1117s33tr/v-reg-ldo-3-3v-smd-1117-sot-223/dp/1202826
.. _12MHz: http://in.element14.com/abracon/abm8-12-000mhz-b2-t/crystal-12mhz-18pf-3-2-x-2-5mm/dp/2467828
.. _.5A resettable Fuse: http://in.element14.com/littelfuse/1812l050pr/resettable-fuse-series-1812l/dp/1653478
.. _100uF/16V Tantalum/Case C: https://www.mouser.in/Passive-Components/Capacitors/Tantalum-Capacitors/_/N-75hqv?P=1yx4aw3Z1yzt836&Keyword=100uF+16V&FS=True
.. _22u /16V 1206: http://in.element14.com/walsin/1206x226k160ct/cap-mlcc-x5r-22uf-16v-1206-reel/dp/2495654?st=22u%20/1206
.. _10u /10V 0805: http://in.element14.com/multicomp/mctt31f106z100ct/ceramic-capacitor-10uf-10v-y5v/dp/1708675
.. _1uF /10V 0805: http://in.element14.com/multicomp/mc0805b105k100ct/cap-mlcc-x7r-1uf-10v-0805/dp/2320857
