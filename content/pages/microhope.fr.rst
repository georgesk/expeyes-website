ExpEyes
#######
:slug: microhope
:lang: fr
:title: ExpEyes
       
Micro-controllers for Hobby Projects and Education  (dernière version, `KuttyPy <kuttypy/index.html>`__)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. raw:: html

	 <div style="float:right; margin-left: 1em;">
	 
.. image:: ../MicroHOPE/images/microhope-photo-horiz.jpg
	   :width: 353px
	   :alt: le circuit imprimé de microhope
.. raw:: html

	 </div>


       MicroHOPE est un système de développement de micro-contrôleur basé sur
un Atmel ATmega32. Il est développé par l'IUAC, New Delhi, et distribué comme
matériel libre. À la différence d'autres systèmes, le principal y est d'attirer
l'attention sur les fondements de l'architecture d'un micro-contrôleur plutôt
que d'étudier les possibilités du kit de développement. La connaissance
acquise sur l'Atmega32 à l'aide de MicroHOPE peut s'appliquer facilement à
la compréhension d'autres micro-contrôleurs. Il n'y a pas d'interface graphique
complexe pour MicroHOPE. Un simple éditeur de texte avec quelques boutons
supplémentaires permettant d'invoquer :code:`avrgcc` et :code:`avrdude` (pour
compiler et injecter le code), et c'est tout. Il a été testé sur GNU/Linux
et MS-Windows. Des paquets Debian et Ubuntu sont disponibles.

      On commence avec des programmes C simples, qui traitent le µC comme
un appareil avec des bornes d'entrée/sortie programmables, puis on introduit
des périphériques comme un convertisseur analogique-numérique, des ports
de communication, etc. L'architecture du µC est explorée à l'aide de programmes
en langage assembleur simples et des DELs connectées aux ports d'entrée-sortie.

Le manuel utilisateur, au format PDF, peut être téléchargé depuis
`ICI </Documents/microhope-en.pdf>`__

`Matériel <../MicroHOPE/mh-hardware.html>`__

`Logiciel <../MicroHOPE/mh-software.html>`__

`Bibliothèque et exemples <../MicroHOPE/library.html>`__

`Exemples en assembleur <../MicroHOPE/assembler.html>`__

`Enregistrement et restitution audio <../MicroHOPE/audio.html>`__

`Récepteur à infrarouge <../MicroHOPE/ir-receiver.html>`__

Programmeur `USBASP <../MicroHOPE/usbasp.html>`__


