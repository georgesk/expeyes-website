Expeyes Junior:
###############

:slug: ejun
:lang: fr
       
.. role:: raw-html(raw)
	  :format: html


.. image:: /images/ejphoto.jpg
	    :width: 300px
	    :alt: photo d'expeyes junior

-  Manuel Utilisateur d'ExpEYES Junior. À télécharger
   `ici </Documents/fr-eyesj.pdf>`__
-  Le manuel du programmeur. À télécharger
   `ici </Documents/fr-eyesj-progman.pdf>`__
-  Introduction à ExpEYES, voir des vidéos sur
   `you-tube <https://www.youtube.com/watch?v=ils7HBFMQ4Y>`__
   (préparées avec l'aide du NCERT, mises en place par Shaju K Y)
-  Programmer ExpEYES avec Python,
   `vidéo <https://www.youtube.com/watch?v=5P5Qu1qnYWY>`__
-  `Tutoriel parlé sur ExpEYES <https://www.youtube.com/watch?v=z8iVJdpkwIY>`__  (en anglais)
-  Brochure ExpEYES. À télécharger
   `ici </Documents/ej-brochure-fr.pdf>`__)
	 
:raw-html:`<div class="stackedImages">`

+-----------------------------------------------------+
| .. image:: /icons/croplus.png                       |
|   :target: experiments/croplus.html                 |
|   :alt: interface Oscillo                           |
|                                                     |
| EYES, l'interface graphique                         |
+-----------------------------------------------------+

+-----------------------------------------------------+
| .. image:: /icons/ad-dc.png                         |
|   :target: experiments/ac-dc.html                   |
|   :alt: courants alternatif et continu              |
|                                                     |
| Courants alternatif et continu                      |
+-----------------------------------------------------+

+-----------------------------------------------------+
| .. image:: /icons/resistor-compare.jpg              |
|   :target: experiments/resistance.html              |
|   :alt: Mesure d'une résistance                     |
|                                                     |
| Résistance                                          |
+-----------------------------------------------------+

+-----------------------------------------------------+
| .. image:: /icons/water-conductivity.jpg            |
|   :target: experiments/water resistance.html        |
|   :alt: Résistance de l'eau                         |
|                                                     |
| Résistance de l'eau                                 |
+-----------------------------------------------------+

+-----------------------------------------------------+
| .. image:: /icons/pickup.png                        |
|   :target: experiments/pickup.html                  |
|   :alt: photo                                       |
|                                                     |
| Signal d'antenne du secteur                         |
+-----------------------------------------------------+

+-----------------------------------------------------+
| .. image:: /icons/parallelplate-cap.jpg             |
|   :target: experiments/capacitance.html             |
|   :alt: photo                                       |
|                                                     |
| Condensateur                                        |
+-----------------------------------------------------+

+-----------------------------------------------------+
| .. image:: /icons/glassplate-cap.jpg                |
|   :target: experiments/dielectric.html              |
|   :alt: photo                                       |
|                                                     |
| Constante diélectrique                              |
+-----------------------------------------------------+

+-----------------------------------------------------+
| .. image:: /icons/acdc-sep-screen.png               |
|   :target: experiments/ac-dc-separation.html        |
|   :alt: photo                                       |
|                                                     |
| Séparation de courant continu et alternatif         |
+-----------------------------------------------------+

+-----------------------------------------------------+
| .. image:: /icons/ac-circuits.png                   |
|   :target: experiments/ac-circuits.html             |
|   :alt: photo                                       |
|                                                     |
| Circuits en courant alternatif                      |
+-----------------------------------------------------+

+-----------------------------------------------------------+
| .. image:: /icons/RC-curves.png                           |
|    :target: experiments/rc-transient.html                 |
|    :alt: photo                                            |
|                                                           |
| Circuit RC, signal transitoire                            |
+-----------------------------------------------------------+

+-----------------------------------------------------------+
| .. image:: /icons/RL-curves.png                           |
|    :target: experiments/rl-transient.html                 |
|    :alt: photo                                            |
|                                                           |
| Circuit RL, signal transitoire                            |
+-----------------------------------------------------------+

+-----------------------------------------------------------+
| .. image:: /icons/RLC-curves.png                          |
|    :target: experiments/rlc-transient.html                |
|    :alt: photo                                            |
|                                                           |
| Circuit RLC, signal transitoire                           |
+-----------------------------------------------------------+

+-----------------------------------------------------------+
| .. image:: /icons/rc-integ1khz.png                        |
|    :target: experiments/rc-integration.html               |
|    :alt: photo                                            |
|                                                           |
| Intégration avec un circuit RC                            |
+-----------------------------------------------------------+

+-----------------------------------------------------------+
| .. image:: /icons/induction-screen.png                    |
|    :target: experiments/em-induction.html                 |
|    :alt: photo                                            |
|                                                           |
| Induction électromagnétique                               |
+-----------------------------------------------------------+

+-----------------------------------------------------------+
| .. image:: /icons/transformer-screen.png                  |
|    :target: experiments/ac-generator.html                 |
|    :alt: photo                                            |
|                                                           |
| Transformateur                                            |
+-----------------------------------------------------------+

+-----------------------------------------------------------+
| .. image:: /icons/ac-generator.jpg                        |
|    :target: experiments/ac-generator.html                 |
|    :alt: photo                                            |
|                                                           |
| Générateur alternatif                                     |
+-----------------------------------------------------------+

+-----------------------------------------------------------+
| .. image:: /icons/singing-magnet.jpg                      |
|    :target: experiments/singing-magnet.html               |
|    :alt: photo                                            |
|                                                           |
| L'aimant qui chante                                       |
+-----------------------------------------------------------+

+-----------------------------------------------------------+
| .. image:: /icons/driven-pendulum.jpg                     |
|    :target: experiments/driven-pendulum.html              |
|    :alt: photo                                            |
|                                                           |
| Pendule excité                                            |
+-----------------------------------------------------------+

+-----------------------------------------------------------+
| .. image:: /icons/blinking-led.jpg                        |
|    :target: experiments/persistance-vision.html           |
|    :alt: photo                                            |
|                                                           |
| DEL clignotante                                           |
+-----------------------------------------------------------+

+-----------------------------------------------------------+
| .. image:: /icons/stroboscope.jpg                         |
|    :target: experiments/stroboscope.html                  |
|    :alt: photo                                            |
|                                                           |
| Stroboscope                                               |
+-----------------------------------------------------------+

+-----------------------------------------------------------+
| .. image:: /icons/light-barrier.jpg                       |
|    :target: experiments/light-barrier.html                |
|    :alt: photo                                            |
|                                                           |
| Barrière photo-électrique                                 |
+-----------------------------------------------------------+

+-----------------------------------------------------------+
| .. image:: /icons/opto-electric-transmission.png          |
|    :target: experiments/opto-coupler.html                 |
|    :alt: photo                                            |
|                                                           |
| Opto-Coupleur                                             |
+-----------------------------------------------------------+

+-----------------------------------------------------------+
| .. image:: /icons/rodpend.jpg                             |
|    :target: experiments/gravity-pendulum.html             |
|    :alt: photo                                            |
|                                                           |
| Mesure de la gravité avec un pendule                      |
+-----------------------------------------------------------+

+-----------------------------------------------------------+
| .. image:: /icons/LDR.jpg                                 |
|    :target: experiments/ldr.html                          |
|    :alt: photo                                            |
|                                                           |
| Étude d'une photorésistance (LDR)                         |
+-----------------------------------------------------------+

+-----------------------------------------------------------+
| .. image:: /icons/half-wave-filter-screen.png             |
|    :target: experiments/half-wave.html                    |
|    :alt: photo                                            |
|                                                           |
| Redressement simple alternance                            |
+-----------------------------------------------------------+

+-----------------------------------------------------------+
| .. image:: /icons/full-wave.png                           |
|    :target: experiments/full-wave.html                    |
|    :alt: photo                                            |
|                                                           |
| Redressement double alternance                            |
+-----------------------------------------------------------+

+-----------------------------------------------------------+
| .. image:: /icons/diode-iv-screen.png                     |
|    :target: experiments/diode-iv.html                     |
|    :alt: photo                                            |
|                                                           |
| Caractéristique courant-tension d'une diode               |
+-----------------------------------------------------------+

+-----------------------------------------------------------+
| .. image:: /icons/transistor-ce.png                       |
|    :target: experiments/transistor-ce.html                |
|    :alt: photo                                            |
|                                                           |
| Caractéristique collecteur-émetteur d'un transistor       |
+-----------------------------------------------------------+

+-----------------------------------------------------------+
| .. image:: /icons/logic-gate.jpg                          |
|    :target: experiments/logic-gates.html                  |
|    :alt: photo                                            |
|                                                           |
| Porte logique                                             |
+-----------------------------------------------------------+

+-----------------------------------------------------------+
| .. image:: /icons/ic555-screen.png                        |
|    :target: experiments/555-oscillator.html               |
|    :alt: photo                                            |
|                                                           |
| Oscillateur à circuit 555                                 |
+-----------------------------------------------------------+

+-----------------------------------------------------------+
| .. image:: /icons/mono555-screen.png                      |
|    :target: experiments/555-monoshot.html                 |
|    :alt: photo                                            |
|                                                           |
| Monostable à circuit 555                                  |
+-----------------------------------------------------------+

+-----------------------------------------------------------+
| .. image:: /icons/clock-divider.png                       |
|    :target: experiments/clock-divider.html                |
|    :alt: photo                                            |
|                                                           |
| Diviseur d'horloge                                        |
+-----------------------------------------------------------+

+-----------------------------------------------------------+
| .. image:: /icons/am-screen.png                           |
|    :target: experiments/am-fm.html                        |
|    :alt: photo                                            |
|                                                           |
| Modulations d'amplitude et de fréquence                   |
+-----------------------------------------------------------+

+-----------------------------------------------------------+
| .. image:: /icons/bandpass-filter.jpg                     |
|    :target: experiments/filters.html                      |
|    :alt: photo                                            |
|                                                           |
| Circuits de filtrage                                      |
+-----------------------------------------------------------+

+-----------------------------------------------------------+
| .. image:: /icons/sound-frequency.jpg                     |
|    :target: experiments/sound-frequency.html              |
|    :alt: photo                                            |
|                                                           |
| Fréquence d'un son                                        |
+-----------------------------------------------------------+

+-----------------------------------------------------------+
| .. image:: /icons/piezo-freq-resp.png                     |
|    :target: experiments/piezo-buzzer.html                 |
|    :alt: photo                                            |
|                                                           |
| Buzzer piézo-électrique                                   |
+-----------------------------------------------------------+

+-----------------------------------------------------------+
| .. image:: /icons/inter-sound.png                         |
|    :target: experiments/sound-beats.html                  |
|    :alt: photo                                            |
|                                                           |
| Battements sonores                                        |
+-----------------------------------------------------------+

+-----------------------------------------------------------+
| .. image:: /icons/velocity-sound.png                      |
|    :target: experiments/sound-velocity.html               |
|    :alt: photo                                            |
|                                                           |
| Vitesse du son                                            |
+-----------------------------------------------------------+

+-----------------------------------------------------------+
| .. image:: /icons/ultrasound.jpg                          |
|    :target: experiments/ultra-sound.html                  |
|    :alt: photo                                            |
|                                                           |
| Ultra-sons                                                |
+-----------------------------------------------------------+

+-----------------------------------------------------------+
| .. image:: /icons/LM35.jpg                                |
|    :target: experiments/temp-sensors.html                 |
|    :alt: photo                                            |
|                                                           |
| Capteurs de température                                   |
+-----------------------------------------------------------+

+-----------------------------------------------------------+
| .. image:: /icons/pt100-screen.png                        |
|    :target: experiments/water-cooling.html                |
|    :alt: photo                                            |
|                                                           |
| Courbe de refroidissement                                 |
+-----------------------------------------------------------+

+-----------------------------------------------------------+
| .. image:: /icons/gravity-TOF.jpg                         |
|    :target: experiments/gravity-tof.html                  |
|    :alt: photo                                            |
|                                                           |
| Mesure de la gravité par temps de vol                     |
+-----------------------------------------------------------+

+-----------------------------------------------------------+
| .. image:: /icons/pendulum-screen.png                     |
|    :target: experiments/pendulum.html                     |
|    :alt: photo                                            |
|                                                           |
| Pendule                                                   |
+-----------------------------------------------------------+


:raw-html:`</div>`

