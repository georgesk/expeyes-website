Expeyes Junior:
###############

:slug: ejun
:lang: en
       
.. role:: raw-html(raw)
	  :format: html


.. image:: /images/ejphoto.jpg
	    :width: 300px
	    :alt: photo of expeyes junior

-  ExpEYES Junior User's Manual. Download from
   `here </Documents/eyesj-a4.pdf>`__
-  Programmer's Manual. Download from
   `here </Documents/eyesj-progman.pdf>`__
-  Introduction to ExpEYES videos on
   `you-tube <https://www.youtube.com/watch?v=ils7HBFMQ4Y>`__
   (prepared with the help of NCERT, uploaded by Shaju K Y)
-  Python Programming of ExpEYES,
   `video <https://www.youtube.com/watch?v=5P5Qu1qnYWY>`__
-  `Spoken Tutorial on ExpEYES <https://www.youtube.com/watch?v=z8iVJdpkwIY>`__
-  ExpEYES Brochure. Download from
   `here </Documents/ej-brochure.pdf>`__
	 
:raw-html:`<div class="stackedImages">`

+-----------------------------------------------------+
| .. image:: /icons/croplus.png                       |
|   :target: experiments/croplus.html                 |
|   :alt: CRO interface                               |
|                                                     |
| EYES GUI                                            |
+-----------------------------------------------------+

+-----------------------------------------------------+
| .. image:: /icons/ad-dc.png                         |
|   :target: experiments/ac-dc.html                   |
|   :alt: alternative and direct current              |
|                                                     |
| AC & DC                                             |
+-----------------------------------------------------+

+-----------------------------------------------------+
| .. image:: /icons/resistor-compare.jpg              |
|   :target: experiments/resistance.html              |
|   :alt: Measuring a resistance                      |
|                                                     |
| Resistance                                          |
+-----------------------------------------------------+

+-----------------------------------------------------+
| .. image:: /icons/water-conductivity.jpg            |
|   :target: experiments/water resistance.html        |
|   :alt: Resistance of water                         |
|                                                     |
| Resistance of water                                 |
+-----------------------------------------------------+

+-----------------------------------------------------+
| .. image:: /icons/pickup.png                        |
|   :target: experiments/pickup.html                  |
|   :alt: photo                                       |
|                                                     |
| Powerline pickup                                    |
+-----------------------------------------------------+

+-----------------------------------------------------+
| .. image:: /icons/parallelplate-cap.jpg             |
|   :target: experiments/capacitance.html             |
|   :alt: photo                                       |
|                                                     |
| Capacitance                                         |
+-----------------------------------------------------+

+-----------------------------------------------------+
| .. image:: /icons/glassplate-cap.jpg                |
|   :target: experiments/dielectric.html              |
|   :alt: photo                                       |
|                                                     |
| Dielectric constant                                 |
+-----------------------------------------------------+

+-----------------------------------------------------+
| .. image:: /icons/acdc-sep-screen.png               |
|   :target: experiments/ac-dc-separation.html        |
|   :alt: photo                                       |
|                                                     |
| AC & DC separation                                  |
+-----------------------------------------------------+

+-----------------------------------------------------+
| .. image:: /icons/ac-circuits.png                   |
|   :target: experiments/ac-circuits.html             |
|   :alt: photo                                       |
|                                                     |
| AC circuits                                         |
+-----------------------------------------------------+

+-----------------------------------------------------------+
| .. image:: /icons/RC-curves.png                           |
|    :target: experiments/rc-transient.html                 |
|    :alt: photo                                            |
|                                                           |
| RC Transient                                              |
+-----------------------------------------------------------+

+-----------------------------------------------------------+
| .. image:: /icons/RL-curves.png                           |
|    :target: experiments/rl-transient.html                 |
|    :alt: photo                                            |
|                                                           |
| RL Transient                                              |
+-----------------------------------------------------------+

+-----------------------------------------------------------+
| .. image:: /icons/RLC-curves.png                          |
|    :target: experiments/rlc-transient.html                |
|    :alt: photo                                            |
|                                                           |
| RLC Transient                                             |
+-----------------------------------------------------------+

+-----------------------------------------------------------+
| .. image:: /icons/rc-integ1khz.png                        |
|    :target: experiments/rc-integration.html               |
|    :alt: photo                                            |
|                                                           |
| RC integration                                            |
+-----------------------------------------------------------+

+-----------------------------------------------------------+
| .. image:: /icons/induction-screen.png                    |
|    :target: experiments/em-induction.html                 |
|    :alt: photo                                            |
|                                                           |
| EM Induction                                              |
+-----------------------------------------------------------+

+-----------------------------------------------------------+
| .. image:: /icons/transformer-screen.png                  |
|    :target: experiments/ac-generator.html                 |
|    :alt: photo                                            |
|                                                           |
| Transformer                                               |
+-----------------------------------------------------------+

+-----------------------------------------------------------+
| .. image:: /icons/ac-generator.jpg                        |
|    :target: experiments/ac-generator.html                 |
|    :alt: photo                                            |
|                                                           |
| AC Generator                                              |
+-----------------------------------------------------------+

+-----------------------------------------------------------+
| .. image:: /icons/singing-magnet.jpg                      |
|    :target: experiments/singing-magnet.html               |
|    :alt: photo                                            |
|                                                           |
| Singing magnet                                            |
+-----------------------------------------------------------+

+-----------------------------------------------------------+
| .. image:: /icons/driven-pendulum.jpg                     |
|    :target: experiments/driven-pendulum.html              |
|    :alt: photo                                            |
|                                                           |
| Driven Pendulum                                           |
+-----------------------------------------------------------+

+-----------------------------------------------------------+
| .. image:: /icons/blinking-led.jpg                        |
|    :target: experiments/persistance-vision.html           |
|    :alt: photo                                            |
|                                                           |
| Blinking LED                                              |
+-----------------------------------------------------------+

+-----------------------------------------------------------+
| .. image:: /icons/stroboscope.jpg                         |
|    :target: experiments/stroboscope.html                  |
|    :alt: photo                                            |
|                                                           |
| Stroboscope                                               |
+-----------------------------------------------------------+

+-----------------------------------------------------------+
| .. image:: /icons/light-barrier.jpg                       |
|    :target: experiments/light-barrier.html                |
|    :alt: photo                                            |
|                                                           |
| Light Barrier                                             |
+-----------------------------------------------------------+

+-----------------------------------------------------------+
| .. image:: /icons/opto-electric-transmission.png          |
|    :target: experiments/opto-coupler.html                 |
|    :alt: photo                                            |
|                                                           |
| Opto-Coupler                                              |
+-----------------------------------------------------------+

+-----------------------------------------------------------+
| .. image:: /icons/rodpend.jpg                             |
|    :target: experiments/gravity-pendulum.html             |
|    :alt: photo                                            |
|                                                           |
| Gravity by Pendulum                                       |
+-----------------------------------------------------------+

+-----------------------------------------------------------+
| .. image:: /icons/LDR.jpg                                 |
|    :target: experiments/ldr.html                          |
|    :alt: photo                                            |
|                                                           |
| Study LDR                                                 |
+-----------------------------------------------------------+

+-----------------------------------------------------------+
| .. image:: /icons/half-wave-filter-screen.png             |
|    :target: experiments/half-wave.html                    |
|    :alt: photo                                            |
|                                                           |
| Halfwave Rectifier                                        |
+-----------------------------------------------------------+

+-----------------------------------------------------------+
| .. image:: /icons/full-wave.png                           |
|    :target: experiments/full-wave.html                    |
|    :alt: photo                                            |
|                                                           |
| Fulwave Rectifier                                         |
+-----------------------------------------------------------+

+-----------------------------------------------------------+
| .. image:: /icons/diode-iv-screen.png                     |
|    :target: experiments/diode-iv.html                     |
|    :alt: photo                                            |
|                                                           |
| Diode IV                                                  |
+-----------------------------------------------------------+

+-----------------------------------------------------------+
| .. image:: /icons/transistor-ce.png                       |
|    :target: experiments/transistor-ce.html                |
|    :alt: photo                                            |
|                                                           |
| Transistor-CE                                             |
+-----------------------------------------------------------+

+-----------------------------------------------------------+
| .. image:: /icons/logic-gate.jpg                          |
|    :target: experiments/logic-gates.html                  |
|    :alt: photo                                            |
|                                                           |
| Logic gate                                                |
+-----------------------------------------------------------+

+-----------------------------------------------------------+
| .. image:: /icons/ic555-screen.png                        |
|    :target: experiments/555-oscillator.html               |
|    :alt: photo                                            |
|                                                           |
| 555 oscillator                                            |
+-----------------------------------------------------------+

+-----------------------------------------------------------+
| .. image:: /icons/mono555-screen.png                      |
|    :target: experiments/555-monoshot.html                 |
|    :alt: photo                                            |
|                                                           |
| 555 monoshot                                              |
+-----------------------------------------------------------+

+-----------------------------------------------------------+
| .. image:: /icons/clock-divider.png                       |
|    :target: experiments/clock-divider.html                |
|    :alt: photo                                            |
|                                                           |
| Clock divider                                             |
+-----------------------------------------------------------+

+-----------------------------------------------------------+
| .. image:: /icons/am-screen.png                           |
|    :target: experiments/am-fm.html                        |
|    :alt: photo                                            |
|                                                           |
| AM and FM                                                 |
+-----------------------------------------------------------+

+-----------------------------------------------------------+
| .. image:: /icons/bandpass-filter.jpg                     |
|    :target: experiments/filters.html                      |
|    :alt: photo                                            |
|                                                           |
| Filter Circuits                                           |
+-----------------------------------------------------------+

+-----------------------------------------------------------+
| .. image:: /icons/sound-frequency.jpg                     |
|    :target: experiments/sound-frequency.html              |
|    :alt: photo                                            |
|                                                           |
| Sound Frequency                                           |
+-----------------------------------------------------------+

+-----------------------------------------------------------+
| .. image:: /icons/piezo-freq-resp.png                     |
|    :target: experiments/piezo-buzzer.html                 |
|    :alt: photo                                            |
|                                                           |
| Piezo Buzzer                                              |
+-----------------------------------------------------------+

+-----------------------------------------------------------+
| .. image:: /icons/inter-sound.png                         |
|    :target: experiments/sound-beats.html                  |
|    :alt: photo                                            |
|                                                           |
| Sound Beats                                               |
+-----------------------------------------------------------+

+-----------------------------------------------------------+
| .. image:: /icons/velocity-sound.png                      |
|    :target: experiments/sound-velocity.html               |
|    :alt: photo                                            |
|                                                           |
| Sound Velocity                                            |
+-----------------------------------------------------------+

+-----------------------------------------------------------+
| .. image:: /icons/ultrasound.jpg                          |
|    :target: experiments/ultra-sound.html                  |
|    :alt: photo                                            |
|                                                           |
| Ultra-sound                                               |
+-----------------------------------------------------------+

+-----------------------------------------------------------+
| .. image:: /icons/LM35.jpg                                |
|    :target: experiments/temp-sensors.html                 |
|    :alt: photo                                            |
|                                                           |
| Temperature Sensors                                       |
+-----------------------------------------------------------+

+-----------------------------------------------------------+
| .. image:: /icons/pt100-screen.png                        |
|    :target: experiments/water-cooling.html                |
|    :alt: photo                                            |
|                                                           |
| Cooling Curve                                             |
+-----------------------------------------------------------+

+-----------------------------------------------------------+
| .. image:: /icons/gravity-TOF.jpg                         |
|    :target: experiments/gravity-tof.html                  |
|    :alt: photo                                            |
|                                                           |
| gravity by TOF                                            |
+-----------------------------------------------------------+

+-----------------------------------------------------------+
| .. image:: /icons/pendulum-screen.png                     |
|    :target: experiments/pendulum.html                     |
|    :alt: photo                                            |
|                                                           |
| Pendulum                                                  |
+-----------------------------------------------------------+


:raw-html:`</div>`

