La conception d'ExpEYES
#######################
:slug: design
:lang: fr
:title: La conception d'ExpEYES
       

La figure suivante montre un diagramme en bloc simplifié d'ExpEYES Junior.
Les éléments capteurs et de contrôle sont connectés au micro-contrôleur. Les
programmes utilisateurs passent par la bibliothèque Python pour communiquer
avec le micro-contrôleur, qui fonctionner les programmes, écrits en C
et en assembleur. Le micro-contrôleur se charge des opérations de mesure
et de contrôle et renvoie les résultats au PC. Le travail est partagé pour
bénéficier des capacités et des points forts de chaque appareil. Le
micro-contrôleur fait touts les mesures en temps réel et passe le résultat
au PC où le code Python est utilisé pour retravailler et afficher les données.

|image0|

-  `La bibliothèque Python </Code/eyesj.py>`__, du côté du PC
-  `le code C </Code/main.c>`__ et `le code Assembleur </Code/asm.s>`__, du côté du micro-contrôleur
-  `Schéma du Circuit </images/eyes.pdf>`__

ExpEYES-17
~~~~~~~~~~

Schéma du circuit et implantation :  `document PDF </Documents/proto1.pdf>`__
`schéma 1 </Documents/proto1.sch>`__
`schéma 2 </Documents/IO.sch>`__ 
`implantation </Documents/proto1.kicad_pcb>`__

`liste des composants <partList.html>`__

La version ExpEYES-17 est dérivée de la `conception sous licence libre
<https://github.com/jithinbp/seelab-jr>`__ publiée par `Jithin B
P <http://jithinbp.in/>`__. La version modifiée est
`ICI <https://github.com/expeyes/expeyes-programs/tree/master/ExpEYES17/Firmware/EJV2_15DEC>`__.

.. |image0| image:: /images/eyesjun-block.png

