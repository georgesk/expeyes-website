Design of ExpEYES
#################
:slug: design
:lang: en
:title: Design of ExpEYES
       

A simplified block diagram of ExpEYES Junior is shown in the figure.
The sensor/control elements are connected to the micro-controller. The
user programs uses the Python Library to communicate to the
micro-controller, running a program, written in C and Assembler. The
micro-controller performs the control/measurement operations and sends
the result back to the PC. The job is divided according to the
capabilities and strong points of each device. The micro-controller
does all the real time measurements and pass on the results to the PC
where Python code is used for processing and displaying the data.

|image0|

-  `Python Library </Code/eyesj.py>`__, on the PC side
-  `C Code </Code/main.c>`__ and  `Assembler Code </Code/asm.s>`__, on the
   micro-controller
-  `Circuit Schematic </images/eyes.pdf>`__

ExpEYES-17
~~~~~~~~~~

Circuit Schematic & Layout  `PDF </Documents/proto1.pdf>`__
`schematic1 </Documents/proto1.sch>`__
`schematic2 </Documents/IO.sch>`__ 
`layout </Documents/proto1.kicad_pcb>`__

`parts list <partList.html>`__

The ExpEYES-17 version has been derived from the `open source
design <https://github.com/jithinbp/seelab-jr>`__ released by `Jithin B
P <http://jithinbp.in/>`__. The modified version is
`HERE <https://github.com/expeyes/expeyes-programs/tree/master/ExpEYES17/Firmware/EJV2_15DEC>`__.

.. |image0| image:: /images/eyesjun-block.png

