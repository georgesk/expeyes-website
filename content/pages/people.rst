People Involved
###############
:slug: people
:date: 2018-09-07
:lang: en
:title: People Involved


The PHOENIX project started in 2005 and many people, both from
`IUAC <http://www.iuac.res.in/indexhighres.html>`__ and outside, has
contributed at various stages. Only those who have worked in developing
equipment are mentioned below. Equally important are the numerous
members of the academic community who took interest in it and worked for
spreading it, by conducting programs and also introducing it to the
syllabus at some places. 

--------------

-  `Pramode C E <http://pramode.net/>`__, IC Software, Trissur, Kerala

Introduced the idea of using `Python language for
Phoenix <http://linuxgazette.net/111/pramode.html>`__ , something that
had a tremendous impact on the project. Suggests new ideas and solutions
and also works for spreading awareness about the project.

-  `Georges
   Khaznadar <https://usb.freeduc.org>`__,
   Teacher of Physics at `lycée Jean Bart, Dunkerque,
   France <https://www.lyceejeanbart.fr/>`__ (georgesk \_at\_
   debian.org)

Due to his efforts expEYES software is now part of Debian GNU/Linux and
it's derivatives like Ubuntu. He did the localisation of software and
documentation, French versions are already available, Created a logo.
Introduced PHOENIX in Europe, first using in his school, then took to
others.
Manages the `expEYES repository <https://github.com/expeyes>`__ on
github. Suggested a lot of creative ideas for the project.

-  Praveen Patil, Department of Physics,G S S College, Tilakwadi,
   Belgaum 590006

Created and maintaining an `expEYES blog
site <http://expeyes.wordpress.com/>`__. Actively involved in conducting
training programs. Working on a new Live CD.

-  `Jithin B P <http://jithinbp.in/>`__\ , Department of Physical and
   Material Sciences, Central University of Himachal Pradesh.

ExpEYES17 is derived from `Seelab
Junior <https://github.com/jithinbp/seelab-jr>`__, the open source
device developed by Jithin. Also working on lab equipment for teaching
nuclear physics.


**People from IUAC**

-  Ajith Kumar B.P. , RF & Electronics Laboratory, IUAC

Initiated the PHOENIX project by designing a parallel port based
interface and organized a training program for teachers in 2005, and
continuing with it. Does designing, developing experiments and writing
software and documentaion for all the equipment from this project. Spend
some amount of time in conducting training programs.

-  V V V Satyanarayana, Data Support laboratory, IUAC

Actively involved in this project from the beginning, providing support
in hardware fabrication and testing, trouble shooting and , if required,
modifying the analog circuits involved. Did the analog circuits for the
alpha spectrometer. Also involved in the activities of the teaching Lab
maintenance.

Other people from IUAC who has contributed to this project are:  Jimson
Sacharias, Kundan Singh, Parmanand Singh, Deepak Munda and S
Venkataramanan.

