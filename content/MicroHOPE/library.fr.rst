La bibliothèque de Microhope
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

:slug: MicroHOPE/library
:date: 2018-09-07
:title: Microhope Library
:lang: fr
       
MicroHOPE n'a pas de bibliothèques compilées, on a mis à disposition des
fichier à inclure pour réaliser diverses tâches comme écrire sur l'écran
LCD, commander les compteurs de temps, etc. Tous les fichiers à inclure
commencent avec :code:`mh-` . Ces fichiers sont destinés aux débutants
pour commencer à utiliser les possibilités du micro-contrôleur. Les utilisateurs
sont encouragés à examiner les fichiers C et comprendre comment ils
fonctionnent. On en attend qu'ils aillent au-delà et configurent les
périphériques en manipulant directement les fonctions spéciales des registres.
Configurer directement les périphériques suppose de se référer au
`data book <http://www.atmel.com/images/doc2503.pdf>`__  de l'ATMega32,
cependant cela augmente grandement la flexibilité de ce que l'on peut utiliser.

Voici la liste des fichiers à inclure :

| `mh-adc.c <microhope/mh-adc.c>`__
  `mh-lcd.c <microhope/mh-lcd.c>`__
  `mh-timer2.c <microhope/mh-timer2.c>`__
  `mh-uart.c <microhope/mh-uart.c>`__
| `mh-digital.c <microhope/mh-digital.c>`__
  `mh-timer.c <microhope/mh-timer.c>`__
  `mh-utils.c <microhope/mh-utils.c>`__

Le répertoire d'exemples :code:`microhope/` sous forme de
`fichier zip <microhope.zip>`__
