MicroHOPE Software
~~~~~~~~~~~~~~~~~~

:slug: MicroHOPE/mh-software
:date: 2018-09-07
:title: MicroHOPE Software
:lang: en
       
MicroHOPE uses avrgcc to compile the C code and avrdude to upload it to
the micro-controller, available on both GNU/Linux and MSWindows
platforms. A minimal text editor, that can invoke the compile and upload
programs is our IDE.

For Debian/Ubuntu systems:
^^^^^^^^^^^^^^^^^^^^^^^^^^

Already part of the software distribution. To install,

:code:`$ sudo apt-get install microhope`

| To copy the examples to a directory named 'microhope' inside your home
  directory run:
| :code:`$ create-microhope-env`

For other GNU/Linux Systems:
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Download microhope.zip

$unzip microhope.zip

:code:`$cd microhope`

$sudo python mh-ide.py      (To set non-root permissions,  $sudo sh
`postinst </MicroHOPE/postinst>`__ )

For MSWindows, visit http://www.expeyes.herobo.com/microhope.php
