Enregistrement et restitution audio
###################################

:slug: MicroHOPE/audio
:date: 2018-09-07
:title: Enregistrement et restitution audio
:lang: fr
       
.. raw:: html

	 <div style="float: right; margin-left: 1em;">

|image0|

.. raw:: html

	 </div>

On peut programmer Microhope pour fonctionner comme une carte audio
USB. Les programmes ci-dessous montrent comment on peut envoyer un fichier
WAV vers Microhope et le jouer à l'aide de la carte-fille convertisseur
numérique-analogique R-2R (la photo montrer un échelle R-2R faite à la main),
et aussi comment un signal audio peut être échantillonné par Microhope et
envoyé vers le PC pour y être enregistré sous forme de fichier WAV. On utilise
un boîtier ExpEYES pour visualiser le signal généré par le
`convertisseur R-2R <plugins.html>`__

Restitution audio
~~~~~~~~~~~~~~~~~

Programme C - audio_streaming.c
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: C
   :linenos:

   #include <avr/io.h>
   #include <avr/interrupt.h>

   int main()
   {
       DDRB = 0xff;
       UCSRB = (1<<RXEN) | (1<<TXEN) | (1<<RXCIE);
       UBRRH = 0; //set baud rate of 500000
       UBRRL = 0;        
       UCSRC = (1<<URSEL) | (1<<UCSZ1) | (1<<UCSZ0);   
       sei();
       for(;;)
       {
       }        
   }

   ISR(USART_RXC_vect)


   {
       PORTB = UDR;
   }

Un programme Python du côté du PC sert à lire le fichier WAV (avec des
échantillons à 8 bits et une vitesse d'échantillonnage de 48000) et envoyer
les échantillons :

audio_streaming.py
^^^^^^^^^^^^^^^^^^

.. code-block:: python
   :linenos:

   import wave,serial
   s=serial.Serial('/dev/ttyUSB1',500000)
   f=wave.open('out2.wav','r')
   x=f.readframes(f.getnframes())
   s.write(x)


Enregistrement audio
~~~~~~~~~~~~~~~~~~~~

Le signal de sortie d'un microphone à condensateur, après amplification
par 100 et déplacement du signal pour le rendre unipolaire (les circuits
ne sont pas présentés ici), est envoyé vers la borne d'entrée analogique PA0.
Le signal est échantillonné à l'aide du convertisseur analogique-numérique
intégré à l'Atmega32, et envoyé vers le PC, où un programme en Python
enregistre les échantillons sous forme d'un fichier WAV.

record.c
^^^^^^^^

.. code-block:: C
   :linenos:

   #include <avr/io.h>
   #include <avr/interrupt.h>

   uint8_t data;
   int main()
   {
       DDRB = 0xff;
      
       // configuration du convertisseur analogique-numérique
       ADCSRA = (1 << ADEN)  | (1<<ADSC) | 4;  //vitesse d'échantillonnage - 38 kSps
       ADMUX = (1 << REFS0) | (1 << ADLAR);  
       while ( !(ADCSRA & (1<<ADIF)) ) ;       // attend une conversion
       ADCSRA |= ADIF;
      
       //configure UART
       UCSRB = (1<<RXEN) | (1<<TXEN);
       UCSRC = (1<<URSEL) | (1<<UCSZ1) | (1<<UCSZ0);
       UBRRH = 0;
       UBRRL = 0;
       
       for(;;)
       {
           ADCSRA = (1 << ADEN)  | (1<<ADSC) | 4 ;
           while ( !(ADCSRA & (1<<ADIF)) ) ;
           data = ADCH;    // lit un échantillon de signal analogique
           ADCSRA |= ADIF;
           PORTB = data;
           while ( !(UCSRA & (1<<UDRE)) );
           UDR = data;    // envoie l'échantillon vers le PC via l'UART
       }
   }

record.py
^^^^^^^^^

.. code-block:: python
   :linenos:

   import serial, wave
   s = serial.Serial('/dev/ttyUSB1',500000)
   w = wave.open('record.wav','w')

   data = s.read(1000000)   # lecture d'un million d'échantillons

   w.setnchannels(1)        # audio monophonique
   w.setsampwidth(1)        # échantillons de 8 bits
   w.setframerate(33500)    # vitesse d'échantillonnage, trouvée empriquement
   w.writeframesraw(x)  
   w.close() 

Contribué par Kishore T 

.. |image0| image:: images/R2Rsound1.jpg
   :width: 359px
