Plugin Modules
##############

:slug: MicroHOPE/plugins
:date: 2018-09-07
:title: Plugins
:lang: en

Several plug-in modules are available for microhope.

Digital Output Board:
~~~~~~~~~~~~~~~~~~~~~

This board has 8 LEDS with series resistors. It can be plugged into
any of the four ports, the ground pin should be matched with
that of the socket.  Mainly used for displaying the
results of assembly language programs and code debugging.

.. image:: images/led8.jpg
	   :alt: Digital Output Board

.. image:: images/led8-schematic.png
	   :width: 588px
	   :alt: Digital Output Board schematics

Digital Input Board:
~~~~~~~~~~~~~~~~~~~~

This board has 8 switches with series resistors, one end connected to
ground. Can be used for feeding 8 bit input data to programs.

.. image:: images/switch8-schematic.png
	   :width: 411px
	   :alt: Digital Input Board schematics

R-2R DAC Board:
~~~~~~~~~~~~~~~

An 8 bit Digital to Analog Converter (DAC) is made using an R-2R network as
shown in the schematic below. This can be used for generating arbitrary
shaped voltage waveforms under software control.

.. image:: images/r2rdac-schematic.png
	   :width: 629px
	   :alt: R-2R DAC Board


Alphanumeric LCD board:
~~~~~~~~~~~~~~~~~~~~~~~

For some applications, it is necessary to have a local display. The HD44780
controller, or compatible IC, based LCD displays are widely available. They
come with a 16 pin connector and the transfer protocol is well documented
For details refer to . The connections between microHOPE and the LCD display
are shown in figure. Pins 4, 5 and 7 of the LCD display are control lines,
connected to PC1, PC2 and PC4. The ASCII codes are transferred in the 4bit
mode, using pins 11 to 14 connected to PC4, PC5, PC6 and PC7. The LCD should
be connected to port C socket, to use the C library functions to access the
display.

.. image:: images/lcd-display.jpg
	   :width: 485px
	   :alt: Alphanumeric LCD board

Motor Control Board:
~~~~~~~~~~~~~~~~~~~~

The motor control board consists of 2 H-bridges (IC L293D). Board can be
powered from outside or from the MicroHOPE socket. An INT/EXT jumper is
provided to select the power option. The voltage level at the for outputs of
L293 is decided by the four LSBs of the port on which it is connected. The
outputs (A, B, C and D) can be used for controlling 2 DC motors or one
stepper motor.

.. image:: images/mh-hbridge.jpg
	   :width: 311px
	   :alt: Motor Control Board

.. image:: images/H-bridge.png
	   :width: 561px
	   :alt: H-Motor Control Board


USBASP Programmer board:
~~~~~~~~~~~~~~~~~~~~~~~~

    This is an open sourced ISP (In-System Programming)
programmer available from http://www.fischl.de/usbasp/ .
This is provided as an accessory to MicroHOPE due to several reasons. If
you want to develop programs that uses the UART of Atmega32, you need to
upload code using ISP. It can be used for programming other AVR
micro-controllers also. It can be used for burning the boot loader. The LED
on the board indicates power. It goes off while uploading code, giving an
additional indication.

.. image:: images/mh-usbasp.jpg
	   :width: 685px
	   :alt: Usbasp Board

.. image:: images/usbasp-schematic.png
	   :width: 679px
	   :alt: Usbasp Board schematics

