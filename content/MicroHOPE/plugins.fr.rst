Les cartes-filles modulaires
############################

:slug: MicroHOPE/plugins
:date: 2018-09-07
:title: Plugins
:lang: fr

Plusieurs modules de cartes-filles sont disponibles pour MicroHOPE.

Carte de sorties numériques :
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cette carte a 8 DELs avec des résistances en série. On peut la brancher
à n'importe lequel des quatre ports, il faut mettre la borne de masse à la bonne
place par rapport à la prise. On l'utilise principalement pour afficher les
résultats de programmes en assembleur et pour déboguer.

.. image:: images/led8.jpg
	   :alt: Carte de sorties numériques

.. image:: images/led8-schematic.png
	   :width: 588px
	   :alt: Carte de sorties numériques, schéma

Carte d'entrées numériques :
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cette carte a huit interrupteurs en série avec des résistances, une borne
connectée à la masse. On peut l'utiliser pour entrer des données 8 bits
aux programmes.

.. image:: images/switch8-schematic.png
	   :width: 411px
	   :alt: Carte d'entrées numériques, schéma

Carte convertisseur numérique-analogique R-2R :
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Un convertisseur numérique-analogique R-2R à 8 bits (CAN) est réalisé à
l'aide d'un réseau R-2R somme le montre schéma ci-dessous. On peut l'utiliser
pour générer des signaux de tension de forme arbitraire sous contrôle
logiciel.

.. image:: images/r2rdac-schematic.png
	   :width: 629px
	   :alt: convertisseur numérique-analogique R-2R


Carte afficheur alphanumérique LCD :
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Pour certaines applications, il faut avoir un affichage local. Des afficheurs
LCD basés sur le contrôleur HD44780, ou un circuit intégré compatible, sont
faciles à approvisionner. Ils viennent avec un connecteur à 16 bornes et le
protocole de transfert est bien documenté. Pour les détails, se référer à .
La connexion entre MicroHOPE et l'afficheur LCD est montrée dans la figure.
Les bornes 4, 5 et 7 de l'afficheur LCD sont des lignes de contrôle,
connectées à PC1, PC2 et PC4. Les codes ASCII sont transférés en mode 4 bits,
à l'aide des bornes 11 à 14 connectées à PC4, PC5, PC6 et PC7. L'afficheur
LCD est censé être connecté à la prise du port C, pour utiliser les fonctions
de la bibliothèque C qui accèdent à l'affichage.

.. image:: images/lcd-display.jpg
	   :width: 485px
	   :alt: afficheur alphanumérique LCD

Carte de contrôle de moteur :
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La carte de contrôle de moteur consiste en deux ponts en H (IC L293D).
La carte peut être alimentée de l'extérieur ou par la prise de MicroHOPE.
Un cavalier INT/EXT est mis à disposition pour sélectionner l'option
d'alimentation. Le niveau de tension aux quatre sorties du L293 est commandé
par les quatre bits de poids faible du port où celui-ci est connecté.
Les sorties (A, B, C et D) peuvent être utilisées pour contrôler deux
moteurs à courant continu ou un moteur pas à pas.

.. image:: images/mh-hbridge.jpg
	   :width: 311px
	   :alt: Carte de contrôle de moteur

.. image:: images/H-bridge.png
	   :width: 561px
	   :alt: Carte de contrôle de moteur (H)


Carte de programmation USBASP :
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    Il s'agit d'un programmateur *ISP* (*In-System Programming*)
sous licence libre disponible à http://www.fischl.de/usbasp/ .
On le fournit comme accessoire pour MicroHOPE, pour plusieurs raisons. Si on
veut développer des programmes qui utilisent la sortie UART de l'Atmega32,
il faut y charger le code avec un *ISP*. On peut aussi l'utiliser pour
programmer d'autres micro-contrôleurs AVR. On peut l'utiliser pour mettre
en place le programme de chargement de démarrage. La DEL sur la carte indique
la présence d'alimentation. Elle s'éteint pendant qu'on charge du code, ce
qui donne une information supplémentaire.

.. image:: images/mh-usbasp.jpg
	   :width: 685px
	   :alt: Carte de programmation USBASP

.. image:: images/usbasp-schematic.png
	   :width: 679px
	   :alt: Carte de programmation USBASP, schéma

