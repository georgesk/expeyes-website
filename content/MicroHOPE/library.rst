The Microhope Library
~~~~~~~~~~~~~~~~~~~~~

:slug: MicroHOPE/library
:date: 2018-09-07
:title: Microhope Library
:lang: en
       
MicroHOPE does not have compiled libraries, we have provided include
files to perform various tasks like writing to the LCD display, setting
the timers etc. All the include files start with mh- . These files are
meant for beginners to get started with using the features of the
micro-controller. The users are encouraged to look at the C files and
understand how the functions work. They are expected to grow beyond that
and configure the peripherals by manipulating the special function
registers directly. Directly configuring the peripherals would require
referring to the ATMega32 `data
book <http://www.atmel.com/images/doc2503.pdf>`__, but would greatly
increase the flexibility with which they can be used.

The include files are listed below:

| `mh-adc.c </MicroHOPE/microhope/mh-adc.c>`__
  `mh-lcd.c </MicroHOPE/microhope/mh-lcd.c>`__
  `mh-timer2.c </MicroHOPE/microhope/mh-timer2.c>`__
  `mh-uart.c </MicroHOPE/microhope/mh-uart.c>`__
| `mh-digital.c </MicroHOPE/microhope/mh-digital.c>`__
  `mh-timer.c </MicroHOPE/microhope/mh-timer.c>`__
  `mh-utils.c </MicroHOPE/microhope/mh-utils.c>`__

Examples Directory :code:`microhope/` as
`zip file </MicroHOPE/microhope.zip>`__
