Le matériel de MicroHOPE
~~~~~~~~~~~~~~~~~~~~~~~~

:slug: MicroHOPE/mh-hardware
:date: 2018-09-07
:title: MicroHOPE Hardware
:lang: fr
	
Le diagramme de bloc du matériel de MicroHOPE est montré ci-dessous.
L'ATmega32 a trente deux bornes d'entrée/sortie à usage général, organisées
en ports de 8 bits de large A, B, C et D sur le boîtier DIP à 40 broches.
À l'exception de deux bits du port D (connectés à un convertisseur USB/série)
toutes les autres trente bornes sont disponibles pour l'utilisateur. Le
programme de chargement de démarrage (*boot loader*) est pré-chargé dans la
mémoire flash de l'ATmega32. Pour y charger un nouveau programme, le PC
redémarre l'ATmega32 en envoyant une impulsion à travers la sortie RTS de
la puce (MCP2200 pour la nouvelle version) du convertisseur USB/série. Le code
du chargeur de démarrage (*boot loader*) reprend la main après chaque
redémarrage, et commence par vérifier si du nouveau code arrive *via* le câble
USB. S'il y en a, il est chargé dans la mémoire flash et on lui passe le
contrôle. S'il n'y a pas de nouveau code, le contrôle est passé au code déjà
présent. Même si l'ATmega32 est capable de fonctionner à 16 MHz, un cristal de
8 MHz est utilisé afin de réduire la consommation électrique et la chaleur. Le
schéma du circuit est `ICI <images/schematic.png>`__.

+----------+----------+
| |image0| | |image1| |
+----------+----------+

Toutes les connexions externes au micro-contrôleur sont faites à travers les
quatre connecteurs externes. En plus des bornes des ports d'entrée-sortie,
l'alimentation 5 V et la masse venant du câble USB sont disponibles sur
chaque connecteur.

| Plusieurs cartes-filles sont aussi disponibles.

.. |image0| image:: /MicroHOPE/images/mh-block.png
.. |image1| image:: /MicroHOPE/images/microhope-photo-horiz.jpg

