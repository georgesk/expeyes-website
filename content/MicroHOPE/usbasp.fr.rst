Programmateur USBASP
~~~~~~~~~~~~~~~~~~~~

:slug: MicroHOPE/usbasp
:date: 2018-09-07
:title: USBASP Programmer
:lang: fr
       

.. raw:: html

	 <div style="float: right; margin-left: 1em;">
.. image:: /MicroHOPE/images/usbasp-plugin.jpg
   :width: 400px
   :alt: carte-fille UsbAsp
.. raw:: html

	 </div>

La plupart des micro-contrôleurs ont la fonctionnalité *In-System Programming*,
*ISP*, implémentée à l'aide de trois bornes, *Serial ClocK* (*SCK*),
*Master-In–Slave-Out* (*MISO*) et *Master-Out–Slave-In* (*MOSI*).
Tous les types de mémoires du micro-contrôleur peuvent être touchés à
l'aide des bornes *SCK*, *MISO* et *MOSI*, pendant qu'on maintient la
borne *RESET* à l'état BAS. Ces bornes, en comptant aussi la masse, sont
accessibles sur la prise à 5 broches J7 sur la carte MicroHOPE.

USBASP est un programmateur *ISP* (*In-System Programming*) sous licence
libre disponible à http://www.fischl.de/usbasp/\ . On le fournit comme
accessoire pour MicroHOPE, pour plusieurs raisons. Si on
veut développer des programmes qui utilisent la sortie UART de l'Atmega32,
il faut y charger le code avec un *ISP*. On peut aussi l'utiliser pour
programmer d'autres micro-contrôleurs AVR. On peut l'utiliser pour mettre
en place le programme de chargement de démarrage. La DEL sur la carte indique
la présence d'alimentation. Elle s'éteint pendant qu'on charge du code, ce
qui donne une information supplémentaire.

La figure montre comment on connecte l'*ISP* à MicroHOPE.

**MicroHOPE est alimenté par la carte USBASP board, à l'aide d'un câblage externe**.

Chargement via l'interface *ISP* à l'aide d'USBASP
--------------------------------------------------

Pour charger :code:`blink.hex`, lancer la commande

:code:`$ avrdude -B10 -c usbasp -patmega32 -U flash:w:blink.hex`

ou utiliser le fichier de script
`mh-upload-usbasp.sh <microhope/mh-upload-usbasp.sh>`__ ainsi :

:code:`$ ./mh-upload-usbasp.sh  blink`

Écriture des fusibles et du chargeur de démmarage pour préparer l'ATmega32 à son utilisation dans MicroHOPE
-----------------------------------------------------------------------------------------------------------

On a besoin du fichier `ATmegaBOOT_168_atmega32.hex <ATmegaBOOT_168_atmega32.hex>`__

.. code-block:: shell
   :linenos:
      
   avrdude -B10 -c usbasp -patmega32 -U flash:w:ATmegaBOOT_168_atmega32.hex
   # charge le fichier hexa
  
   avrdude -B10 -c usbasp -patmega32 -U lfuse:w:0xff:m -U hfuse:w:0xda:m
   # met les fusibles à 0xFF et 0xDA
  
   avrdude -b 19200 -P /dev/ttyACM0 -pm32 -c stk500v1 -n
   # vérifie le périphérique sur /dev/ttyACM0

Il est plus facile d'utiliser le script
script `mhbootload32-usbasp.sh <microhope/mhbootload32-usbasp.sh>`__ qui
contient ces commandes.

--------------

.. raw:: html

	 <div style="float: right; margin-left: 1em;">

|image0|

.. raw:: html

	 </div>

On peut programmer les circuits de la famille AVR à l'aide du programmeur
USBASP, en les connectant comme montré sur la figure. Il y a 6 connexions
externes à la carte. La masse et l'alimentation 5 V sont connectées en
insérant deux fils. Les quatre connexions restantes sont faites à l'aide de
quatre bornes carrées de 1mm à la plaque de contacts d'un côté, et àa la prise
de la carte USBASP de l'autre côté.

On utilise la commande

:code:`$ avrdude -B10 -c usbasp -patmega32 -U flash:w:blink.hex`

pour charger le code, ce qui fait clignoter la DEL branchée à PB0.

.. |image0| image:: images/usbasp-on-BB.jpg
   :width: 565px

