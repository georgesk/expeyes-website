Récepteur infrarouge
~~~~~~~~~~~~~~~~~~~~

:slug: MicroHOPE/ir-receiver
:date: 2018-09-07
:title: Infrared Receiver
:lang: fr
       

Les signaux infrarouges émis par une télécommande de téléviseur, etc. peuvent
être capturés en utilisant le récepteur IR TSOP1738. Le programme
:code:`ir-recv.c` utilise un sous-programme de service d'interruption pour
décoder ce signal.La sortie de la puce est connectée au bit 2 de PORTD,
comme le montre la figure ci-dessous. Appuyer sur quelques boutons de la
télécommande. Le nombre reçu s'affichera sur l'écran LCD de MicroHOPE. On peut
mofifier le code :code:`ir-recv.c` afin de travailler avec le signal IR à un
octet émis à partir d'ExpEYES. L'octet reçu sera affiché sur l'écran LCD.
Télécharger ir-recv.c
      

+----------+----------+
| |image0| | |image1| |
+----------+----------+

.. |image0| image:: images/ir-receiver.png
.. |image1| image:: images/ir-receive-proto.jpg
   :width: 428px
