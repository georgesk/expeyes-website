Le logiciel de MicroHOPE
~~~~~~~~~~~~~~~~~~~~~~~~

:slug: MicroHOPE/mh-software
:date: 2018-09-07
:title: MicroHOPE Software
:lang: fr
       
MicroHOPE utilise :code:`avrgcc` pour compiler le code C et :code:`avrdude`
pour le charger dans le micro-contrôleur, qui sont disponibles sous
GNU/Linux et MSWindows. Un éditeur de texte minimal, qui peut lancer la
compilation et le chargement est l'interface utilisateur.

Pour les systèmes Debian/Ubuntu :
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Ils font déjà partie de la distrinution. Pour installer, ...

:code:`$ sudo apt-get install microhope`

| Pour copier les exemples dans un répertoire nommé :code:`microhope` dans son
  répertoire personnel, lancer :
| :code:`$ create-microhope-env`

Pour les autres systèmes GNU/Linux :
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Télécharger microhope.zip

:code:`$ unzip microhope.zip`

:code:`$ cd microhope`

:code:`$sudo python mh-ide.py`       (Pour donner des permissions aux utilisateurs ordinaires,  `$ sudo sh postinst </MicroHOPE/postinst>`__ )

Pour MSWindows, veuillez visiter http://www.expeyes.herobo.com/microhope.php
