"""
This is a helper to translate http://expeyes.in/ejun.html to 
RST format
"""

from bs4 import BeautifulSoup
import re

LONG=60

def encadre (contenu, bord="|", espace=" ", longueur=LONG):
    return bord+espace+contenu+espace*(longueur-2-len(contenu))+bord

def ligne (longueur=LONG):
    return encadre("", bord="+", espace="-", longueur=longueur)

file=open("mirror/expeyes.in/ejun.html")
soup = BeautifulSoup(file.read(), 'html.parser')


for div in soup.find_all("div", class_="item"):
    href=div.find("a")["href"]
    img=div.find("img")["src"]
    caption=div.find("span", class_="caption").text.strip()
    caption=re.sub(r'[\n ]+', " ",caption)
    print()
    print(ligne())
    print(encadre(".. image:: /"+img))
    print(encadre("   :target: "+href))
    print(encadre("   :alt: photo"))
    print(encadre(""))
    print(encadre(caption))
    print(ligne())
    
