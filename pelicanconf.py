#!/usr/bin/env python3
# -*- coding: utf-8 -*- #

AUTHOR = 'Ajith Kumar'
SITENAME = 'Expeyes'
SITEURL = ''

PATH = 'content'

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None
FEED_RSS="feeds/rss.xml"


# Blogroll
LINKS = (
    ("Framasoft","https://framasoft.org/"),
    ("La Quadrature du Net","https://www.laquadrature.net/fr/"),
)

# Social widget
SOCIAL = (('Mastodon', 'https://mastodon.social/about'),
          ('Diaspora-Fr', 'https://diaspora-fr.org/'),)

DEFAULT_PAGINATION = 10

THEME = 'themes/expeyesdev-random2'

PAGE_PATHS = ['pages','images/favicon.png']
STATIC_PATHS = ['images','icons','Documents','Expt17','Code', 'MicroHOPE']
EXTRA_PATH_METADATA = {
    'images/favicon.png': {'path': 'favicon.png'}
}

SLUGIFY_SOURCE = 'title'

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

#### PLUGINS
PLUGIN_PATHS = ['plugins']
PLUGINS = ['i18n_subsites',]

#### I18N
main_lang = "en"
main_site_url = ""

I18N_SUBSITES = {
    'fr': {
        'SITENAME': 'ExpEYES',
        'THEME': 'themes/expeyesdev-random2',
        'STATIC_PATHS': ['images','icons','Documents','Expt17','Code', 'MicroHOPE'],
    },
}

JINJA_ENVIRONMENT = {
    'extensions': ['jinja2.ext.i18n'],
}

I18N_GETTEXT_LOCALEDIR = THEME+'/lang'
I18N_GETTEXT_DOMAIN = 'expeyes'
