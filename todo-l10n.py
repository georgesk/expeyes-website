#!/usr/bin/python3

"""
Generate a todo-list for localization of the contents for the website
"""
import os, sys, re
from datetime import datetime

def isSourceFile(path, f):
    """
    detection of source files
    @param path path to the file
    @param f filename
    @return a boolean, True when the file can be translated
    """
    # consider only ReSTructured Text,
    # disregard files finishing like foo.fr.rst
    if f[-4:] == ".rst" and f[-7]!=".":
        ## disregard files with a tag "no-translation"
        with open(os.path.join(path, f), encoding="utf-8") as infile:
            found = re.search(r"^:tags: .*no-translation.*$", infile.read(), re.M)
        return not found
    

def updateTodo(missing, lagging, path, f, lang):
    """
    says what is to do for i18n
    @param missing list or source files which miss a translation for language lang
    @param lagging list of source files whose translation in language lang is not up to date
    @param path the path of the source file
    @param f the name of the source file
    @param lang the language
    """
    if isSourceFile(path,f):
        # consider only ReSTructured Text,
        # disregard files finishing like foo.fr.rst
        source=os.path.join(path, f)
        target=os.path.join(path, f[:-4]+"."+lang+".rst")
        if os.path.exists(target):
            if os.path.getmtime(target) < os.path.getmtime(source) or \
               open(target).read().startswith(":tags: not-translated"):
                lagging.append(source)
        else:
            missing.append(source)
    return

def reportTodo(missing, lagging, lang):
    """
    generates a report about missing and not up-to-date translations
    @param missing list or source files which miss a translation for language lang
    @param lagging list of source files whose translation in language lang is not up to date
    @param lang the language
    @return a string
    """
    title="REPORT FOR LANGUAGE "+lang.upper()
    line1="="*len(title)
    result=title+"\n"+line1+"\n\nMISSING FILES\n~~~~~~~~~~~~~\n\n"
    result+=" ".join(missing)
    result+="\n\nUPDATE TRANSLATIONS FOR {lang}\n~~~~~~~~~~~~~~~~~~~~~~~~{sub}\n\n".format(lang=lang.upper(), sub="~"*len(lang))
    result+=" ".join(lagging)
    result+="\n"
    return result
    
rstTemplate=re.compile(r"^(.*)\.rst$")

def createMissing(files, fromlang, tolang):
    """
    Creates missing files 
    @param fromlang is the preferred language for templates to copy from
    @param to lang is the target language.
    """
    if fromlang=="en": fromlang=""
    date=datetime.now().strftime("%Y-%m-%d")
    newdate = f":date: {date}\n"
    newlang = f":lang: {tolang}\n"
    for f in files:
        name=rstTemplate.match(f).group(1)
        preferred=f"{name}.{fromlang}.rst"
        source=preferred if os.path.exists(preferred) else f
        target=f"{name}.{tolang}.rst"
        print (f"{source} ==> {target}")
        with open(target, "w") as outfile:
            outfile.write(":tags: no-translation\n")
            for l in open(source).readlines():
                l=re.sub(r"^:date: .*", newdate, l)
                l=re.sub(r"^:lang: .*", newlang, l)
                outfile.write(l)
    return

if __name__=="__main__":
    lang=sys.argv[1:]
    for l in lang:
        missing, lagging = [],[]
        for path, dirnames, filenames in os.walk("content"):
            for f in filenames:
                updateTodo(missing, lagging, path, f, l)
        print(reportTodo (missing, lagging, l))
        if missing:
            rep=input("Do you want to create automatically missing files for language %s? [N/y]" %l)
            if rep.upper()=="Y":
                rep=input("select an alternative language to get files from (not 'en' which is the default):")
                createMissing(missing, rep, l)
